<?php
/**
 * @var string $display_price
 * @var string $otomoto_price
 * @var string $display_url
 * @var string $otomoto_url
 * @var string $otomoto_details_title
 */

$checked = '';
?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Widget Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
               name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
               value="<?php echo esc_attr( $title ); ?>"/>
    </p>

    <h2><?php esc_html_e( 'Fields to display:', 'inspirelabs-import-otomoto' ); ?></h2>
    <p>
        <input id="<?php echo $this->get_field_id( 'display_price' ) ?>"
               type="checkbox" <?php checked( $display_price, 'on' ) ?> class="check-column"
               name="<?php echo $this->get_field_name( 'display_price' ) ?>" />
        <label for="<?php echo $this->get_field_id( 'display_price' ) ?>"><?php esc_html_e( 'Display Price', 'inspirelabs-import-otomoto' ) ?></label>
    </p>
    <p>
        <input id="<?php echo $this->get_field_id( 'display_url' ) ?>"
               type="checkbox" <?php checked( $display_url, 'on' ) ?> class="checkbox"
               name="<?php echo $this->get_field_name( 'display_url' ) ?>" />
        <label for="<?php echo $this->get_field_id( 'display_url' ) ?>"><?php esc_html_e( 'Display announcement URL', 'inspirelabs-import-otomoto' ) ?></label>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id( 'otomoto_url_title' ) ?>"><?php esc_html_e( 'Announcement URL title', 'inspirelabs-import-otomoto' ) ?></label>
        <input id="<?php echo $this->get_field_id( 'otomoto_url_title' ) ?>" type="text"
               class="widefat" name="<?php echo $this->get_field_name( 'otomoto_url_title' ) ?>"
               value="<?php echo esc_attr( $otomoto_url ) ?>"/>
    </p>

<?php
foreach ( $this->otomoto_custom_fields as $key => $value ) : ?>
	<?php if ( isset( $instance[ $key ] ) ) : ?>
		<?php $checked = $instance[ $key ]; ?>
	<?php endif; ?>
    <p>
        <input id="<?php echo $this->get_field_id( $key ) ?>"
               type="checkbox" <?php checked( $checked, 'on' ) ?> class="checkbox"
               name="<?php echo $this->get_field_name( $key ) ?>"/>
        <label for="<?php echo $this->get_field_id( $key ) ?>"><?php echo __( 'Display ' . esc_attr( $value['text'] ) ) ?></label>
    </p>
<?php endforeach; ?>
