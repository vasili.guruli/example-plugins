<?php
 /** @var string $active_tab */
?>
<div class="wrap inpirelabs-import-otomoto">
    <div class="inpirelabs-import-otomoto-layout-main">
        <div class="inpirelabs-import-otomoto-layout-box">
            <form action="options.php" method="post">
                <h2 class="nav-tab-wrapper">
                    <a href="<?php echo esc_attr('?page=otomoto_settings_page&tab=user_settings') ?>" class="nav-tab
                <?php echo $active_tab === 'user_settings' ? 'nav-tab-active' : ''; ?>"><?php echo __( 'User Settings', 'inspirelabs-import-otomoto' ) ?></a>
                    <a href="<?php echo esc_attr('?page=otomoto_settings_page&tab=import_otomoto') ?>" class="nav-tab
                <?php echo $active_tab === 'import_otomoto' ? 'nav-tab-active' : ''; ?>"><?php echo __( 'Import Otomoto', 'inspirelabs-import-otomoto' ) ?></a>
                    <a href="<?php echo esc_attr('?page=otomoto_settings_page&tab=how_to_use') ?>" class="nav-tab
                <?php echo $active_tab === 'how_to_use' ? 'nav-tab-active' : ''; ?>"><?php echo __( 'How to use', 'inspirelabs-import-otomoto' ) ?></a>
                </h2>
				<?php if ( $active_tab === 'user_settings' ) : ?>
					<?php settings_fields( 'otomoto-user-settings-section' ); ?>
					<?php do_settings_sections( 'otomoto-user-settings-section' ); ?>
					<?php submit_button(); ?>
				<?php endif; ?>
				<?php if ( $active_tab === 'import_otomoto' ) : ?>
                    <div class="import-otomoto">
						<?php settings_fields( 'otomoto-import-settings-section' ); ?>
						<?php do_settings_sections( 'otomoto-import-settings-section' ); ?>
                    </div>
				<?php endif; ?>
	            <?php if ( $active_tab === 'how_to_use' ) : ?>
                    <div class="import-otomoto">
			            <?php include INSPIRELABS_IMPORT_OTOMOTO_TEMPLATE_DIR . '/admin/how-to-use.php'?>
                    </div>
	            <?php endif; ?>
            </form>
        </div>
    </div>
</div>

