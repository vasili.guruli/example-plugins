<div class="wrap inpirelabs-import-otomoto">
	<div class="inpirelabs-import-otomoto-layout-main">
		<div class="inpirelabs-import-otomoto-layout-box">
			<h2 class="title"><?php esc_html_e('How to display imported announcements', 'inspirelabs-import-otomoto') ?></h2>
			<blockquote>
				<p>
					<?php esc_html_e('When import announcements from Otomoto, go to WP Admin -> Cars to see imported announcements.', 'inspirelabs-import-otomoto'); ?>
				</p>
				<p>
					<?php esc_html_e('To display announcements on your site there are two ways:', 'inspirelabs-import-otomoto') ?> <br />
					<?php esc_html_e('1 . Create new template in your themes directory e.g "otomoto-page.php".', 'inspirelabs-import-otomoto') ?>
					<?php esc_html_e('After that you can insert the code snippet in your template file:', 'inspirelabs-import-otomoto') ?> <br />
					<strong><&#63;php echo do_shortcode("[otomoto_get_announcements]"); &#63;></strong><br />
                    <?php esc_html_e('Or you can display the fixed number of announcements by inserting the following code snippet: ', 'inspirelabs-import-otomoto') ?><br />
                    <strong><&#63;php echo do_shortcode("[otomoto_get_announcements posts_per_page=3]"); &#63;></strong>
				</p>
				<p>
					<?php esc_html_e('2. You can insert short code in your Posts/Pages from admin panel WP Admin -> Posts/Pages:', 'inspirelabs-import-otomoto') ?> <br />
					<strong>[otomoto_get_announcements]</strong> <?php esc_html_e('Or', 'inspirelabs-import-otomoto'); ?> <strong>otomoto_get_announcements</strong><br />
					<?php esc_html_e('Depending from shortcode insert mode on Posts/Pages', 'inspirelabs-import-otomoto') ?> <br />
                    <?php esc_html_e('You can display also fixed number of announcements by inserting the following shortcode: ', 'inspirelabs-import-otomoto') ?>
                    <strong>[otomoto_get_announcements posts_per_page=3]</strong><br />
                    <?php esc_html_e('Replace ', 'inspirelabs-import-otomoto') ?><strong>posts_per_page=<i>3</i></strong><?php esc_html_e(' with the number you want e.g:', 'inspirelabs-import-otomoto') ?>
                    <strong>[otomoto_get_announcements posts_per_page=<i><?php esc_html_e('posts number to display', 'inspirelabs-import-otomoto') ?></i>]</strong>
				</p>
			</blockquote>
			<p>
				<?php esc_html_e('After import announcements you can "Clear data" from "Import Otomoto" tab if you want', 'inspirelabs-import-otomoto') ?><br />
				<?php esc_html_e('as it is used as cached file to display imported announcements on "Import Otomoto" tab', 'inspirelabs-import-otomoto') ?>
			</p>
		</div>
	</div>
</div>
