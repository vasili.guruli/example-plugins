<div class="wrap">
    <h2><?php echo esc_html( __( 'Import Otomoto announcements', 'inspirelabs-import-otomoto' ) ); ?></h2>
    <small>
		<?php echo sprintf( __( 'Here you can import announcements from your Otomoto account.
         All announcements will be visible <a href="%s">here</a>', 'inspirelabs-import-otomoto' ), esc_url( admin_url( 'edit.php?post_type=car' ) ) ); ?>
    </small>
    <ul class="list-group">
		<?php if ( ! empty( get_option( '_manual_otomoto_import_time' ) ) ) : ?>
            <li class="list-group-item">
                <strong><?php echo __( 'Last Manual Import: ', 'inspirelabs-import-otomoto' ) ?><?php echo get_option( '_manual_otomoto_import_time' ) ?></strong>
            </li>
		<?php endif; ?>
		<?php if ( ! empty( get_option( '_scheduled_otomoto_import_time' ) ) ) : ?>
            <strong><?php echo __( 'Last Scheduled Import: ', 'inspirelabs-import-otomoto' ) ?><?php echo get_option( '_scheduled_otomoto_import_time' ) ?></strong>
		<?php endif; ?>
    </ul>
</div>