<?php
/** @var array $features */
?>
<?php if ( isset( $features ) ) :  ?>
	<div class="offer-features">
		<h4 class="offer-features-title"><?php echo __( 'Features', 'inspirelabs-import-otomoto' ); ?></h4>
		<ul class="offer-features-list">
			<?php foreach ( $features as $feature ) : ?>
				<li class="offer-feature-item">
					<i class="fas fa-check-circle"></i>
					<?php esc_html( ucwords( $feature ) ); ?>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>
