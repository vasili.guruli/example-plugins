<?php
/**
 * @var string $otomoto_price
 * @var string $otomoto_url
 * @var string $otomoto_price_title
 * @var string $otomoto_url_title
 * @var string $otomoto_details_title
 * @var string $otomoto_brand
 * @var string $display_url
 */
?>
<div class="otomoto-widget car-info">
	<?php if ( isset( $custom_fields ) ) : ?>
        <h3><?php echo esc_attr( $otomoto_brand ); ?></h3>
        <ul class="info announcements">
			<?php foreach ( $custom_fields as $custom_field => $field ) : ?>
				<?php foreach ( $field as $key => $value ) : ?>
					<?php if ( 'on' === $field[ $key ] ) : ?>
						<?php $class = $field['class'] ?>
						<?php $display = esc_attr( get_post_meta( $post->ID, $key, true ) ); ?>
						<?php
						if ( 'otomoto_przebieg' === $key ) {
							$display .= ' km';
						}
						if ( 'otomoto_pojemnosc_silnika' === $key ) {
							$display .= ' cm3';
						}
						if ( 'otomoto_moc_silnika' === $key ) {
							$display .= ' KM';
						}
						if ( 'petrol' === $display ) {
						    $display = 'benzyna';
                        }
						?>
                        <li class="details">
                           <i class="fas <?php echo esc_attr( $field['class'] ) ?>"></i>&nbsp;<?php echo esc_attr( $display ); ?>
                        </li>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
        </ul>
	<?php endif; ?>
    <p class="field display-url">
        <?php if( 'on' === $display_url ) : ?>
        <a class="button button-primary link-to-announcement" target="_blank"
           href="<?php echo esc_url( $otomoto_url ) ?>"><?php echo esc_attr( $otomoto_url_title ) ?></a>
        <?php endif; ?>
    </p>
    <p class="field">
        <span class="price"><?php echo esc_attr( $otomoto_price ) ?></span>
    </p>
</div>
