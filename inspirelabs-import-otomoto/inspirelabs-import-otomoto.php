<?php
/**
Plugin Name: Inspirelabs Import Otomoto
Plugin URI: https://www.wpdesk.pl/sklep/import-otomoto/
Description: Integracja z Otomoto
Product: Inspirelabs Import Otomoto
Version: 1.0.0
Author: vasilguruli|InspireLabs
Author URI: https://www.wpdesk.net/
Text Domain: inspirelabs-import-otomoto

@package \InspireLabs\ImportOtomoto

Copyright 2020 WP Desk Ltd.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/* THESE TWO VARIABLES CAN BE CHANGED AUTOMATICALLY */
$plugin_version           = '1.0.0';
$plugin_release_timestamp = '2019-11-29 19:01';

$plugin_name        = 'Inspirelabs Import Otomoto';
$plugin_class_name  = '\InspireLabs\ImportOtomoto\Plugin';
$plugin_text_domain = 'inspirelabs-import-otomoto';
$product_id         = 'Import Otomoto';
$plugin_file        = __FILE__;
$plugin_dir         = dirname( __FILE__ );

// Define plugin directory for inclusions
if ( ! defined( 'INSPIRELABS_IMPORT_OTOMOTO_DIR' ) ) {
	define( 'INSPIRELABS_IMPORT_OTOMOTO_DIR', dirname( __FILE__ ) );
}
// Define plugin URL for assets
if ( ! defined( 'INSPIRELABS_IMPORT_OTOMOTO_URL' ) ) {
	define( 'INSPIRELABS_IMPORT_OTOMOTO_URL', plugins_url( '', __FILE__ ) );
}
// Define templates directory
if ( ! defined( 'INSPIRELABS_IMPORT_OTOMOTO_TEMPLATE_DIR' ) ) {
	define( 'INSPIRELABS_IMPORT_OTOMOTO_TEMPLATE_DIR', INSPIRELABS_IMPORT_OTOMOTO_DIR . '/templates/' );
}

// Define cache dir
if ( ! defined( 'INSPIRELABS_IMPORT_OTOMOTO_CACHE_DIR' ) ) {
	define( 'INSPIRELABS_IMPORT_OTOMOTO_CACHE_DIR', dirname( __FILE__ ) . '/temp/cache' );
}

// Define log dir
if ( ! defined( 'INSPIRELABS_IMPORT_OTOMOTO_LOG_DIR' ) ) {
	define( 'INSPIRELABS_IMPORT_OTOMOTO_LOG_DIR', dirname( __FILE__ ) . '/log' );
}

$requirements = [
	'php'     => '7.1',
	'wp'      => '5.0',
];
require __DIR__ . '/vendor_prefixed/wpdesk/wp-plugin-flow/src/plugin-init-php52.php';

$schedule_import = new InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoScheduleImport\OtomotoScheduleImport();
register_deactivation_hook( __FILE__, function () use ( $schedule_import ) {
	$schedule_import->clear_scheduled_import();
} );

$schedule_import->otomoto_schedule_import();
$schedule_import->run();
