(function ($) {
    var otomoto_schedule_import = $('#otomoto_api_schedule_import');
    $(otomoto_schedule_import).on('change', function () {
        if ($(this).is(':checked')) {
            $('.schedule-import').show();
        } else {
            $('.schedule-import').hide();
        }
    });
    if ($(otomoto_schedule_import).is(':checked')) {
        $('.schedule-import').show();
    } else {
        $('.schedule-import').hide();
    }

})(jQuery);