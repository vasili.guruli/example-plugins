(function ($) {
    $('#otomoto_api_status').on('click', function (e) {
        let api_username = $('#otomoto_api_username');
        let api_password = $('#otomoto_api_password');
        let client_id = $('#otomoto_api_client_id');
        let client_secret = $('#otomoto_api_client_secret');
        let api_test_mode = $('#otomoto_api_test_mode');
        let url = $(api_test_mode).attr("checked") ? 'https://sbotomotopl.playground.lisbontechhub.com/api/open/oauth/token' : 'https://www.otomoto.pl/api/open/oauth/token';

        $.ajax({
            type: "post",
            dataType: "json",
            url: otomoto_api.ajax_url,
            data: {
                action: 'otomoto_api_status',
                api_username: $(api_username).val(),
                api_password: $(api_password).val(),
                client_id: $(client_id).val(),
                client_secret: $(client_secret).val(),
                api_url: url,
                otomoto_nonce: otomoto_api.otomoto_nonce
            },
            success: function (response) {
                if ( response.status === 'ok' ) {
                    $('span.api-success').show();
                    $('span.api-fail').hide();
                } else{
                    $('span.api-fail').show();
                    $('span.api-success').hide();
                }
            }
        });
    });
})(jQuery);