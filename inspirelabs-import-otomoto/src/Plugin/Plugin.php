<?php
/**
 * Plugin main class.
 *
 * @package InspireLabs\ImportOtomoto
 */

namespace InspireLabs\ImportOtomoto;

use InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoIntegration;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use InspireLabsImportOtomotoVendor\WPDesk_Plugin_Info;
use InspireLabsImportOtomotoVendor\WPDesk\PluginBuilder\Plugin\AbstractPlugin;
use InspireLabsImportOtomotoVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use InspireLabsImportOtomotoVendor\WPDesk\PluginBuilder\Plugin\HookableParent;

class Plugin extends AbstractPlugin implements LoggerAwareInterface, HookableCollection {
	
	use LoggerAwareTrait;
	use HookableParent;
	
	/** @var string $plugin_dir */
	private $plugin_dir;
	
	/**
	 * Plugin constructor.
	 *
	 * @param WPDesk_Plugin_Info $plugin_info Plugin info.
	 */
	public function __construct( WPDesk_Plugin_Info $plugin_info ) {
		parent::__construct( $plugin_info );
		$this->setLogger( new NullLogger() );
		
		$this->plugin_url       = $this->plugin_info->get_plugin_url();
		$this->plugin_dir       = $this->plugin_info->get_plugin_dir();
		$this->plugin_namespace = $this->plugin_info->get_text_domain();
		$this->docs_url         = 'https://www.wpdesk.pl/docs/otomoto-import/';
	}
	
	/**
	 * Initializes plugin external state.
	 *
	 * The plugin internal state is initialized in the constructor and the plugin should be internally consistent after creation.
	 * The external state includes hooks execution, communication with other plugins, integration with WC etc.
	 *
	 * @return void
	 */
	public function init() {
		parent::init();
	}
	
	/**
	 * Integrate with WordPress and with other plugins using action/filter system.
	 *
	 * @return void
	 */
	public function hooks() {
		parent::hooks();
		
		add_action( 'plugins_loaded', array( $this, 'load' ) );
	}
	
	/**
	 * Register plugin when GF is loaded
	 */
	public function load() {
		if ( class_exists( 'InspireLabs\\ImportOtomoto\\OtomotoIntegration\\OtomotoIntegration' ) ) {
			$plugin = new OtomotoIntegration();
			$plugin->run_otomoto(
				array(
					'plugin_url'     => $this->plugin_url,
					'plugin_dir'     => $this->plugin_dir,
					'plugin_version' => $this->plugin_info->get_version()
				)
			);
		}
	}
}
