<?php
/**
 * Class OtomotoUserSettings .
 *
 * @package InspireLabsOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoUserSettings;

use InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoCache\OtomotoCache;
use InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoSettingsHtml\OtomotoSettingsHtml;

class OtomotoUserSettings {

	/**
	 * Run settings initialization for Otomoto
	 *
	 * @return void
	 */
	public function run() {
		$this->hooks();
	}

	/**
	 * Plugin hooks
	 */
	public function hooks() {
		add_action( 'admin_menu', [ $this, 'otomoto_create_admin_menu' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_scripts' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_styles' ], 9999 );
		add_action( 'admin_init', [ $this, 'register_otomoto_settings' ] );
	}

	/**
	 * Admin scripts
	 */
	public function admin_scripts() {

	}

	/**
	 * Admin styles
	 */
	public function admin_styles() {
		wp_register_style( 'dashicon-otomoto',
			INSPIRELABS_IMPORT_OTOMOTO_URL . '/assets/css/otomoto-dashicon/css/dashicons.css', [ 'dashicons' ], '0.1' );
		wp_enqueue_style( 'dashicon-otomoto' );
		wp_enqueue_style('otomoto-admin-style', INSPIRELABS_IMPORT_OTOMOTO_URL . '/assets/css/admin_style.css', [], '1.0');
	}

	/**
	 * Admin menu for Otomoto settings
	 */
	public function otomoto_create_admin_menu() {
		add_menu_page(
			__( 'Otomoto', 'inspirelabs-import-otomoto' ),
			__( 'Otomoto', 'inspirelabs-import-otomoto' ),
			'manage_options',
			'otomoto_settings_page',
			[ $this, 'otomoto_settings' ],
			'dashicons-dashicons' // Custom dash Icon -> https://glyphter.com/
		);
	}

	/**
	 * Register settings for Otomoto import
	 */
	public function register_otomoto_settings() {
		register_setting( 'otomoto-user-settings-section', 'otomoto_api_username' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_api_password' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_api_client_id' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_api_client_secret' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_api_test_mode' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_create_gallery' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_api_schedule_import' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_api_schedule_import_time' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_posts_count' );
		register_setting( 'otomoto-user-settings-section', 'otomoto_announcements_to_import' );
		register_setting( 'otomoto-import-settings-section', 'otomoto_import' );

		add_settings_section(
			'otomoto_user_settings',
			__( '', 'inspirelabs-import-otomoto' ),
			[ $this, 'otomoto_user_settings_title' ],
			'otomoto-user-settings-section'
		);
		add_settings_section(
			'otomoto_import_settings',
			__( '', 'inspirelabs-import-otomoto' ),
			[ $this, 'otomoto_import_settings_info' ],
			'otomoto-import-settings-section'
		);
		add_settings_field(
			'otomoto-api-user-name',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_api_username'
			]
		);
		add_settings_field(
			'otomoto-api-user-password',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_api_password'
			]
		);
		add_settings_field(
			'otomoto-api-client-id',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_api_client_id'
			]
		);
		add_settings_field(
			'otomoto-api-client-secret',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_api_client_secret'
			]
		);
		add_settings_field(
			'otomoto-api-test-mode',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_api_test_mode'
			]
		);
		add_settings_field(
			'otomoto-create-gallery',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_create_gallery'
			]
		);
		add_settings_field(
			'otomoto-posts-count',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_posts_count'
			]
		);
		add_settings_field(
			'otomoto-announcements-to-import',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_announcements_to_import'
			]
		);
		add_settings_field(
			'otomoto-import',
			'',
			[ $this, 'otomoto_import_settings' ],
			'otomoto-import-settings-section',
			'otomoto_import_settings',
			[
				'value' => 'otomoto_api_import_data'
			]
		);
		add_settings_field(
			'otomoto-api-schedule-import',
			'',
			array( $this, 'otomoto_api_user_settings' ),
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_api_schedule_import'
			]
		);
		add_settings_field(
			'otomoto-api-schedule-import-time',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_api_schedule_import_time'
			]
		);
		add_settings_field(
			'otomoto-api-status',
			'',
			[ $this, 'otomoto_api_user_settings' ],
			'otomoto-user-settings-section',
			'otomoto_user_settings',
			[
				'value' => 'otomoto_api_status'
			]
		);
	}

	/**
	 * User settings title
	 */
	public function otomoto_user_settings_title() {
		echo sprintf( __( '%1$sOtomoto User Settings%2$s', 'inspirelabs-import-otomoto' ), '<h2 class="inspirelabs-otomoto-settings-title">', '</h2>' );
	}

	/**
	 * User settings title
	 */
	public function otomoto_import_settings_info() {
		include( INSPIRELABS_IMPORT_OTOMOTO_TEMPLATE_DIR . 'admin/import-otomoto-info.php' );
	}

	/**
	 * Otomoto user settings
	 *
	 * @param array $args
	 *
	 * @return void
	 */
	public function otomoto_api_user_settings( $args ) {
		if ( ! empty( $args['value'] ) ) {
			OtomotoSettingsHtml::generate_form_fields_for_otomoto_user_settings( $args );
		}
	}

	/**
	 * Otomoto import
	 *
	 * @param array $args
	 *
	 * @return void
	 */
	public function otomoto_import_settings( $args ) {
		$import_hash = base64_encode( hash_hmac( 'sha256', get_option( 'otomoto_api_client_secret' ),
			get_option( 'otomoto_api_client_id' ), true ) );
		if ( ! empty( $args['value'] ) ) {
			$otomoto_data         = OtomotoCache::get_cache( $import_hash );
			$display_clean_button = 'hidden';
			if ( isset( $otomoto_data['otomoto_data'] ) ) {
				OtomotoSettingsHtml::otomoto_data_table( $otomoto_data['otomoto_data'] );
			}
			if ( ! empty( $otomoto_data ) ) {
				$display_clean_button = 'visible';
				$import_hash          = 'none';
			}
			OtomotoSettingsHtml::generate_import_button_for_otomoto( $args, $import_hash, $display_clean_button );
		}
	}

	/**
	 * Otomoto settings .
	 */
	public function otomoto_settings() {
		if ( ! is_admin() && ! current_user_can( 'manage_options' ) ) {
			return;
		}
		$active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'user_settings';
		if ( isset( $_GET['settings-updated'] ) ) {
			add_settings_error( 'optomoto_settings_saved', 'Otomoto settings', __( 'Settings Saved', 'inspirelabs-import-otomoto' ),
				'updated' );
		}

		settings_errors( 'optomoto_settings_saved' );
		include( INSPIRELABS_IMPORT_OTOMOTO_TEMPLATE_DIR . 'admin/settings.php' );
	}
}
