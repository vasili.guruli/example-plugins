<?php
/**
 * Class OtomotoScheduleImport .
 *
 * @package InspireLabsOtomoto
 */


namespace InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoScheduleImport;

use InspireLabs\ImportOtomoto\OtomotoIntegration\ImportOtomoto\ImportOtomoto;
use InspireLabs\ImportOtomoto\OtomotoIntegration\ImportOtomoto\Otomoto;

class OtomotoScheduleImport extends ImportOtomoto {
	/**
	 * Run schedule import for Otomoto
	 *
	 * @return void
	 */
	public function run() {
		
		if ( 'on' === get_option( 'otomoto_api_schedule_import' ) ) {
			add_filter( 'cron_schedules', array( $this, 'otomoto_schedule_time_set' ), 5 );
			add_action( 'otomoto_scheduled_import', array(
				$this,
				'scheduled_import'
			), 99
			);
		}
	}
	
	/**
	 * Register scheduled event for Otomoto, if job already exists delete it after next next schedule save
	 *
	 * @return void|bool
	 */
	public function otomoto_schedule_import() {
		$cron_jobs = get_option( 'cron' );
		if ( 'on' === get_option( 'otomoto_api_schedule_import' ) ) {
			$schedule_import_time = get_option( 'otomoto_api_schedule_import_time' );
			if ( 'select_time' === $schedule_import_time ) {
				wp_clear_scheduled_hook( 'otomoto_scheduled_import' );
				return false;
			}
			foreach ( $cron_jobs as $crons => $job ) {
				if ( isset( $job['otomoto_scheduled_import'] ) ) {
					foreach ( $job['otomoto_scheduled_import'] as $otomoto_jobs ) {
						if ( $otomoto_jobs['schedule'] !== $schedule_import_time ) {
							wp_clear_scheduled_hook( 'otomoto_scheduled_import' );
						}
					}
				}else{
					if ( ! wp_next_scheduled( 'otomoto_scheduled_import' ) ) {
						wp_schedule_event( time(), $schedule_import_time, 'otomoto_scheduled_import' );
					}
				}
			}
		}
	}
	
	/**
	 * Scheduled Otomoto import
	 */
	public function scheduled_import() {
		$otomoto                  = new Otomoto();
		$scheduled_otomoto_import = date( 'Y-m-d H:i:s' );
		$otomoto->import( 'auto' );
		update_option( '_scheduled_otomoto_import_time', $scheduled_otomoto_import );
	}
	
	/**
	 * Clear scheduled import cron job
	 *
	 * @return void
	 */
	public function clear_scheduled_import() {
		wp_clear_scheduled_hook( 'otomoto_scheduled_import' );
	}
	
	/**
	 * Set custom time for schedule import
	 *
	 * @param array $schedules
	 *
	 * @return array
	 */
	public function otomoto_schedule_time_set( $schedules ) {
		
		$display               = '';
		$interval              = 1800;
		$otomoto_schedule_time = get_option( 'otomoto_api_schedule_import_time' );
		switch ( $otomoto_schedule_time ) {
			case 'twicedaily' :
				$interval = 43200;
				$display  = __( 'Dwa razy dziennie', 'inspirelabs-import-otomoto' );
				break;
			case 'hourly' :
				$interval = 3600;
				$display  = __( 'Co godzinę', 'inspirelabs-import-otomoto' );
				break;
			case 'weekly' :
				$interval = 604800;
				$display  = __( 'Raz w tygodniu', 'inspirelabs-import-otomoto' );
				break;
		}
		
		$schedules[ $otomoto_schedule_time ] = array(
			'interval' => $interval,
			'display'  => $display
		);
	
		return $schedules;
	}
}
