<?php
/**
 * Plugin init class.
 *
 * @package InspireLabs\ImportOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration;

use InspireLabs\ImportOtomoto\OtomotoIntegration\ImportOtomoto\OtomotoApiConnect;

class OtomotoIntegration {
	/**@var array $otomoto_configurations */
	private $otomoto_configurations = array();
	/** @var string $plugin_url */
	private $plugin_url;
	/**@var string $plugin_dir */
	private $plugin_dir;
	/**@var string $plugin_version */
	private $plugin_version;
	
	/**
	 * Run otomoto import plugin
	 *
	 * @param array $args
	 *
	 * @return void
	 */
	public function run_otomoto( $args = array() ) {
		
		$this->hooks();
		
		$this->otomoto_configurations = array(
			'import_otomoto'    => '\\InspireLabs\\ImportOtomoto\\OtomotoIntegration\\ImportOtomoto\\ImportOtomoto',
			'otomoto_post_type' => '\\InspireLabs\\ImportOtomoto\\OtomotoIntegration\\OtomotoCarPostType\\OtomotoCarPostType',
			'otomoto_shortcode' => '\\InspireLabs\\ImportOtomoto\\OtomotoIntegration\\OtomotoImportShortCode\\OtomotoImportShortCode',
			'otomoto_settings'  => '\\InspireLabs\\ImportOtomoto\\OtomotoIntegration\\OtomotoUserSettings\\OtomotoUserSettings',
		);
		
		$this->plugin_url     = $args['plugin_url'];
		$this->plugin_dir     = $args['plugin_dir'];
		$this->plugin_version = $args['plugin_version'];
		$this->run_otomoto_configurations();
	}
	
	/**
	 * Otomoto plugin hooks
	 *
	 * @return void
	 */
	private function hooks() {
		add_action( 'wp_enqueue_scripts', array( $this, 'otomoto_styles' ), 99 );
		add_action( 'wp_enqueue_scripts', array( $this, 'otomoto_scripts' ), 99 );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		add_filter( 'the_content', array( $this, 'the_content' ) );
		add_action( 'widgets_init', array( $this, 'register_otomoto_widget' ) );
	}
	
	/**
	 * Register custom widget for Otomoto
	 *
	 * @return void
	 */
	public function register_otomoto_widget() {
		register_widget( '\\InspireLabs\\ImportOtomoto\\OtomotoIntegration\\OtomotoWidget\\OtomotoWidget' );
	}
	
	public function the_content( $content ) {
		global $post;
		
		if ( 'car' !== $post->post_type ) {
			return $content;
		}
		
		return $content;
	}
	
	/**
	 * Run otomoto configurations
	 *
	 * @return void
	 */
	private function run_otomoto_configurations() {
		foreach ( $this->otomoto_configurations as $configuration ) {
			if ( class_exists( $configuration ) ) {
				try {
					if ( ( new \ReflectionClass( $configuration ) )->getConstructor() ) {
						//TODO Add args handle for class::__constructor()
						continue;
					}
					$otomoto_run = new $configuration();
					if ( method_exists( $otomoto_run, 'run' ) ) {
						$otomoto_run->run(
							array(
								'plugin_url' => $this->plugin_url,
								'plugin_dir' => $this->plugin_dir,
							)
						);
					}
				} catch ( \ReflectionException $e ) {
					//TODO Catch unhandled exception
				}
			}
		}
	}
	
	/**
	 * Otomoto frontend scripts
	 *
	 * @return void
	 */
	public function otomoto_scripts() {
	    global $post;
	    
	    if ( 'car' === $post->post_type && is_single() ) {
		    wp_enqueue_script( 'lightbox-js',
			    $this->plugin_url . '/assets/js/lightbox.min.js', array(
				    'jquery',
			    ), '2.0.5', true );
		
		    wp_enqueue_script( 'otomoto-gallery-js',
			    $this->plugin_url . '/assets/js/otomoto-gallery.js', array(
				    'jquery',
			    ), '1.0.5', true );
	    }
	}
	
	/**
	 * Otomoto admin scripts
	 *
	 * @return void
	 */
	public function admin_scripts() {
		if ( ! is_admin() ) {
			return;
		}
		$current_screen = get_current_screen();
		
		if ( 'toplevel_page_otomoto_settings_page' === $current_screen->base ) {
			wp_enqueue_script( 'otomoto-admin-schedule-import',
				INSPIRELABS_IMPORT_OTOMOTO_URL . '/assets/js/schedule-import-time.js', array( 'jquery' ), $this->plugin_version, true );
			wp_enqueue_script( 'otomoto-api-status',
				$this->plugin_url . '/assets/js/api-status.js', array(
					'jquery',
				), $this->plugin_version, true );
			
			wp_localize_script( 'otomoto-api-status', 'otomoto_api', array(
				'otomoto_nonce' => wp_create_nonce( 'otomoto-api-status' ),
				'ajax_url'      => esc_url( admin_url( 'admin-ajax.php' ) )
			) );
			
			wp_enqueue_style( 'otomoto-font-awesome-styles', $this->plugin_url . '/assets/css/all.min.css', array(), '5.13.0' );
			
		}
	}
	
	/**
	 * Otomoto front styles
	 *
	 * @return void
	 */
	public function otomoto_styles() {
	    global $post;
		wp_enqueue_style( 'otomoto-styles-bootstrap', $this->plugin_url . '/assets/css/bootstrap.min.css', array(), '4.5.2', true );
		wp_enqueue_style( 'otomoto-styles-simple-line-icons', 'https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css', array(), '2.4.1', true );
		
		wp_enqueue_style( 'otomoto-font-awesome-styles', $this->plugin_url . '/assets/css/all.min.css', array(), '5.13.0' );
		
		wp_enqueue_style( 'otomoto-styles', $this->plugin_url . '/assets/css/otomoto_style.css', array( 'otomoto-font-awesome-styles' ), '1.0' );
		
		if ( 'car' === $post->post_type && is_single() ) {
			wp_enqueue_style( 'lightbox-css',
				$this->plugin_url . '/assets/css/lightbox.min.css', array(), '2.0.5', false );
		}
	}
}
