<?php
/**
 * Class OtomotoSettingsHtml .
 *
 * @package InspireLabs\ImportOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoCache;


class OtomotoCache {

	/**
	 * Run custom post type register for Otomoto
	 *
	 * @param array $args
	 *
	 * @return void
	 */
	public function run( $args = array() ) {

	}

	/**
	 * @param $key
	 * @param $data
	 * @param int $time
	 *
	 * @return bool
	 */
	public function set_cache( $key, $data, $time = 3600 ) {
		$content = array();

		if ( $time ) {
			$content['otomoto_data']       = $data;
			$content['created_at'] = time() + $time;

			if ( file_put_contents( INSPIRELABS_IMPORT_OTOMOTO_CACHE_DIR . '/' . md5( $key ) . '.txt', serialize( $content ) ) ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param $key
	 *
	 * @return bool|mixed
	 */
	public static function get_cache( $key ) {
		$file = INSPIRELABS_IMPORT_OTOMOTO_CACHE_DIR . '/' . md5( $key ) . '.txt';
		if ( file_exists( $file ) ) {
			$content = unserialize( file_get_contents( $file ) );
			if ( ! is_array( $content ) ) {
				return false;
			}
			if ( time() <= $content['created_at'] ) {
				return $content;
			}

			unlink( $file );
		}

		return false;
	}

	/**
	 * @param $key
	 *
	 * @return void
	 */
	public function delete_cache( $key ) {
		$file = INSPIRELABS_IMPORT_OTOMOTO_CACHE_DIR . '/' . md5( $key ) . '.txt';
		if ( file_exists( $file ) ) {
			unlink( $file );
		}
	}
}
