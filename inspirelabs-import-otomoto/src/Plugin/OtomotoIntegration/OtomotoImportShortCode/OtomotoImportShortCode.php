<?php
/**
 * Class Otomoto .
 *
 * @package InspirelabsOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoImportShortCode;

use InspireLabs\ImportOtomoto\OtomotoIntegration\AllowedHtml\AllowedHtml;

class OtomotoImportShortCode {
	use AllowedHtml;
	
	/**@var int $otomoto_posts_per_page */
	private $otomoto_posts_per_page = 5;
	
	/**
	 * Run Shortcode initialization for Otomoto
	 *
	 * @return void
	 */
	public function run() {
		add_shortcode( 'otomoto_get_announcements', array( $this, 'otomoto_announcements' ) );
		if ( '' !== get_option( 'otomoto_posts_count' ) ) {
			$this->otomoto_posts_per_page = (int) get_option( 'otomoto_posts_count' );
		}
	}
	
	/**
	 * @param \WP_Post $post
	 * @param string $css_class
	 * @param int $posts_counter
	 *
	 * @return string
	 */
	public function otomoto_get_cars( $post, $css_class, $posts_counter = 0 ) {
		$no_image_url = INSPIRELABS_IMPORT_OTOMOTO_URL . '/assets/images/no-image.png';
		$post_id      = $post->ID;
		$url          = esc_url( get_post_meta( $post_id, 'otomoto_url', true ) );
		$power        = esc_attr( get_post_meta( $post_id, 'otomoto_moc_silnika', true ) );
		$capacity     = esc_attr( get_post_meta( $post_id, 'otomoto_pojemnosc_silnika', true ) );
		$fuel         = esc_attr( get_post_meta( $post_id, 'otomoto_typ_paliwa', true ) );
		$year         = esc_attr( get_post_meta( $post_id, 'otomoto_rok', true ) );
		$mileage      = esc_attr( get_post_meta( $post_id, 'otomoto_przebieg', true ) );
		$price        = esc_attr( get_post_meta( $post_id, 'otomoto_cena', true ) );
		$price_net    = esc_attr( get_post_meta( $post_id, 'otomoto_cena_netto', true ) );
		$is_reserved  = mb_strpos( mb_strtolower( $post->post_title ), 'rezerw' ) !== false;
		$thumb_image  = esc_url( get_the_post_thumbnail_url( $post_id, 'otomoto-car-thumb' ) );
		if ( false === $thumb_image ) {
			$thumb_image = $no_image_url;
		}
		
		$html = '';
		$html .= '<div class="' . esc_attr( $css_class ) . ' one_third car_box ' . ( esc_attr( $is_reserved ) ? 'reserved' : '' ) . '">';
		$html .= '<div class="container">';
		$html .= '';
		$html .= '<div class="thumbnail">
					<a href="' . esc_url( get_permalink( $post->ID ) ) . '" target="_blank">
					<img src="' . esc_url( $thumb_image ) . '" class="img-thumbnail" alt="photo" />
					</a>
					<a href="' . esc_url( get_permalink( $post->ID ) ) . '" target="_blank">
					' . esc_attr( $post->post_title ) . '
					</a>
					</div>
					<ul class="info announcements">
						<li><i class="fas fa-car"></i>&nbsp;' . esc_html( $year ) . '</li>
						<li><i class="fas fa-road"></i>&nbsp;' . esc_html( $mileage ) . ' km</li>
						<li class="upper"><i class="fas fa-gas-pump"></i>&nbsp;' . esc_html( ( $fuel === 'petrol' ? 'benzyna' : $fuel ) ) . '</li>
						<li><i class="fas fa-horse"></i>&nbsp;' . esc_html( $power ) . ' KM</li>
						<li><i class="fas fa-rocket"></i>&nbsp;' . esc_html( $capacity ) . ' cm<sub>3</sub></li>
						<li class="go-to-announcement"><a href="' . esc_url( $url ) . '" target="_blank" data-toggle="tooltip" class="button button-primary" title="' . esc_attr( $post->post_title ) . '">' . esc_html( __( 'Go to announcement', 'inspirelabs-import-otomoto' ) ) . '</a></li>
					</ul>';
		$html .= '<div class="well"><span class="price">' . __( 'Price', 'inspirelabs-import-otomoto' ) . ' ' . ( $price_net == 1 ? 'netto' : '' ) . ': <i>' . esc_attr( $price ) . '</i></span></div>';
		$html .= '</div>';
		$html .= '<div style="clear:both;"></div>';
		
		$html .= '</div>';
		
		return wp_kses( $html, $this->allowed_html() );
		
	}
	
	/**
	 * Get otomoto posts
	 *
	 * @param int $limit
	 *
	 * @return string
	 */
	public function otomoto_get_posts( $limit = 5 ) {
		$active = ( 'on' === esc_attr( get_option( 'otomoto_api_test_mode' ) ) ? '' : 'active' );
		$args   = array(
			'post_type'      => 'car',
			'post_status'    => 'any',
			'meta_key'       => 'otomoto_status',
			'meta_value'     => esc_attr( $active ),
			'posts_per_page' => esc_attr( $limit )
		);
		
		$posts         = get_posts( $args );
		$posts_counter = count( $posts );
		
		$html = '';
		if ( $posts_counter < $limit ) {
			$limit = $posts_counter;
		}
		
		for ( $i = 1; $i <= $limit; $i ++ ) {
			$post_index = $i - 1;
			
			if ( ! array_key_exists( $post_index, $posts ) ) {
				continue;
			}
			
			$post = $posts[ $post_index ];
			
			$html .= $this->otomoto_get_cars( $post, 'section' . $i . ( $i === $posts_counter ? ' last' : '' ), $posts_counter );
			
		}
		
		return wp_kses( $html, $this->allowed_html() );
	}
	
	/**
	 * Display Otomoto Announcements add shortcode -> [otomoto_get_announcements]
	 *
	 * @param array $attr
	 *
	 * @return string
	 */
	public function otomoto_announcements( $attr ) {
		
		$args = shortcode_atts( array(
			'posts_per_page' => esc_attr( $this->otomoto_posts_per_page )
		), $attr );
		
		$html = '';
		$html .= '<div id="primary" class="otomoto-announcements">' . $this->otomoto_get_posts( $args['posts_per_page'] ) . '</div>';
		
		return wp_kses( $html, $this->allowed_html() );
	}
}
