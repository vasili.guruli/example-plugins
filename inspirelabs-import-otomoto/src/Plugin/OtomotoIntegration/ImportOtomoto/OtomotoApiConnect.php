<?php
/**
 * Class Otomoto .
 *
 * @package OtomotoApiConnect
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\ImportOtomoto;


class OtomotoApiConnect {
	/** @var string $otomoto_auth_url */
	private $otomoto_auth_url;
	/** @var string $otomoto_api_url */
	private $otomoto_api_url;
	/** @var string $otomoto_user_id */
	private $otomoto_user_id;
	/** @var string $otomoto_user_name */
	private $otomoto_user_name;
	/** @var string $otomoto_user_password */
	private $otomoto_user_password;
	/** @var array $otomoto_user_secret */
	private $otomoto_user_secret;

	const AUTH_GRANT_TYPE = 'password';
	const AUTH_HEADERS_APPLICATION_TYPE_JSON = 'application/json';
	const AUTH_HEADERS_AUTHORIZATION_TYPE_BASIC = 'Basic';
	const AUTH_HEADERS_AUTHORIZATION_TYPE_BEARER = 'Bearer';
	
	/**
	 * OtomotoApiConnect constructor.
	 *
	 * @param array $args
	 */
	public function __construct( $args = [] ) {
		$this->otomoto_auth_url      = esc_url( $args['auth_url'] );
		$this->otomoto_api_url       = esc_url( $args['api_url'] );
		$this->otomoto_user_id       = esc_attr( $args['user_id'] );
		$this->otomoto_user_name     = esc_attr( $args['user_name'] );
		$this->otomoto_user_password = esc_attr( $args['user_password'] );
		$this->otomoto_user_secret   = esc_attr( $args['user_secret'] );
	}

	/**
	 * Get Auth Token from Otomoto to send it back for adverts
	 *
	 * @return bool|array
	 */
	public function get_auth_token_from_request() {
		try {
			$request = wp_remote_post( $this->get_otomoto_auth_url(), [
				'headers' => [
					'Accept'        => self::AUTH_HEADERS_APPLICATION_TYPE_JSON,
					'Authorization' => self::AUTH_HEADERS_AUTHORIZATION_TYPE_BASIC . ' ' . $this->decode_otomoto_user_credentials_for_auth_request()
				],
				'body'    => $this->build_query(),
			] );
		} catch ( \Exception $e ) {

			$errors = [
				'message' => $e->getMessage(),
				'line'    => $e->getLine(),
				'file'    => $e->getFile(),
				'code'    => $e->getCode()
			];
			do_action( 'otomoto_notices', $errors );

			return false;

		}
		if ( is_wp_error( $request ) ) {
			$errors = [
				'message' => __('Could not get Auth Token from Otomoto service. Please refresh page or try later', 'inspirelabs-import-otomoto'),
				'description' => __('', 'inspirelabs-import-otomoto'),
			];
			do_action( 'otomoto_notices', $errors );
			
			return false;
		}
		return json_decode( $request['body'] );
	}

	/**
	 * Send back Auth Token to Otomoto and get Adverts
	 *
	 * @return bool|array
	 */
	public function send_auth_token_and_get_adverts() {
		/** @var \StdClass $response */
		$response  = $this->get_auth_token_from_request();
		$adverts = [];
		if ( null !== $response ) {
			if ( is_object( $response ) ) {
				try {
					$adverts = wp_remote_get( $this->get_otomoto_api_url(), [
						'headers' => [
							'Accept' => self::AUTH_HEADERS_APPLICATION_TYPE_JSON,
							'Content-Type' => self::AUTH_HEADERS_APPLICATION_TYPE_JSON,
							'Authorization' => self::AUTH_HEADERS_AUTHORIZATION_TYPE_BEARER . ' ' . $response->access_token,
						]
					] );
				} catch ( \Exception $e ) {
					$errors = [
						'message' => $e->getMessage(),
						'line'    => $e->getLine(),
						'file'    => $e->getFile(),
						'code'    => $e->getCode()
					];
					do_action( 'otomoto_notices', $errors );
					
					return false;
					
				}
				
				return json_decode( $adverts['body'] );
			}
		}
		if ( is_wp_error( $adverts ) ) {
			$errors = [
				'message' => __('Could not get Auth Token from Otomoto service. Please refresh page or try later', 'inspirelabs-import-otomoto'),
				'description' => __('', 'inspirelabs-import-otomoto'),
			];
			do_action( 'otomoto_notices', $errors );
			
			return false;
		}
		
		return $adverts;
	}

	/**
	 * Decode User Credentials for Auth request
	 *
	 * @return string
	 */
	private function decode_otomoto_user_credentials_for_auth_request() {
		return base64_encode( $this->get_otomoto_user_id() . ':' . $this->get_otomoto_user_secret() );
	}

	/**
	 * Build Http query for Auth request
	 *
	 * @return string
	 */
	private function build_query() {
		$otomoto_params = [
			'grant_type' => self::AUTH_GRANT_TYPE,
			'username'   => $this->get_otomoto_user_name(),
			'password'   => $this->get_otomoto_user_password(),
		];

		return http_build_query( $otomoto_params, null, '&' );
	}

	/**
	 * Get Otomoto Auth Url
	 *
	 * @return string
	 */
	public function get_otomoto_auth_url() {
		return $this->otomoto_auth_url;
	}

	/**
	 * Get Otomoto Api Url
	 *
	 * @return string
	 */
	public function get_otomoto_api_url() {
		return $this->otomoto_api_url;
	}

	/**
	 * Get Otomoto User Id
	 *
	 * @return string
	 */
	public function get_otomoto_user_id() {
		return $this->otomoto_user_id;
	}

	/**
	 * Get Otomoto User name
	 *
	 * @return string
	 */
	public function get_otomoto_user_name() {
		return $this->otomoto_user_name;
	}

	/**
	 * Get Otomoto User password
	 *
	 * @return string
	 */
	public function get_otomoto_user_password() {
		return $this->otomoto_user_password;
	}

	/**
	 * Get Otomoto User secret
	 *
	 * @return string
	 */
	public function get_otomoto_user_secret() {
		return $this->otomoto_user_secret;
	}
}
