<?php
/**
 * Class OtomotoSettingsHtml .
 *
 * @package ImportOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\ImportOtomoto;


use InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoCache\OtomotoCache;

class ImportOtomoto {
	/** @var OtomotoCache $otomoto_cache */
	private $otomoto_cache;
	
	const ERROR_DEBUG = false;
	
	/**
	 * Run Otomoto import
	 *
	 * @return void
	 */
	public function run() {
		add_action( 'admin_head', array( $this, 'import_otomoto' ), 99 );
		add_action( 'otomoto_notices', array( $this, 'otomoto_notices' ) );
		
		$this->otomoto_cache = new OtomotoCache();
	}
	
	/**
	 * Show notices if something went wrong during import
	 *
	 * @param array $errors
	 *
	 * @return void
	 *
	 * @see Otomoto::import()
	 *
	 */
	public function otomoto_notices( $errors ) {
		if ( isset( $errors ) ) {
			if ( true === self::ERROR_DEBUG ) {
				echo '<div class="error notices-dismissible">
							<p>' . $errors['message'] . '</p>
							<p><b>File</b>: ' . esc_attr( $errors['file'] ) . '</p>
							<p><b>Error Code</b>: ' . esc_attr( $errors['code'] ) . '</p>
							<p><b>Line</b>: ' . esc_attr( $errors['line'] ) . '</p>
					</div>';
			} else {
				echo '<div class="error notices-dismissible"><p>' . sprintf( __( 'Something went wrong during import. Please try again later.
				 Error message: %1$s%4$s%2$s' ), '<strong>', '</strong>', '<br />', esc_attr( $errors['message'] . ' - ' . $errors['description'] ) ) . '</p></div>';
			}
		}
	}

	/**
	 * Import Otomoto data
	 *
	 * @return void
	 */
	public function import_otomoto() {
		
		if ( get_current_screen()->base !== 'toplevel_page_otomoto_settings_page' ) {
			return;
		}
		if ( ! is_admin() && ! current_user_can( 'manage_options' ) ) {
			return;
		}
		$otomoto = new Otomoto();
		
		if ( true === $otomoto->get_otomoto_auth_token() ) {
			update_option( 'otomoto_api_status', 'ok' );
			
		} else {
			update_option( 'otomoto_api_status', 'fail' );
		}
		$import_hash = base64_encode( hash_hmac( 'sha256', get_option( 'otomoto_api_client_secret' ), get_option( 'otomoto_api_client_id' ), true ) );
		$hash        = '';
		if ( isset( $_GET['import_hash'] ) ) {
			$hash = rawurldecode( $_GET['import_hash'] );
		}
		if ( '' !== $hash && $hash === $import_hash ) {
			$manual_update_time = date( 'Y-m-d H:i:s' );
			if ( ! OtomotoCache::get_cache( $import_hash ) ) {
				$otomoto_data = $otomoto->import( 'manual' );
				if ( ! empty( $otomoto_data ) ) {
					update_option( '_manual_otomoto_import_time', $manual_update_time );
					$this->otomoto_cache->set_cache( $import_hash, $otomoto_data );
				}
			}
		}
		if ( isset( $_GET['clean_data'] ) && $_GET['clean_data'] === 'clean_cache' ) {
			$this->otomoto_cache->delete_cache( $import_hash );
		}
	}
}
