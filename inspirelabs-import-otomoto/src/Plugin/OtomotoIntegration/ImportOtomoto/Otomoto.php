<?php
/**
 * Class Otomoto .
 *
 * @package InspirelabsOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\ImportOtomoto;


use InspireLabs\ImportOtomoto\OtomotoIntegration\AllowedHtml\AllowedHtml;
use InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoApiCredentials\OtomotoApiCredentials;
use InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoCache\OtomotoCache;
use OAuth2\Client;
use WP_Http;

class Otomoto {
	use OtomotoApiCredentials;
	use AllowedHtml;
	
	/** @var array $log */
	private $log = [];
	/** @var string $sandbox */
	private $sandbox;
	/** @var OtomotoApiConnect $otomoto_api */
	private $otomoto_api;
	
	public function __construct() {
		$this->otomoto_api = new OtomotoApiConnect( $this->get_appropriate_settings_data_for_otomoto() );
		// Otomoto returns hashed images and wordpress doesn't allow to upload them
		// This need to be fixed with better way
		define( 'ALLOW_UNFILTERED_UPLOADS', true );
	}
	
	/**
	 * Get announcement fro Otomoto
	 *
	 * @return array
	 */
	private function get_adverts() {
		return $this->otomoto_api->send_auth_token_and_get_adverts();
	}
	
	/**
	 * Get otomoto auth token
	 *
	 * @return bool
	 */
	public function get_otomoto_auth_token() {
		$access_token = $this->otomoto_api->get_auth_token_from_request();
		
		if ( null !== $access_token ) {
			if ( ! empty( $access_token->access_token ) ) {
				return true;
			} else {
				return false;
			}
		}
		
		return false;
	}
	
	/**
	 * Translate features.
	 *
	 * Otomoto returns features as "key" e.g assisted-steering => Assisted Steering, returns only key [assisted-steering]
	 * [array => [ 0 => assisted-steering ]]
	 * For now translation should be in manual mode, as we don't know what will be set as announcement features
	 * replace values with Polish translation
	 *
	 * @return array
	 */
	private function translate_features() {
		return [
			'abs'                         => 'ABS',
			'cd'                          => 'CD',
			'central-lock'                => 'Centralny zamek',
			'front-electric-windows'      => 'Elektryczne szyby przednie',
			'electronic-rearview-mirrors' => 'Elektrycznie ustawiane lusterka',
			'electronic-immobiliser'      => 'Immobilizer',
			'front-airbags'               => 'Poduszka powietrzna kierowcy',
			'front-passenger-airbags'     => 'Poduszka powietrzna pasażera',
			'original-radio'              => 'Radio fabryczne',
			'assisted-steering'           => 'Wspomaganie kierownicy',
			'alarm'                       => 'Alarm',
			'alloy-wheels'                => 'Alufelgi',
			'asr'                         => 'ASR (kontrola trakcji)',
			'park-assist'                 => 'Asystent parkowania',
			'lane-assist'                 => 'Asystent pasa ruchu',
			'bluetooth'                   => 'Bluetooth',
			'automatic-wipers'            => 'Czujnik deszczu',
			'blind-spot-sensor'           => 'Czujnik martwego pola',
			'automatic-lights'            => 'Czujnik zmierzchu',
			'both-parking-sensors'        => 'Czujniki parkowania przednie',
			'rear-parking-sensors'        => 'Czujniki parkowania tylne',
			'panoramic-sunroof'           => 'Dach panoramiczny',
			'electric-exterior-mirror'    => 'Elektrochromatyczne lusterka boczne',
			'electric-interior-mirror'    => 'Elektrochromatyczne lusterko wsteczne',
			'rear-electric-windows'       => 'Elektryczne szyby tylne',
			'electric-adjustable-seats'   => 'Elektrycznie ustawiane fotele',
			'esp'                         => 'ESP (stabilizacja toru jazdy)',
			'aux-in'                      => 'Gniazdo AUX',
			'sd-socket'                   => 'Gniazdo SD',
			'usb-socket'                  => 'Gniazdo USB',
			'towing-hook'                 => 'Hak',
			'head-display'                => 'HUD (wyświetlacz przezierny)',
			'isofix'                      => 'Isofix',
			'rearview-camera'             => 'Kamera cofania',
			'automatic-air-conditioning'  => 'Klimatyzacja automatyczna',
			'quad-air-conditioning'       => 'Klimatyzacja czterostrefowa',
			'dual-air-conditioning'       => 'Klimatyzacja dwustrefowa',
			'air-conditioning'            => 'Klimatyzacja manualna',
			'onboard-computer'            => 'Komputer pokładowy',
			'side-window-airbags'         => 'Kurtyny powietrzne',
			'shift-paddles'               => 'Łopatki zmiany biegów',
			'mp'                          => 'MP',
			'gps'                         => 'Nawigacja GPS',
			'dvd'                         => 'Odtwarzacz DVD',
			'speed-limiter'               => 'Ogranicznik prędkości',
			'auxiliary-heating'           => 'Ogrzewanie postojowe',
			'heated-windshield'           => 'Podgrzewana przednia szyba',
			'heated-rearview-mirrors'     => 'Podgrzewane lusterka boczne',
			'front-heated-seats'          => 'Podgrzewane przednie siedzenia',
			'rear-heated-seats'           => 'Podgrzewane tylne siedzenia',
			'driver-knee-airbag'          => 'Poduszka powietrzna chroniąca kolana',
			'front-side-airbags'          => 'Poduszki boczne przednie',
			'rear-passenger-airbags'      => 'Poduszki boczne tylne',
			'tinted-windows'              => 'Przyciemniane szyby',
			'radio'                       => 'Radio niefabryczne',
			'adjustable-suspension'       => 'Regulowane zawieszenie',
			'roof-bars'                   => 'Relingi dachowe',
			'system-start-stop'           => 'System Start-Stop',
			'sunroof'                     => 'Szyberdach',
			'daytime-lights'              => 'Światła do jazdy dziennej',
			'leds'                        => 'Światła LED',
			'fog-lights'                  => 'Światła przeciwmgielne',
			'xenon-lights'                => 'Światła Xenonowe',
			'leather-interior'            => 'Tapicerka skórzana',
			'velour-interior'             => 'Tapicerka welurowa',
			'cruise-control'              => 'Tempomat',
			'active-cruise-control'       => 'Tempomat aktywny',
			'tv'                          => 'Tuner TV',
			'steering-whell-comands'      => 'Wielofunkcyjna kierownica',
			'cd-changer'                  => 'Zmieniarka CD',
		];
	}
	
	/**
	 * @param string $otomoto_id
	 *
	 * @return array
	 */
	public function get_advert_details( $otomoto_id ) {
		return [];
	}
	
	/**
	 * Import announcements from Otomoto
	 *
	 * @param string $import_type
	 *
	 * @return bool|array
	 */
	public function import( $import_type = '' ) {
		
		$handled = [];
		$adverts = $this->get_adverts();
		
		$adverts                = json_decode( json_encode( $adverts ), true );
		$cars_for_admin_preview = [];
		if ( $import_type == 'manual' ) {
			$cars_for_admin_preview = $this->manual_otomoto_import( $adverts );
		}
		if ( isset( $adverts['results'] ) ) {
			if ( is_array( $adverts['results'] ) && count( $adverts['results'] ) > 0 ) {
				foreach ( $adverts['results'] as $i => $result ) {
					$returned = $this->update_or_insert( $result );
					if ( $returned !== false ) {
						$handled[] = $returned;
					}
				}
			}
			$this->delete_except( $handled );
		} else {
			$message = array(
				'message'     => __( 'Nothing to import', 'inspirelabs-import-otomoto' ),
				'description' => __( 'Adverts are empty or there was a problem with access token', 'inspirelabs-import-otomoto' )
			);
			do_action( 'otomoto_notices', $message );
		}
		
		return $cars_for_admin_preview;
	}
	
	/**
	 * @param int $post_id
	 *
	 * @return void
	 */
	private function delete( $post_id ) {
		$this->delete_post_media( $post_id );
		wp_delete_post( $post_id, true );
	}
	
	/**
	 * @param array $handled
	 *
	 * @return void
	 */
	private function delete_except( $handled ) {
		$args = [
			'post_type'    => 'car',
			'post__not_in' => $handled,
		];
		
		$posts = get_posts( $args );
		foreach ( $posts as $post ) {
			$this->delete( $post->ID );
		}
	}
	
	/**
	 * Delete attachments for posts
	 *
	 * @param int $post_id
	 *
	 * @return void
	 */
	private function delete_post_media( $post_id ) {
		
		$attachments = get_posts( [
			'post_type'      => 'car',
			'posts_per_page' => - 1,
			'post_status'    => 'any',
			'post_parent'    => $post_id
		] );
		
		foreach ( $attachments as $attachment ) {
			if ( false !== $attachment->ID ) {
				wp_delete_attachment( $attachment->ID );
			}
		}
	}
	
	/**
	 * Break point for features list
	 *
	 * @param array $features
	 * @param int $first_break_point
	 *
	 * @return string
	 */
	private function first_break_point( $features, $first_break_point ) {
		$html                      = '';
		$first_break_point_counter = 0;
		foreach ( $features as $feature ) {
			if ( $first_break_point_counter <= $first_break_point ) {
				$feature = str_replace( '-', ' ', $feature );
				$html    .= '<li class="offer-feature-item">';
				$html    .= '<i class="fas fa-check-circle"></i>';
				$html    .= esc_html( ucwords( $feature ) );
				$html    .= '</li>';
			}
			$first_break_point_counter ++;
		}
		
		return $html;
	}
	
	/**
	 * Break point for features list
	 *
	 * @param array $features
	 * @param int $second_break_point
	 * @param int $third_break_point
	 *
	 * @return string
	 */
	private function second_break_point( $features, $second_break_point, $third_break_point ) {
		$html                       = '';
		$second_break_point_counter = 0;
		foreach ( $features as $feature ) {
			if ( $second_break_point_counter <= $second_break_point && $second_break_point_counter >= $third_break_point ) {
				$feature = str_replace( '-', ' ', $feature );
				$html    .= '<li class="offer-feature-item">';
				$html    .= '<i class="fas fa-check-circle"></i>';
				$html    .= esc_html( ucwords( $feature ) );
				$html    .= '</li>';
				
			}
			$second_break_point_counter ++;
		}
		
		return $html;
	}
	
	/**
	 * Break point for features list
	 *
	 * @param array $features
	 * @param int $fourth_break_point
	 *
	 * @return string
	 */
	private function third_break_point( $features, $fourth_break_point ) {
		$html                      = '';
		$third_break_point_counter = 0;
		foreach ( $features as $feature ) {
			if ( $third_break_point_counter <= count( $features ) && $third_break_point_counter >= $fourth_break_point ) {
				$feature = str_replace( '-', ' ', $feature );
				$html    .= '<li class="offer-feature-item">';
				$html    .= '<i class="fas fa-check-circle"></i>';
				$html    .= esc_html( ucwords( $feature ) );
				$html    .= '</li>';
				
			}
			$third_break_point_counter ++;
		}
		
		return $html;
	}
	
	/**
	 * Display otomoto announcement features in list mode
	 *
	 * @param array $features
	 *
	 * @return string
	 */
	private function otomoto_features( $features ) {
		$html               = '';
		$first_break_point  = (int) round( count( $features ) / 3 );
		$second_break_point = count( $features ) - $first_break_point;
		$third_break_point  = (int) $first_break_point + 1;
		$fourth_break_point = (int) $second_break_point + 1;
		
		if ( isset( $features ) ) {
			$html .= '<div class="offer-features">';
			$html .= '<h4 class="offer-features-title">' . __( 'Wyposażenie', 'inspirelabs-import-otomoto' ) . '</h4>';
			
			/* First break point */
			$html .= '<ul class="offer-features-list">';
			$html .= $this->first_break_point( $features, $first_break_point );
			$html .= '</ul>';
			/* End first break point */
			
			/* Second break point */
			$html .= '<ul class="offer-features-list">';
			$html .= $this->second_break_point( $features, $second_break_point, $third_break_point );
			$html .= '</ul>';
			/* End second break point */
			
			/* Third break point */
			$html .= '<ul class="offer-features-list">';
			$html .= $this->third_break_point( $features, $fourth_break_point );
			$html .= '</ul>';
			/* End third break point */
			$html .= '</div>';
		}
		
		return wp_kses( $html, $this->allowed_html() );
	}
	
	/**
	 * Display otomoto announcement features in list mode
	 *
	 * @param array $images
	 * @param int $post_id
	 * @param string $title
	 *
	 * @return string
	 */
	private function otomoto_images( $images, $title, $post_id = null ) {
		$html = '';
		if ( isset( $images ) ) {
			$html .= '<div class="wpview wpview-wrap otomoto-admin-gallery-images" data-wpview-text="[post_id=' . esc_attr( $post_id ) . ']" data-wpview-type="gallery" data-mce-selected="1" contenteditable="false">';
			$html .= '<h2 class="font-weight-light text-center text-lg-left mt-4 mb-0">' . esc_attr( 'Galeria zdjęć' ) . '</h2>';
			$html .= '<div class="gallery gallery-columns-3 otomoto-gallery-images">';
			foreach ( $images as $gallery_images => $image ) {
				$html .= '<dl class="gallery-item otomoto-gallery-image">';
				$html .= '<dt class="gallery-icon">';
				$html .= '<a href="' . esc_url( $image['large_image'] ) . '" title="' . esc_attr( $title ) . '">';
				$html .= '<img width="300" height="220" class="img-thumbnail" src="' . esc_url( $image['thumbnail'] ) . '" alt="' . esc_attr( $title ) . '" />';
				$html .= '</a>';
				$html .= '</dt>';
				$html .= '</dl>';
			}
			$html .= '<br style="clear: both;">';
			$html .= '</div>';
			$html .= '			<span class="wpview-end"></span>';
			$html .= '</div>';
		}
		
		return wp_kses( $html, $this->allowed_html() );
	}
	
	/**
	 * Insert data to Otomoto custom post type
	 *
	 * @param array $post_data
	 *
	 * @return bool|int|\WP_Error
	 */
	private function insert_data_to_otomoto_post( $post_data ) {
		$post_id = wp_insert_post( $post_data, true );
		
		if ( is_wp_error( $post_id ) ) {
			$message = [
				'message'     => __( 'Something went wrong. Please try again later', 'inspirelabs-import-otomoto' ),
				'description' => __( 'The post with ID: ' . esc_attr( $post_id ) . ' does not exists', 'inspirelabs-import-otomoto' )
			];
			
			do_action( 'otomoto_notices', $message );
			
			return false;
		}
		
		return $post_id;
	}
	
	/**
	 * Update Otomoto post
	 *
	 * @param array $post_data
	 * @param int $id (post ID)
	 *
	 * @return bool|int|\WP_Error
	 */
	private function update_otomoto_post( $post_data, $id ) {
		$post_data['ID'] = $id;
		$post_id         = wp_update_post( $post_data, true );
		if ( is_wp_error( $post_id ) ) {
			$message = [
				'message'     => __( 'Something went wrong. Please try again later', 'inspirelabs-import-otomoto' ),
				'description' => __( 'The post with ID: ' . $post_id . ' does not exists', 'inspirelabs-import-otomoto' )
			];
			
			do_action( 'otomoto_notices', $message );
			
			return false;
		}
		
		return $post_id;
	}
	
	/**
	 * Get Otomoto posts
	 *
	 * @param int $otomoto_id
	 *
	 * @return int
	 */
	private function get_otomoto_posts( $otomoto_id ) {
		$post_id = 0;
		$args    = [
			'meta_key'       => 'otomoto_id',
			'meta_value'     => $otomoto_id,
			'post_type'      => 'car',
			'post_status'    => 'any',
			'posts_per_page' => - 1
		];
		
		$posts = get_posts( $args );
		
		if ( count( $posts ) > 1 ) {
			$post_id = $posts[0]->ID;
		} elseif ( count( $posts ) === 1 ) {
			$post_id = $posts[0]->ID;
		}
		
		return $post_id;
	}
	
	/**
	 * @param array $result
	 *
	 * @return int
	 */
	private function update_or_insert( $result ) {
		$post_id                  = 0;
		$otomoto_id               = $result['id'];
		$otomoto_last_update_date = $result['last_update_date'];
		$url                      = $result['url'];
		$description              = $result['description'];
		$title                    = [];
		$make                     = $model = $version = '';
		$features                 = [];
		if ( isset( $result['params']['features'] ) ) {
			$features = $result['params']['features'];
			foreach ( $features as $key => $value ) {
				foreach ( $this->translate_features() as $feature => $translate ) {
					if ( $value === $feature ) {
						$features[ $key ] = str_replace( $features[ $key ], esc_attr( $translate ), $features[ $key ] );
					}
				}
			}
		}
		if ( '' === $features[0] ) {
			unset( $features[0] );
		}
		
		
		$post_meta_data = [
			'otomoto_id'                          => $otomoto_id,
			'otomoto_url'                         => $url,
			'otomoto_brand'                       => '',
			'otomoto_model'                       => '',
			'otomoto_wersja'                      => '',
			'otomoto_data_ostatniej_aktualizacji' => $otomoto_last_update_date,
			'otomoto_status'                      => $result['status'],
			'otomoto_opis'                        => $result['description'],
			'otomoto_dodatkowe_funkcje'           => implode(
				' ',
				array_map(
					function ( $features ) {
						return ( html_entity_decode( '&#9745;' ) . ' ' . $features );
					},
					$features
				)
			),
			'otomoto_kolor'                       => ucfirst( $result['params']['color'] ),
			'otomoto_typ_paliwa'                  => $result['params']['fuel_type'],
			'otomoto_moc_silnika'                 => $result['params']['engine_power'],
			'otomtoto_skrzynia_biegow'            => $result['params']['gearbox'],
			'otomoto_pojemnosc_silnika'           => $result['params']['engine_capacity'],
			'otomoto_nazwa_samochodu'             => ucfirst( $result['params']['make'] ) . ' ' . ucfirst( $result['params']['model'] )
		];
		
		if ( array_key_exists( 'params', $result ) ) {
			if ( array_key_exists( 'make', $result['params'] ) ) {
				$title[] = $post_meta_data['otomoto_brand'] = ucfirst( $result['params']['make'] );
			}
			if ( array_key_exists( 'model', $result['params'] ) ) {
				$title[] = $post_meta_data['otomoto_model'] = ucfirst( $result['params']['model'] );
			}
			
			if ( array_key_exists( 'version', $result['params'] ) ) {
				$version = $result['params']['version'];
			}
			
			$post_meta_data['otomoto_rok']      = $result['params']['year'];
			$post_meta_data['otomoto_przebieg'] = $result['params']['mileage'];
			$post_meta_data['otomoto_wersja']   = $version;
			if ( array_key_exists( 'price', $result['params'] ) ) {
				$post_meta_data['otomoto_cena']       = $result['params']['price'][1] . ' ' . $result['params']['price']['currency'];
				$post_meta_data['otomoto_cena_netto'] = $result['params']['price']['gross_net'] == 'net' ? 1 : 0;
			}
			
		}
		$title[] = $result['title'];
		$title   = implode( " ", $title );
		
		$post_data = [
			'post_type'      => 'car',
			'post_title'     => esc_attr( $title ),
			'post_content'   => $description . $this->otomoto_features( str_replace( ' .', '', $features ) ),
			'post_status'    => 'publish',
			'comment_status' => 'closed',
			'ping_status'    => 'closed',
		];
		$post_id   = $this->get_otomoto_posts( $otomoto_id );
		
		$announcement_update_date = get_post_meta( $post_id, 'otomoto_data_ostatniej_aktualizacji', true );
		$last_update_date         = $result['last_update_date'];
		
		$post_update_date           = self::create_date_format_from_announcement_update_date( $announcement_update_date );
		$announcement_retrieve_date = self::create_date_format_from_announcement_update_date( $last_update_date );
		
		if ( $post_id === 0 ) {
			$post_id = $this->insert_data_to_otomoto_post( $post_data );
		} else {
			if ( $announcement_retrieve_date > $post_update_date ) {
				$post_id = $this->update_otomoto_post( $post_data, $post_id );
			}
		}
		
		
		if ( array_key_exists( 'image_collection_id', $result ) ) {
			reset( $result['photos'] );
			$first_key = key( $result['photos'] );
			$image_key = key( $result['photos'][ $first_key ] );
			$image_url = $result['photos'][ $first_key ][ $image_key ];
			
			$attach_id = $this->otomoto_insert_attachment_from_url( $image_url, $post_id );
			$images    = $this->retrieve_images_from_response( $result );
			add_image_size( 'otomoto-thumbnails', 260, 195, true );
			if ( $attach_id !== false ) {
				if ( get_option( 'otomoto_create_gallery' ) === 'on' ) {
					$gallery_images = $this->add_images_for_gallery( $images, $post_id, $post_data );
					$this->insert_images_to_post( $gallery_images, $title, $post_id, $description, $features );
				}
			} else {
				unset( $post_meta_data['otomoto_data_ostatniej_aktualizacji'] );
			}
		}
		
		$this->update_otomoto_meta_data( $post_meta_data, $post_id );
		$status = get_post_meta( $post_id, 'otomoto_status', true );
		$this->set_post_post_private_if_status_is_not_active( $status, $post_id );
		
		return $post_id;
	}
	
	function add_custom_gallery( $images, $post_id ) {
		$ids = array();
		foreach ( $images as $gallery_images => $image ) {
			$image_url = $image['large_image'];
			
			
			$file_array = [ 'name' => wp_basename( $image_url ), 'tmp_name' => download_url( $image_url ) ];
			
			
			if ( is_wp_error( $file_array['tmp_name'] ) ) {
				return $file_array['tmp_name'];
			}
			
			$ids[] = media_handle_sideload( $file_array, $post_id );
		}
		
		return $ids;
	}
	
	/**
	 * Set post status private if Otomoto returns announcement status as removed or outdated
	 *
	 * @param string $status
	 * @param int $post_id
	 *
	 * @return void
	 */
	private function set_post_post_private_if_status_is_not_active( $status, $post_id ) {
		if ( 'active' !== $status ) {
			$_post = get_post( $post_id );
			if ( 'car' === $_post->post_type ) {
				if ( 'private' !== $_post->post_status ) {
					$_post->post_status = 'private';
					wp_update_post( $_post );
				}
			}
		}
	}
	
	/**
	 *
	 * Create date format from announcement to compare before update post
	 *
	 * @param string $date
	 *
	 * @return \DateTime|false
	 *
	 * @throws \Exception  Emits Exception in case of an error.
	 */
	public static function create_date_format_from_announcement_update_date( $date ) {
		$date_time = new \DateTime();
		if ( $date_time ) {
			return $date_time::createFromFormat( 'Y-m-d H:i:s', $date );
		}
		
		return false;
	}
	
	/**
	 * Add images for gallery
	 *
	 * @param array $images
	 * @param int $post_id
	 * @param array $post_data
	 *
	 * @retrun void|array
	 */
	private function add_images_for_gallery( $images, $post_id, $post_data ) {
		$gallery_images = [];
		if ( isset( $images ) ) {
			foreach ( $images as $image ) {
				$gallery_images[] = [
					'post_id'          => esc_attr( $post_id ),
					'thumbnail'   => esc_url( $image['thumbnail'] ),
					'large_image' => esc_url( $image['large_image'] ),
					'title'       => esc_attr( $post_data['post_title'] ),
				];
			}
			
			update_post_meta( $post_id, '_otomoto_gallery_images', $gallery_images );
		}
		
		return $gallery_images;
	}
	
	/**
	 * Update meta data for Otomoto
	 *
	 * @param array $post_meta_data
	 * @param int $post_id
	 *
	 * @retrun void
	 */
	private function update_otomoto_meta_data( $post_meta_data, $post_id ) {
		foreach ( $post_meta_data as $post_meta => $post_meta_value ) {
			if ( get_post_meta( $post_id, $post_meta, false ) ) {
				update_post_meta( $post_id, $post_meta, $post_meta_value );
			} else {
				add_post_meta( $post_id, $post_meta, $post_meta_value );
			}
		}
	}
	
	/**
	 * Retrieve images to add in post for gallery
	 *
	 * @param array $result
	 *
	 * @return array
	 */
	private function retrieve_images_from_response( $result ) {
		$images     = [];
		$dimensions = [
			'thumbnail'   => '320x240',
			'large_image' => '1080x720'
		];
		
		if ( ! empty( $result['photos'] ) ) {
			foreach ( $result['photos'] as $photos => $photo ) {
				foreach ( $photo as $key => $value ) {
					if ( $photo[ $dimensions['thumbnail'] ] === $photo[ $key ] ) {
						$images[] = [
							'thumbnail'   => $value,
							'large_image' => $photo[ $dimensions['large_image'] ],
						];
					}
				}
			}
		}
		
		return $images;
	}
	
	/**
	 * Insert images to post to create gallery later
	 *
	 * @param array $images
	 * @param int $post_id
	 * @param string $description
	 * @param array $features
	 * @param string $title
	 *
	 * @return void
	 */
	private function insert_images_to_post( $images, $title, $post_id, $description = '', $features = [] ) {
		if ( null !== $post_id ) {
			if ( ! empty( $images ) ) {
				
				$image_gallery = $this->otomoto_images( $images, $title, $post_id );
				
				$otomoto_announcements = [
					'ID'           => $post_id,
					'post_content' => $image_gallery . $description . $this->otomoto_features( str_replace( ' .', '', $features ) ),
				];
				wp_update_post( $otomoto_announcements );
			}
		}
	}
	
	/**
	 * Insert image from otomoto API
	 *
	 * @param string $url
	 * @param int|null $post_id
	 *
	 * @return bool|int|\WP_Error
	 */
	private function otomoto_insert_attachment_from_url( $url, $post_id = null ) {
		
		if ( ! class_exists( 'WP_Http' ) ) {
			include_once( ABSPATH . WPINC . '/class-http.php' );
		}
		$http     = new WP_Http();
		$response = $http->request( $url );
		
		if ( is_wp_error( $response ) ) {
			return false;
		}
		if ( $response['response']['code'] !== 200 ) {
			return false;
		}
		
		$upload = wp_upload_bits( basename( $url ), null, $response['body'] );
		
		if ( ! empty( $upload['error'] ) ) {
			return false;
		}
		
		$file_path = $upload['file'];
		if ( '' === pathinfo( basename( $file_path ), PATHINFO_EXTENSION ) ) {
			$file_name = exif_imagetype( $file_path );
			switch ( $file_name ) {
				case 1 :
					$file_name .= '.gif';
					break;
				case 2 :
					$file_name .= '.jpg';
					break;
				case 3 :
					$file_name .= '.png';
					break;
				default :
					$file_name = 'no_image.png';
			}
			
			$attachment_title = sanitize_file_name( pathinfo( $file_path, PATHINFO_FILENAME ) );
		} else {
			$file_name        = basename( $file_path );
			$attachment_title = sanitize_file_name( pathinfo( $file_name, PATHINFO_FILENAME ) );
		}
		$file_type = wp_check_filetype( $file_name, null );
		
		$wp_upload_dir = wp_upload_dir();
		
		$post_info = [
			'guid'           => $wp_upload_dir['url'] . '/' . $file_name,
			'post_mime_type' => $file_type['type'],
			'post_title'     => $attachment_title,
			'post_content'   => '',
			'post_status'    => 'inherit',
		];
		
		$attach_id = wp_insert_attachment( $post_info, $file_path, $post_id );
		
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );
		
		// Crop image
		$attach_data['width']  = 780;
		$attach_data['height'] = 520;
		
		wp_update_attachment_metadata( $attach_id, $attach_data );
		
		set_post_thumbnail( $post_id, $attach_id );
		
		return $attach_id;
	}
	
	/**
	 * Manual import from Otomoto
	 *
	 * @param array $adverts
	 *
	 * @return array|bool
	 */
	private function manual_otomoto_import( $adverts ) {
		if ( ! is_admin() ) {
			return false;
		}
		$count_active          = 0;
		$count_outdated        = 0;
		$count_removed_by_user = 0;
		if ( isset( $adverts ) ) {
			if ( ! empty( $adverts['results'] ) ) {
				foreach ( $adverts['results'] as $key => $value ) {
					
					if ( $value['status'] == 'active' ) {
						$count_active ++;
					} elseif ( $value['status'] == 'outdated' ) {
						$count_outdated ++;
					} elseif ( $value['status'] == 'removed_by_user' ) {
						$count_removed_by_user ++;
					}
					
					$i              = 0;
					$main_car_image = '';
					if ( isset( $value['photos'] ) ) {
						foreach ( $value['photos'] as $photos ) {
							if ( $i > 0 ) {
								break;
							}
							$main_car_image = $photos['148x110']; // Dimension from Otomoto
							$i ++;
						}
					}
					
					$features = [];
					if ( isset( $value['params']['features'] ) ) {
						$features = $value['params']['features'];
					}
					
					$cars_for_admin_preview['cars'][ $key ] = [
						'image'       => $main_car_image,
						'status'      => $value['status'],
						'make'        => $value['params']['make'],
						'model'       => $value['params']['model'],
						'year'        => $value['params']['year'],
						'mileage'     => $value['params']['mileage'],
						'price'       => $value['params']['price'][1] . ' ' . $value['params']['price']['currency'],
						'title'       => $value['title'],
						'description' => substr( $value['description'], 0, 200 ) . '...',
						'features'    => $features,
					];
				}
			} else {
				$errors = array(
					'message'     => __( 'Nie było nic do zaimportowania', 'inspirelabs-import-otomoto' ),
					'description' => __( 'Prawdopodobnie nie ma żadnych ogłoszeń, proszę sprawdzić w panelu Otomoto', 'inspirelabs-import-otomoto' ),
				);
				if ( ! empty( $adverts ) ) {
					if ( isset( $adverts['error'] ) ) {
						$errors['message']     = __( 'Invalid token.', 'inspirelabs-import-otomoto' );
						$errors['description'] = __( 'Token is invalid and/or expired', 'inspirelabs-import-otomoto' );
					}
				}
				do_action( 'otomoto_notices', $errors );
			}
		}
		
		
		$cars_for_admin_preview['statuses']['active']          = $count_active;
		$cars_for_admin_preview['statuses']['outdated']        = $count_outdated;
		$cars_for_admin_preview['statuses']['removed_by_user'] = $count_removed_by_user;
		
		return $cars_for_admin_preview;
	}
}
