<?php
/**
 * Class OtomotoSettingsHtml .
 *
 * @package InspireLabsOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoSettingsHtml;

class OtomotoSettingsHtml {
	/**
	 * @param array $args
	 *
	 * @return void
	 */
	public static function generate_form_fields_for_otomoto_user_settings( $args ) {
		$html = '';
		if ( isset( $args ) ) {
			if ( 'ok' === esc_attr( get_option( 'otomoto_api_status' ) ) ) {
				$api_status = '<span class="api-success">' . __( 'OK', 'inspirelabs-import-otomoto' ) . '</span>';
			} else {
				$api_status = '<span class="api-fail">' . __( 'Not Connected', 'inspirelabs-import-otomoto' ) . '</span>';
			}
			
			$value = esc_attr( get_option( $args['value'] ) );
			switch ( $args['value'] ) {
				case 'otomoto_api_test_mode' :
					$enable = esc_attr( get_option( $args['value'] ) );
					if ( $enable == 'on' ) {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$html .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'Enable Test Mode', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html .= '<td><input type="checkbox" class=""
	                id="' . esc_attr( $args['value'] ) . '"  name="' . esc_attr( $args['value'] ) . '" ' . $checked . ' />
	                <small>' . __( 'Check the checkbox if you want to test the integration with Otomoto', 'inspirelabs-import-otomoto' ) . '</small></td></tr>';
					
					break;
				case 'otomoto_api_schedule_import' :
					$enable = esc_attr( get_option( $args['value'] ) );
					if ( $enable == 'on' ) {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$html .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'Enable Schedule Import', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html .= '<td><input type="checkbox" class=""
	                id="' . esc_attr( $args['value'] ) . '"  name="' . esc_attr( $args['value'] ) . '" ' . $checked . ' />
	                <small>' . __( 'Check the checkbox if you want to enable schedule import of announcements', 'inspirelabs-import-otomoto' ) . '</small></td></tr>';
					
					break;
				case 'otomoto_api_schedule_import_time' :
					$html .= '<tr class="schedule-import"><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'Set Schedule Time', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html .= '<td>';
					$html .= '<select id="' . esc_attr( $args['value'] ) . '" name="' . esc_attr( $args['value'] ) . '">';
					$html .= self::generate_time_options_html( $value );
					$html .= '</select>';
					$html .= '<p><small class="helper-tooltip">' . sprintf( __( '', 'inspirelabs-import-otomoto' ) ) . '</small></p><td></tr>';
					break;
				case 'otomoto_api_username' :
					$html .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'API Username', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html .= '<td><input type="text" class="" size="35"
	                id="' . esc_attr( $args['value'] ) . '" name="' . esc_attr( $args['value'] ) . '"
	                value="' . esc_attr( $value ) . '" />
	                <p><small class="helper-tooltip">' . sprintf( __( 'Otomoto API User Name.&nbsp;&nbsp;<a href="%s"> Click here</a>&nbsp;&nbsp; to find out how to get API Credentials for Otomoto', 'inspirelabs-import-otomoto' ), esc_url( 'https://pomoc.otomoto.pl/hc/pl/articles/206872877-Zasady-udzielania-dost%C4%99pu-do-nowego-API' ) ) . '</small></p></td></tr>';
					
					break;
				case 'otomoto_api_password' :
					$html .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'API Password', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html .= '<td><input type="text" class="" size="35"
	                id="' . esc_attr( $args['value'] ) . '" name="' . esc_attr( $args['value'] ) . '"
	                value="' . esc_attr( $value ) . '" />
	                <p><small class="helper-tooltip">' . sprintf( __( 'Otomoto API Password.&nbsp;&nbsp;<a href="%s"> Click here</a>&nbsp;&nbsp; to find out how to get API Credentials for Otomoto', 'inspirelabs-import-otomoto' ), esc_url( 'https://pomoc.otomoto.pl/hc/pl/articles/206872877-Zasady-udzielania-dost%C4%99pu-do-nowego-API' ) ) . '</small></p></td></tr>';
					
					break;
				case 'otomoto_api_client_id' :
					$html .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'API Client ID', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html .= '<td><input type="text" class="" size="35"
	                id="' . esc_attr( $args['value'] ) . '" name="' . esc_attr( $args['value'] ) . '"
	                value="' . esc_attr( $value ) . '" />
	                <p><small class="helper-tooltip">' . sprintf( __( 'Otomoto API Client ID.&nbsp;&nbsp;<a href="%s"> Click here</a>&nbsp;&nbsp; to find out how to get API Credentials for Otomoto', 'inspirelabs-import-otomoto' ), esc_url( 'https://pomoc.otomoto.pl/hc/pl/articles/206872877-Zasady-udzielania-dost%C4%99pu-do-nowego-API' ) ) . '</small></p></td></tr>';
					
					break;
				
				case 'otomoto_api_client_secret' :
					$html .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'API Client Secret', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html .= '<td><input type="text" class="" size="35"
	                id="' . esc_attr( $args['value'] ) . '" name="' . esc_attr( $args['value'] ) . '"
	                value="' . esc_attr( $value ) . '" />
	                <p><small class="helper-tooltip">' . sprintf( __( 'Otomoto API Client Secret.&nbsp;&nbsp;<a href="%s"> Click here</a>&nbsp;&nbsp; to find out how to get API Credentials for Otomoto', 'inspirelabs-import-otomoto' ), esc_url( 'https://pomoc.otomoto.pl/hc/pl/articles/206872877-Zasady-udzielania-dost%C4%99pu-do-nowego-API' ) ) . '</small></p></td></tr>';
					
					break;
				case 'otomoto_create_gallery' :
					$enable = esc_attr( get_option( $args['value'] ) );
					if ( $enable == 'on' ) {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$html .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'Enable image gallery import in post content', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html .= '<td><input type="checkbox" class=""
	                id="' . esc_attr( $args['value'] ) . '"  name="' . esc_attr( $args['value'] ) . '" ' . $checked . ' />
	                <small>' . __( 'Check the checkbox if you want to enable gallery import in post content. When this option is checked images are imported in admin panel: <strong>Cars -> All cars -> Edit</strong>.
	                        Please note that gallery styling may require', 'inspirelabs-import-otomoto' ) . '</small></td></tr>';
					
					break;
				case 'otomoto_api_status' :
					$html .= '<tr><th class="api-status" scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'API Connection Status: ', 'inspirelabs-import-otomoto' ) . '</label>' . $api_status . '</th></tr>';
					break;
				case 'otomoto_posts_count' :
					self::sanitize_input_values( $value, $args, 'Value for announcements per page is incorrect (Must be greater then 0)' );
					
					$otomoto_post_per_page = get_option( $args['value'] );
					$html                  .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'Number of announcements to display', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html                  .= '<td><input type="number" class="" size="35" min="5" max="1000"
	                id="' . esc_attr( $args['value'] ) . '" name="' . esc_attr( $args['value'] ) . '"
	                value="' . esc_attr( $otomoto_post_per_page ) . '" /><br />
	                <small>' . __( 'Set the number of announcements to display (default 5)', 'inspirelabs-import-otomoto' ) . '</small></td></tr>';
					
					break;
				case 'otomoto_announcements_to_import' :
					self::sanitize_input_values( $value, $args, 'Value for announcements to import is incorrect (Must be greater then 0)' );
					
					$otomoto_announcements_to_import = get_option( $args['value'] );
					$html                            .= '<tr><th scope="row"><label for="' . esc_attr( $args['value'] ) . '">' . __( 'Number of announcements to import', 'inspirelabs-import-otomoto' ) . '</label></th>';
					$html                            .= '<td><input type="number" class="" size="35" min="1" max="1000"
	                id="' . esc_attr( $args['value'] ) . '" name="' . esc_attr( $args['value'] ) . '"
	                value="' . esc_attr( $otomoto_announcements_to_import ) . '" /><br />
	                <small>' . __( 'Set the number of announcements to import', 'inspirelabs-import-otomoto' ) . '</small></td></tr>';
					
					break;
				default :
					$html .= '<h1>' . esc_html_e( 'No fields found', 'inspirelabs-import-otomoto' ) . '</h1>';
					break;
			}
		}
		
		echo $html;
	}
	
	/**
	 * Sanitize input fields for digit values
	 *
	 * @param string $value
	 * @param array $args
	 * @param string message
	 *
	 * @return void
	 */
	private static function sanitize_input_values( $value, $args, $message = '' ) {
		if ( '' !== $value ) {
			if ( ! ctype_digit( $value ) || (int) $value === 0 ) {
				if ( isset( $_GET['settings-updated'] ) ) {
					add_settings_error( 'otomoto_settings_error', 'Otomoto Message',
						__( $message, 'inspirelabs-import-otomoto' ), 'error' );
					update_option( $args['value'], '' );
				}
				settings_errors( 'otomoto_settings_error' );
			}
		}
	}
	
	/**
	 * Schedule import time
	 *
	 * @return array
	 */
	private static function import_schedule_time() {
		return array(
			'select_time' => __('Select schedule import', 'inspirelabs-import-otomoto'),
			'hourly'     => __( 'Hourly', 'inspirelabs-import-otomoto' ),
			'twicedaily' => __( 'Twice a day', 'inspirelabs-import-otomoto' ),
			'weekly'     => __( 'Once a week', 'inspirelabs-import-otomoto' )
		);
	}
	
	/**
	 * Generate schedule import time
	 *
	 * @param string $value
	 *
	 * @return string
	 */
	private static function generate_time_options_html( $value ) {
		$html = '';
		foreach ( self::import_schedule_time() as $schedule => $time ) {
			$html .= '<option value="' . esc_attr( $schedule ) . '" ' . selected( $value, $schedule, false ) . '>' . esc_attr( $time ) . '</option>';
		}
		
		return $html;
	}
	
	/**
	 * @param array $args
	 * @param string $import_hash
	 * @param string $display_clean_button
	 * @param string $disable_import
	 *
	 * @return void
	 */
	public static function generate_import_button_for_otomoto( $args, $import_hash = '', $display_clean_button = 'none', $disable_import = '' ) {
		$html = '';
		if ( isset( $args ) ) {
			if ( $args['value'] === 'otomoto_api_import_data' ) {
				$html .= '<tr>
							<td><a class="button button-primary" href="' . esc_url( admin_url( 'admin.php?page=otomoto_settings_page&tab=import_otomoto&import_hash=' . urlencode( esc_attr( $import_hash ) ) . '' ) ) . '">' . __( 'Import', 'inspirelabs-import-otomoto' ) . '</a></td>
							<td><a class="button button-secondary" style="visibility:' . $display_clean_button . '" href="' . esc_url( admin_url( 'admin.php?page=otomoto_settings_page&tab=import_otomoto&clean_data=clean_cache' ) ) . '">' . __( 'Clean data', 'inspirelabs-import-otomoto' ) . '</a></td>
						 </tr>';
				$html .= '';
			}
		}
		
		echo $html;
	}
	
	/**
	 * Otomoto data table
	 *
	 * @param array $cars_for_admin_preview
	 *
	 * @return void
	 */
	public static function otomoto_data_table( $cars_for_admin_preview = array() ) {
		$html = '';
		if ( isset( $cars_for_admin_preview ) ) {
			if ( isset( $cars_for_admin_preview['cars'] ) ) {
				$html .= '
				 <thead>
		            <tr>
		                <th class="manage-column">' . __( 'Photo', 'inspirelabs-import-otomoto' ) . '</th>
		                <th class="manage-column">' . __( 'Status', 'inspirelabs-import-otomoto' ) . '</th>
		                <th class="manage-column">' . __( 'Brand', 'inspirelabs-import-otomoto' ) . '</th>
		                <th class="manage-column">' . __( 'Model', 'inspirelabs-import-otomoto' ) . '</th>
		                <th class="manage-column">' . __( 'Year', 'inspirelabs-import-otomoto' ) . '</th>
		                <th class="manage-column">' . __( 'Mileage', 'inspirelabs-import-otomoto' ) . '</th>
		                <th class="manage-column">' . __( 'Price', 'inspirelabs-import-otomoto' ) . '</th>
		                <th class="manage-column">' . __( 'Description', 'inspirelabs-import-otomoto' ) . '</th>
		                <th class="manage-column">' . __( 'Title', 'inspirelabs-import-otomoto' ) . '</th>
		            </tr>
            	</thead>
				';
				
				foreach ( $cars_for_admin_preview['cars'] as $car ) {
					$html .= '
							<tbody>
							<tr>
							<td class="manage-column  check-column"><img alt="image" width="120" height="70" src="' . esc_url( $car['image'] ) . '"></td>
		                    <td class="manage-column  check-column">' . esc_attr( $car['status'] ) . '</td>
		                    <td class="manage-column  check-column">' . esc_attr( $car['make'] ) . '</td>
		                    <td class="manage-column  check-column">' . esc_attr( $car['model'] ) . '</td>
		                    <td class="manage-column  check-column">' . esc_attr( $car['year'] ) . '</td>
		                    <td class="manage-column  check-column">' . esc_attr( $car['mileage'] ) . '</td>
		                    <td class="manage-column  check-column">' . esc_attr( $car['price'] ) . '</td>
		                    <td class="manage-column  check-column">' . esc_attr( $car['description'] ) . '</td>
		                    <td class="manage-column  check-column">' . esc_attr( $car['title'] ) . '</td>
		                    </tr>
		                    </tbody>
						';
				}
			} else {
				$html .= '<td class="description">' . esc_html_e( 'There was nothing to import, please click "Clean data" (Cache file) and try again later', 'inspirelabs-import-otomoto' ) . '</td>';
			}
		}
		echo $html;
	}
}
