<?php
/**
 * Class AllowedHtml .
 *
 * @package InspireLabsOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\AllowedHtml;


trait AllowedHtml {
	/**
	 * Allowed html output
	 *
	 * @return array
	 */
	public function allowed_html() {
		return array(
			'a'     => array(
				'class' => array(),
				'href'  => array(),
				'rel'   => array(),
				'title' => array(),
			),
			'div'   => array(
				'class' => array(),
				'title' => array(),
				'style' => array(),
			),
			'img'   => array(
				'alt'    => array(),
				'class'  => array(),
				'height' => array(),
				'src'    => array(),
				'width'  => array(),
			),
			'span'  => array(
				'class' => array(),
				'title' => array(),
				'style' => array(),
			),
			'table' => array(
				'class' => array(),
			),
			'thead' => array(
				'class' => array(),
			),
			'th'    => array(
				'class' => array(),
			),
			'tr'    => array(
				'class' => array(),
			),
			'td'    => array(
				'class' => array(),
			),
			'p'     => array(
				'class' => array(),
				'id'    => array(),
			),
			'h1'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'h2'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'h3'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'h4'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'dl'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'dt'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'figure'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'i'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'ul'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'li'    => array(
				'class' => array(),
				'id'    => array(),
			),
		);
	}
}