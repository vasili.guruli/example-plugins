<?php
/**
 * Class OtomotoApiCredentials .
 *
 * @package InspireLabsOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoApiCredentials;


trait OtomotoApiCredentials {
	/**
	 * Get appropriate data from settings
	 *
	 * @return array
	 */
	public function get_appropriate_settings_data_for_otomoto() {
		$api_url = $this->api_url();
		
		return [
			'auth_url'      => esc_url( $api_url['api_token_endpoint'] ),
			'api_url'       => esc_url( $api_url['api_url'] ),
			'user_id'       => esc_attr( get_option( 'otomoto_api_client_id' ) ),
			'user_name'     => esc_attr( get_option( 'otomoto_api_username' ) ),
			'user_password' => esc_attr( get_option( 'otomoto_api_password' ) ),
			'user_secret'   => esc_attr( get_option( 'otomoto_api_client_secret' ) ),
		];
	}
	
	/**
	 * Otomoto API Endpoints
	 *
	 * @return array
	 */
	protected function api_url() {
		$sandbox      = esc_attr( get_option( 'otomoto_api_test_mode' ) );
		$import_limit = esc_attr( get_option( 'otomoto_announcements_to_import' ) );
		if ( 'on' === $sandbox ) {
			$otomoto_api_endpoints = [
				'api_url'            => 'https://sbotomotopl.playground.lisbontechhub.com/api/open/account/adverts?limit=' . esc_attr( $import_limit ) . '&page=1',
				'api_token_endpoint' => 'https://sbotomotopl.playground.lisbontechhub.com/api/open/oauth/token'
			];
		} else {
			$otomoto_api_endpoints = [
				'api_url'            => 'https://www.otomoto.pl/api/open/account/adverts?limit=' . esc_attr( $import_limit ) . '&page=1',
				'api_token_endpoint' => 'https://www.otomoto.pl/api/open/oauth/token'
			];
		}
		
		return $otomoto_api_endpoints;
	}
}