<?php
/**
 * Class OtomotoCarPostType .
 *
 * @package InspireLabs\ImportOtomoto
 */

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoCarPostType;


class OtomotoCarPostType {

	/**
	 * Run custom post type register for Otomoto
	 *
	 * @param array $args
	 *
	 * @return void
	 */
	public function run( $args = array() ) {
		$this->hooks();
	}

	/**
	 * Hooks for custom post type init
	 *
	 * @return void
	 */
	private function hooks() {
		add_action( 'init', array( $this, 'register_otomoto_car_post_type' ), 99 );
		add_action( 'init', array( $this, 'add_car_image_size' ), 99 );
	}

	/**
	 * Register Otomoto Car post type
	 *
	 * @return void
	 */
	public function register_otomoto_car_post_type() {
		$args = array(
			'labels'             => $this->car_post_type_labels(),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'car' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ),
		);

		register_post_type( 'car', $args );
	}

	/**
	 * Car post type labels
	 *
	 * @return @void
	 */
	private function car_post_type_labels() {
		return array(
			'name'                  => _x( 'Cars', 'Post type general name', 'inspirelabs-import-otomoto' ),
			'singular_name'         => _x( 'Car', 'Post type singular name', 'inspirelabs-import-otomoto' ),
			'menu_name'             => _x( 'Cars', 'Admin Menu text', 'inspirelabs-import-otomoto' ),
			'name_admin_bar'        => _x( 'Car', 'Add New on Toolbar', 'inspirelabs-import-otomoto' ),
			'add_new'               => __( 'Add New', 'inspirelabs-import-otomoto' ),
			'add_new_item'          => __( 'Add New Car', 'inspirelabs-import-otomoto' ),
			'new_item'              => __( 'New Car', 'inspirelabs-import-otomoto' ),
			'edit_item'             => __( 'Edit Car', 'inspirelabs-import-otomoto' ),
			'view_item'             => __( 'View Car', 'inspirelabs-import-otomoto' ),
			'all_items'             => __( 'All Cars', 'inspirelabs-import-otomoto' ),
			'search_items'          => __( 'Search Cars', 'inspirelabs-import-otomoto' ),
			'parent_item_colon'     => __( 'Parent Cars:', 'inspirelabs-import-otomoto' ),
			'not_found'             => __( 'No Cars found.', 'inspirelabs-import-otomoto' ),
			'not_found_in_trash'    => __( 'No Cars found in Trash.', 'inspirelabs-import-otomoto' ),
			'featured_image'        => _x( 'Car Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'inspirelabs-import-otomoto' ),
			'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'inspirelabs-import-otomoto' ),
			'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'inspirelabs-import-otomoto' ),
			'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'inspirelabs-import-otomoto' ),
			'archives'              => _x( 'Car archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'inspirelabs-import-otomoto' ),
			'uploaded_to_this_item' => _x( 'Uploaded to this Car', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'inspirelabs-import-otomoto' ),
			'filter_items_list'     => _x( 'Filter Cars list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'inspirelabs-import-otomoto' ),
			'items_list_navigation' => _x( 'Cars list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'inspirelabs-import-otomoto' ),
			'items_list'            => _x( 'Cars list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'inspirelabs-import-otomoto' ),
		);
	}

	/**
	 * Add image size for thumbnails
	 *
	 * @return void
	 */
	public function add_car_image_size() {
		add_image_size( 'otomoto-car-thumb', 407, 305 );
	}
}
