<?php

namespace InspireLabs\ImportOtomoto\OtomotoIntegration\OtomotoWidget;

class OtomotoWidget extends \WP_Widget {
	/** @var array $otomoto_custom_fields */
	private $otomoto_custom_fields;
	/** @var array $font_awesome_class */
	private $font_awesome_class;
	
	/**
	 * Otomoto Widget constructor
	 */
	public function __construct() {
		parent::__construct( 'otomoto_widget', __( 'Otomoto Widget', 'inspirelabs-import-otomoto' ), array(
			'description' => __( 'Display Otomoto custom fields', 'inspirelabs-import-otomoto' )
		), array() );
		
		$this->otomoto_custom_fields = array(
			'otomoto_rok'               => array(
				'text'  => __( 'Year', 'inspirelabs-import-otomoto' ),
				'class' => 'fa-car'
			),
			'otomoto_przebieg'          => array(
				'text'  => __( 'Mileage', 'inspirelabs-import-otomoto' ),
				'class' => 'fa-road',
			),
			'otomoto_typ_paliwa'        => array(
				'text'  => __( 'Fuel', 'inspirelabs-import-otomoto' ),
				'class' => 'fa-gas-pump',
			),
			'otomoto_moc_silnika'       => array(
				'text'  => __( 'Engine Power', 'inspirelabs-import-otomoto' ),
				'class' => 'fa-horse',
			),
			'otomoto_pojemnosc_silnika' => array(
				'text'  => __( 'Engine Capacity', 'inspirelabs-import-otomoto' ),
				'class' => 'fa-rocket',
			)
		);
		
		$this->font_awesome_class = array(
			'fa-car',
			'fa-road',
			'fa-gas-pump',
			'fa-horse',
			'fa-rocket',
		);
	}
	
	/**
	 * Echoes the widget content.
	 *
	 * Subclasses should override this function to generate their widget code.
	 *
	 * @param array $args Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance The settings for the particular instance of the widget.
	 *
	 * @since 2.8.0
	 *
	 */
	public function widget( $args, $instance ) {
		global $post;
		$otomoto_price = '';
		$otomoto_url   = '';
		if ( 'car' === $post->post_type && is_single() ) {
			
			$title = apply_filters( 'widget_title', $instance['title'] );
			
			echo $args['before_widget'];
			if ( ! empty( $title ) ) {
				echo $args['before_title'] . $title . $args['after_title'];
			}
			
			$display_price         = isset( $instance['display_price'] ) ? $instance['display_price'] : '';
			$display_url           = isset( $instance['display_url'] ) ? $instance['display_url'] : '';
			$custom_fields         = $instance['custom_fields'];
			$otomoto_brand         = get_post_meta( $post->ID, 'otomoto_nazwa_samochodu', true );
			if ( 'on' === $display_price ) {
				$otomoto_price       = get_post_meta( $post->ID, 'otomoto_cena', true );
				$otomoto_price_title = ( empty( $instance['otomoto_price_title'] ) ? '' : $instance['otomoto_price_title'] );
			}
			if ( 'on' === $display_url ) {
				$otomoto_url       = get_post_meta( $post->ID, 'otomoto_url', true );
				$otomoto_url_title = ( empty( $instance['otomoto_url_title'] ) ? '' : $instance['otomoto_url_title'] );
			}
			include( INSPIRELABS_IMPORT_OTOMOTO_TEMPLATE_DIR . 'frontend/display-widget.php' );
			echo $args['after_widget'];
		}
	}
	
	/**
	 * Outputs the settings update form.
	 *
	 * @param array $instance Current settings.
	 *
	 * @return string Default return is 'noform'.
	 * @since 2.8.0
	 *
	 */
	public function form( $instance ) {
		$otomoto_url   = '';
		$display_price = '';
		$display_url   = '';
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'Details of announcement', 'inspirelabs-import-otomoto' );
		}
		if ( isset( $instance['otomoto_url_title'] ) ) {
			$otomoto_url = $instance['otomoto_url_title'];
		}
		if ( isset( $instance['display_price'] ) ) {
			$display_price = $instance['display_price'];
		}
		if ( isset( $instance['display_url'] ) ) {
			$display_url = $instance['display_url'];
		}
		
		include( INSPIRELABS_IMPORT_OTOMOTO_TEMPLATE_DIR . 'admin/widget.php' );
	}
	
	/**
	 * Updates a particular instance of a widget.
	 *
	 * This function should check that `$new_instance` is set correctly. The newly-calculated
	 * value of `$instance` should be returned. If false is returned, the instance won't be
	 * saved/updated.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 *
	 * @return array Settings to save or bool false to cancel saving.
	 * @since 2.8.0
	 *
	 */
	public function update( $new_instance, $old_instance ) {
		$instance                          = array();
		$instance['title']                 = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['display_price']         = strip_tags( $new_instance['display_price'] );
		$instance['display_url']           = strip_tags( $new_instance['display_url'] );;
		$instance['otomoto_url_title']     = strip_tags( $new_instance['otomoto_url_title'] );
		
		foreach ( $this->otomoto_custom_fields as $key => $value ) {
			if ( isset( $key ) ) {
				$instance[ $key ]            = strip_tags( $new_instance[ $key ] );
				$instance['custom_fields'][] = [
					$key    => strip_tags( $instance[ $key ] ),
					'class' => $value['class']
				];
			}
		}
		
		return $instance;
	}
}
