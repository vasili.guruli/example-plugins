<?php
declare( strict_types=1 );

/**
 * Add appropriate template.
 *
 * @param string $slag
 * @param string $where
 * @param string $template
 *
 * @return void
 */
function add_plugin_template( $slag, $where = 'admin', $template = 'settings' ) {
	include SCAN_UPLOADED_FILES_DIR . '/templates/' . esc_attr( $where ) . '/' . esc_attr( $template ) . '-' . esc_attr( $slag ) . '.php';
}

/**
 * Check tab to display content.
 *
 * @return string|void
 */
function check_tab() {
	if ( isset( $_GET['suf_nonce'] ) ) {
		if ( ! wp_verify_nonce( esc_attr( $_GET['suf_nonce'] ), 'scan_up_files_nonce' ) ) {
			//phpcs:disable
			die( esc_attr( __( 'Your are not allowed to be here', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ) );
		}
	}
	return isset( $_GET['tab'] ) ? esc_attr( $_GET['tab'] ) : esc_attr( 'scan_up_files_general_settings' );
}
