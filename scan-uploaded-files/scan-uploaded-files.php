<?php
declare( strict_types=1 );

/**
 * Plugin Name: Scan Uploaded Files
 * Description: This plugin allows to scan files during the upload for malware
 * Author: @vasilguruli
 * Version: 1.0
 * Text Domain: scan-uploaded-files
 * Domain Path: /languages/
 * Tested up to: 5.2.4
 */

use ScanUploadedFiles\Plugin;

if ( ! defined( 'ABSPATH' ) ) {
	die( esc_attr( __( 'You are not allowed to be here', 'scan-uploaded-files' ) ) );
}
require_once __DIR__ . '/config/application.php';
//phpcs:disable
include __DIR__ . '/config/helper-functions.php';
//phpcs:ignore
include SCAN_UPLOADED_FILES_DIR . '/vendor/autoload.php';
$plugin = \ScanUploadedFiles\Init::create();
$plugin->register();
