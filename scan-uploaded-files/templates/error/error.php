<?php
/**
 * @var $errors array
 */
//phpcs:disable
?>
<html lang="en">
<head>
    <title><?php esc_html_e('Include file  › Error') ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="robots" content="noindex,follow">
    <style type="text/css">
        html {
            background: #f1f1f1;
        }

        body {
            background: #fff;
            color: #444;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
            margin: 2em auto;
            padding: 1em 2em;
            max-width: 1500px;
            -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.13);
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.13);
        }

        h1 {
            border-bottom: 1px solid #dadada;
            clear: both;
            color: #666;
            font-size: 24px;
            margin: 30px 0 0 0;
            padding: 0;
            padding-bottom: 7px;
        }

        #error-page {
            margin-top: 50px;
        }

        #error-page p {
            font-size: 14px;
            line-height: 1.5;
            margin: 25px 0 20px;
        }

        #error-page code {
            font-family: Consolas, Monaco, monospace;
        }

        ul li {
            margin-bottom: 10px;
            font-size: 14px;
        }

        a {
            color: #0073aa;
        }

        a:hover,
        a:active {
            color: #00a0d2;
        }

        a:focus {
            color: #124964;
            -webkit-box-shadow: 0 0 0 1px #5b9dd9,
            0 0 2px 1px rgba(30, 140, 190, 0.8);
            box-shadow: 0 0 0 1px #5b9dd9,
            0 0 2px 1px rgba(30, 140, 190, 0.8);
            outline: none;
        }
    </style>
</head>
<body id="error-page">
<div id="ts-error">
	<?php if ( ! empty( $errors ) ) : ?>
        <ul class="ts-error-display">
			<?php foreach ( $errors as $error => $message ) : ?>
                <li class="message"><h2><?php esc_html_e('Some error occurred, details below:') ?></h2>
                </li>
                <li class="message"><?php echo sprintf( '%s', esc_attr( $message['message'] ) ) ?></li>
                <li class="message"><strong><?php esc_html_e('IN FILE') ?></strong>
                    >>> <?php echo sprintf( '%s', esc_attr(  $message['file'] ) ) ?></li>
                <li class="message">
                    <?php esc_html_e('Code: ') ?><?php echo sprintf( '%s', esc_attr(  $message['code'] ) ) ?>
                </li> <li class="message">
                    <?php esc_html_e('Line: ') ?><?php echo sprintf( '%s', esc_attr(  $message['line'] ) ) ?></li>
				<?php if ( APPLICATION_ENVIRONMENT ) : ?>
                    <li class="message"><h2><?php esc_html_e( 'Backtrace:' ) ?></h2></li>
                    <pre><?php print_r( $message['trace'] ); ?></pre>
				<?php endif; ?>
			<?php endforeach; ?>
        </ul>
	<?php endif; ?>
</div>
</body>
</html>