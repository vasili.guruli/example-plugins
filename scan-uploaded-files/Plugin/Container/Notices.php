<?php

namespace ScanUploadedFiles\Container;


interface Notices {
	/**
	 * Admin notices
	 */
	public function notices();
}