<?php

namespace ScanUploadedFiles\Container;


interface Section {
	
	/**
	 * Register settings
	 *
	 * @param string $optionGroup
	 * @param string $option_name
	 * @param null|string $object
	 * @param null|string $method
	 */
	public function register_settings( $optionGroup, $option_name, $object = null, $method = null );
	/**
	 * Add settings section
	 *
	 * @param string $id
	 * @param string $title
	 * @param array $handler
	 * @param string $page
	 */
	public function add_settings_section( $id, $title, array $handler, $page );
	/**
	 * Add settings section
	 *
	 * @param string $id
	 * @param string $title
	 * @param array $handler
	 * @param string $page
	 * @param string $section
	 * @param array $data
	 */
	public function add_settings_field( $id, $title, array $handler, $page, $section, $data = [] );
}