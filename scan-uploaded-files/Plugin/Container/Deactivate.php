<?php

namespace ScanUploadedFiles\Container;

interface Deactivate
{
	/**
	 * Deactivate Plugin
	 */
	public function deactivate();
}