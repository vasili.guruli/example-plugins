<?php
declare( strict_types=1 );

namespace ScanUploadedFiles\ClamAv;

use Appwrite\ClamAV\Network;
use Appwrite\ClamAV\Pipe;

/**
 * Class ClamAv.
 *
 * @package ScanUploadedFiles\ClamAv
 */
class ClamAv {
	public function __construct() {
	}
	
	/**
	 * Scan file with CLamAv
	 *
	 * @param array $data
	 *
	 * @return bool
	 */
	public function scan_file() {
	}
	
	/**
	 * Get ClamAv executable file
	 *
	 * @return string
	 */
	private function get_clamav_executable() {
	}
}
