<?php

namespace ScanUploadedFiles\ScanUploadedFilesInit;

use ScanUploadedFiles\Container\Plugin;
use ScanUploadedFiles\Errors\Errors;
use ScanUploadedFiles\Services\Services;

class ScanUploadedFilesInit extends Services implements Plugin {
	
	/**@var array $errors */
	//phpcs:ignore
	public $errors = [];
	/**@var array $tsServices */
	//phpcs:ignore
	protected $add_services = [];
	
	/**
	 * ScanUploadedFilesInit
	 *
	 * @return void
	 */
	public function __construct() {
		//phpcs:ignore
		$this->add_services = [
			'plugin'     => 'ScanUploadedFiles\Plugin',
			'settings'   => 'ScanUploadedFiles\Settings\Settings',
			'admin_ajax' => 'ScanUploadedFiles\Ajax\AdminAjax',
			'clam_av'    => 'ScanUploadedFiles\ClamAv\ClamAv',
		];
	}
	
	/**
	 * On Plugin activate action
	 *
	 * @return void
	 */
	public function activate() {
		// TODO: Implement activate() method.
	}
	
	/**
	 * On Plugin deactivate action
	 *
	 * @return void
	 */
	public function deactivate() {
		// TODO: Implement deactivate() method.
	}
	
	/**
	 * Admin notices
	 *
	 * @return void
	 */
	public function notices() {
		// TODO: Implement notices() method.
	}
	
	/**
	 * Plugin actions
	 *
	 * @return void
	 */
	public function actions() {
		// TODO: Implement actions() method.
	}
	
	/**
	 * Register new plugin
	 *
	 * @return void
	 */
	public function register() {
		//phpcs:ignore
		add_action( 'plugins_loaded', [ $this, 'load_scan_uploaded_files_plugin', ] );
	}
	
	/**
	 * Load Plugin
	 *
	 * @throws \ScanUploadedFiles\Errors\Errors
	 */
	public function load_scan_uploaded_files_plugin() {
		try {
			//phpcs:disable
			$this->set_details( 'suf.activate', $this->add_services['plugin'],
				[
					'plugin_url'    => SCAN_UPLOADED_FILES_URL,
					'plugin_dir'    => SCAN_UPLOADED_FILES_DIR,
					'settings_page' => SCAN_UPLOADED_FILES_SETTINGS_PAGE,
				],
				//phpcs:disable
				[
					'actions' => 'actions',
				]
			);
			$this->set_details( 'suf.settings', $this->add_services['settings'],
				[],
				[
					'run' => 'run',
				]
			);
			$this->set_details( 'suf.admin_ajax', $this->add_services['admin_ajax'],
				[],
				[
					'add_ajax' => 'add_ajax',
				]
			);
			$this->set_details( 'suf.clam_am', $this->add_services['clam_av'], [], [
				'scan_file' => 'scan_file'
			] );
			
		} catch ( Errors $e ) {
			$errors = new Errors();
			foreach ( $this->add_services as $services => $service ) {
				$errors->add_errors( sprintf( __( 'Service << %s >> could not be found', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), esc_attr( $service ) ) );
			}
		}
		
		$this->add_service();
	}
}
