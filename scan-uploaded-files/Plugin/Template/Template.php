<?php
namespace ScanUploadedFiles\Template;


use ScanUploadedFiles\Errors\Errors;

class Template {
	
	/**
	 * Locate template to display
	 *
	 * @param string $template
	 * @param string $where
	 *
	 * @return string
	 */
	public static function locate_template( $template, $where = 'admin' ) {
		$file = SCAN_UPLOADED_FILES_DIR . '/templates/' . esc_attr( $where ) . '/' . esc_attr( $template ) . '.php';
		if ( ! file_exists( $file ) ) {
			/** @noinspection PhpUnusedLocalVariableInspection */
			$errors = new Errors();
			//phpcs:ignore
			$errors->add_errors( sprintf( __( 'File <strong><< %s >></strong> Could not be found' ), esc_attr( $file ) ) );
		}
		return $file;
	}
	
	/**
	 * Output the template
	 *
	 * @param string $template
	 * @param string $where
	 * @param array $env
	 *
	 * @throws \Exception
	 */
	public static function output( $template, $where, array $env ) {
		$file = self::locate_template( $template, $where );
		//phpcs:ignore
		extract( $env );
		/** @noinspection PhpIncludeInspection */
		//phpcs:ignore
		require( $file );
	}
	
	/**
	 * Get template to display
	 *
	 * @param string $template
	 * @param string $where
	 * @param array $env
	 *
	 * @return false|string
	 * @throws \Exception
	 */
	public static function get( $template, $where, array $env ) {
		ob_start();
		self::output( $template, $where, $env );
		return ob_get_clean();
	}
}
