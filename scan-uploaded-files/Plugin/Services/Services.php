<?php

namespace ScanUploadedFiles\Services;

use ScanUploadedFiles\Container\Service;
use ScanUploadedFiles\Errors\Errors;

/**
 * Class FormFields
 *
 * @package ScanUploadedFiles\FormFields
 *
 * @author form Krzysztof Kassowski
 * @link https://github.com/jigoshop/Jigoshop-eCommerce/blob/master/src/Jigoshop/Container/Services.php
 */
class Services implements Service {
	
	/** @var array $services */
	//phpcs:ignore
	public $services = [];
	
	/** @var array $services */
	//phpcs:ignore
	public $args = [];
	
	/**
	 * Set details for registered service
	 *
	 * @param string $key
	 * @param string $name
	 * @param array $args
	 * @param array $methods
	 *
	 * @param array $method_args
	 *
	 * @throws \ScanUploadedFiles\Errors\Errors
	 */
	public function set_details( $key, $name, array $args, array $methods, $method_args = array() ) {
		if ( $this->key_exists( $key ) ) {
			//phpcs:disable
			throw new Errors( sprintf( __( 'Details already set for << %s >>', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), esc_attr( $key ) ) );
		}
		
		$service_details = [
			'name'    => esc_attr( $name ),
			'args'    => $args,
			'methods' => $methods,
			'method_args' => $method_args,
		];
		
		$this->services[ $key ] = $service_details;
	}
	
	/**
	 * Check if key exists for service
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	public function key_exists( $key ) {
		return isset( $this->services[ $key ] );
	}
	
	/**
	 * Set key for service
	 *
	 * @param $key
	 * @param $name
	 *
	 * @return void
	 */
	public function set( $key, $name ) {
		$this->services[ $key ] = $name;
	}
	
	/**
	 * Get key for service
	 *
	 * @param $key
	 *
	 * @return string
	 */
	public function get( $key ) {
		return $this->services[ $key ];
	}
	
	/**
	 *
	 * Get called class name
	 *
	 * @param string $key
	 *
	 * @return string
	 */
	public function get_class_name( $key ) {
		return $this->services[ $key ]['name'];
	}
	
	/**
	 * Add service
	 */
	public function add_service() {
		$class_name    = '';
		$errors        = new Errors();
		foreach ( $this->services as $key => $value ) {
			if ( ! empty( $key ) ) {
				$class_name = $this->get_class_name( $key );
			}
			if ( class_exists( $class_name ) ) {
				$call_class = ( empty( $value['args'] ) ? new $class_name() : new $class_name( $value['args'] ) );
				foreach ( $value['methods'] as $methods => $method ) {
					if ( method_exists( $call_class, $method ) ) {
						//TODO add args support
						$call_class->$method();
					} else {
						$errors->add_errors( sprintf( __( 'Method << %s >> could not be found', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), esc_attr( $method ) ) );
					}
				}
			} else {
				$errors->add_errors( sprintf( __( 'Class << %s >> could not be found', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), esc_attr( $class_name ) ) );
			}
		}
	}
}