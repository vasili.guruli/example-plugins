<?php

namespace ScanUploadedFiles\Errors;

use Throwable;

/**
 * Class FormFields
 *
 * @package ScanUploadedFiles\Errors
 */
class Errors extends \Exception {
	
	protected $message;
	protected $code;
	//phpcs:ignore
	private $errors = [];
	
	/**
	 * Errors constructor
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable $previous
	 *
	 * @return void
	 */
	public function __construct( $message = '', $code = 0, Throwable $previous = null ) {
		$this->message = $message;
		$this->code    = $code;
		parent::__construct( $message, $code, $previous );
	}
	
	/**
	 * Add error message
	 *
	 * @param string $message
	 *
	 * @return array
	 */
	private function add_message( $message ) {
		//phpcs:ignore
		$errors[] = [
			/* translators: 1) $message */
			//phpcs:ignore
			'message' => sprintf( __( '%s', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), esc_attr( $message ) ),
			'file'    => sprintf( '%s', esc_attr( $this->getFile() ) ),
			'code'    => sprintf( '%s', esc_attr( $this->getCode() ) ),
			'line'    => sprintf( '%s', esc_attr( $this->getLine() ) ),
			'trace'   => $this->getTrace(),
		];
		return $errors;
	}
	/**
	 * Display error messages
	 *
	 * @param string $message
	 * @param bool $include_in_html
	 *
	 * @return void
	 */
	public function add_errors( $message, $include_in_html = false ) {
		//phpcs:ignore
		$errors = [];
		if ( ! empty( $this->add_message( $message ) ) ) {
			foreach ( $this->add_message( $message ) as $error => $error_message ) {
				$errors[] = $error_message;
			}
		}
		if ( ! empty( $errors ) ) {
			$this->load_error_file( $errors, $include_in_html );
			exit();
		}
	}
	/**
	 * Include error template
	 *
	 * @param array $errors
	 * @param bool $include_in_html
	 *
	 * @return void
	 */
	private function load_error_file( $errors, $include_in_html = false ) {
		if ( ! empty( $errors ) ) {
			if ( $include_in_html ) {
				include SCAN_UPLOADED_FILES_ERROR_DIR . '/separate-error.php';
			} else {
				include SCAN_UPLOADED_FILES_ERROR_DIR . '/error.php';
			}
		}
	}
}