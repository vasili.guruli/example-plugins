<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * @var string $log_file
 */
return array(
	'method_enable'          => array(
		'title'       => __( 'Enable/Disable', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'checkbox',
		'description' => __( 'Enable PayPo Payments', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => false,
	),
	'method_title'           => array(
		'title'       => __( 'Title', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'text',
		'description' => __( 'This controls the method title in checkout page', 'woocommerce-paypo-payment-gateway' ),
		'default'     => __( 'PayPo - Kup dziś, zapłać za 30 dni.', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => false,
	),
	'method_description'     => array(
		'title'       => __( 'Description', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'text',
		'description' => __( 'This controls method description in checkout page', 'woocommerce-paypo-payment-gateway' ),
		'default'     => __( 'Integracja płatności PayPo', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => false,
	),
	'test_mode'              => array(
		'title'       => __( 'Enable Test Mode', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'select',
		'description' => __( 'You can enable test mode for sandbox integration. This is only for testing purposes, no real payments will be made', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => true,
		'options'     => array(
			'no'  => __( 'No', 'woocommerce-paypo-payment-gateway' ),
			'yes' => __( 'Yes', 'woocommerce-paypo-payment-gateway' )
		)
	),
	'merchant_id'            => array(
		'title'       => __( 'Merchant ID', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'text',
		'description' => __( 'Set merchant id provided by PayPo.', 'woocommerce-paypo-payment-gateway' ),
		'default'     => __( '', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => true,
	),
	'merchant_api_token'     => array(
		'title'       => __( 'Merchant Api Key (Token)', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'text',
		'description' => __( 'Set merchant api key provided by PayPo.', 'woocommerce-paypo-payment-gateway' ),
		'default'     => __( '', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => true,
	),
	'custom_logo_url'        => array(
		'title'       => __( 'URL to custom logo', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'text',
		'description' => __( 'Enter url to custom logo (e.g https://your-site.com/wp-content/uploads/2020/02/awesome.logo.png)', 'woocommerce-paypo-payment-gateway' ),
		'default'     => __( '', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => false,
	),
	'order_limit_min_amount' => array(
		'title'             => __( 'Minimum order amount for payment', 'woocommerce-paypo-payment-gateway' ),
		'type'              => 'number',
		'description'       => __( 'Set the minimum order amount for payment. Choose when PayPo should be displayed on checkout page.
													Min value: 10. Max value: 1000', 'woocommerce-paypo-payment-gateway' ),
		'default'           => __( '', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'          => true,
		'custom_attributes' => array(
			'min'  => 10,
			'max'  => 1000,
			'step' => 0.01
		)
	),
	'order_limit_max_amount' => array(
		'title'             => __( 'Maximum order amount for payment', 'woocommerce-paypo-payment-gateway' ),
		'type'              => 'number',
		'description'       => __( 'Set the maximum order amount for payment. Choose when PayPo should be displayed on checkout page.
													Min value: 1001. Max value: 10 000', 'woocommerce-paypo-payment-gateway' ),
		'default'           => __( '', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'          => true,
		'custom_attributes' => array(
			'min'  => 1000,
			'max'  => 10000,
			'step' => 0.01
		)
	),
	'debug_log'              => array(
		'title'       => __( 'Włącz zapisywania logów', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'select',
		'description' => __( 'Możesz włączyć zapisywanie logów debugowania dla requestów (<strong>tylko w celach programistycznych</strong>)', 'woocommerce-paypo-payment-gateway' ),
		'default'     => __( '', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => false,
		'options'     => array(
			'no'  => __( 'No', 'woocommerce-paypo-payment-gateway' ),
			'yes' => __( 'Yes', 'woocommerce-paypo-payment-gateway' )
		)
	),
	'log_file'               => array(
		'title'       => __( 'Pobierz plik logów', 'woocommerce-paypo-payment-gateway' ),
		'type'        => 'hidden',
		'description' => ( empty( $log_file ) ? __( 'Nie ma jeszcze pliku do pobrania, z dzisiejszą datą. Wszystkie logi bedą w katalogu "wp-content/plugins/woocommerce-gateway-paypo/log"' ) : '<a href="' . esc_url( $log_file ) . '" target="_blank">' . __( 'Pobierz plik' ) . '</a>' ),
		'default'     => __( 'Pobierz plik', 'woocommerce-paypo-payment-gateway' ),
		'desc_tip'    => false,
	),
);
