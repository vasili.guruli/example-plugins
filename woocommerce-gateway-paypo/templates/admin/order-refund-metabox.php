<?php
/**
 * @var int $refund_amount
 * @var WC_Order $order
 */
$wrong_amount = get_post_meta( $order->get_id(), '_wrong_amount', true );
?>
<input type="hidden" name="paypo_refund_nonce" value="<?php echo wp_create_nonce(); ?>">
<p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
<p><small><?php echo sprintf(__('Wiecej informacji o zwrotach PayPo znajdziesz <a href="%s" target="_blank"> tutaj</a>'), esc_url('https://paypo.freshdesk.com/support/solutions/folders/17000136506')) ?></small></p>
<label for="paypo_order_refund"><?php echo __('Kwota zwrotu (kwota jaka pozostała do spłaty): ' ) . ( $refund_amount  ? $refund_amount . '&nbsp;zł' : '') ?></label>

<input id="paypo_order_refund" class="short wc_input_price" type="text" step="0.01" style="width:250px;" name="paypo_order_refund_amount" placeholder="<?php echo $refund_amount ?>"
       value="<?php echo esc_attr( $refund_amount ); ?>">
<small><strong><?php echo __('Uwaga! Kwota bieżąca w PayPo będzie zmieniona na kwotę zwrotu (zwrot częściowy PayPo).
 W Przypadku gdy kwota jest ustalona na 0, zwrot będzie całkowity!') ?></strong></small>
<?php if ( '' !== $wrong_amount ) :  ?>
    <p><small><strong style="color: #FF0000"><?php echo __('Zmiana kwoty zwrotu na ' . $wrong_amount . 'zł' . ' nie jest możliwe!') ?></strong></small></p>
<?php endif; ?>
</p>