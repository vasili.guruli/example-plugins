<?php
/**
 * @var array $redirect
 * @var $order WC_Order
 */

?>
<div class="error">
    <h2><?php echo __( 'Wystąpił błąd podczas komunikacji z PayPo. Proszę spróbować póznej', 'woocommerce-paypo-payment-gateway' ) ?></h2>
	<?php if ( ! empty( $redirect['info'] ) ) : ?>
		<?php echo __( 'Błąd serwera: ' . $redirect['info'] ); ?>
	<?php endif; ?>
</div>
<a class="button cancel"
   href="<?php echo $order->get_cancel_order_url_raw() ?>"><?php echo __( 'Cancel order &amp; restore cart', 'woocommerce-paypo-payment-gateway' ) ?></a>