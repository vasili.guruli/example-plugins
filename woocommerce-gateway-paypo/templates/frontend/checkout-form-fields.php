<?php
/**
 * @var array $redirect
 * @var $order WC_Order
 */

?>
<form action="<?php echo esc_url( $redirect['redirect_url'] ); ?>" method="post">
    <input type="submit" class="button-alt" id="submit_paypo_form"
           value="<?php echo __( 'Pay via PayPo', 'woocommerce-paypo-payment-gateway' ) ?>"/>
    <a class="button cancel"
       href="<?php echo $order->get_cancel_order_url_raw() ?>"><?php echo __( 'Cancel order &amp; restore cart', 'woocommerce-paypo-payment-gateway' ) ?></a>
</form>