��    !      $      ,      ,     -     M     Y     g     }     �  `   �     �           ,     E      Q     r     u     �     �     �  "   �  "   �       '   %  "   M  �   p  �        �  1   �  /   �     �            r        �  p  �  *   	     ?	     D	     Q	     k	     �	  g   �	     �	     
     %
     B
     \
     x
     |
      �
  '   �
     �
  &   �
          =  #   U  2   y  �   �  �   T  
   �  -     /   0  
   `     k     �  �   �     )   Cancel order &amp; restore cart Description Documentation Enable PayPo Payments Enable Test Mode Enable/Disable Enter url to custom logo (e.g https://your-site.com/wp-content/uploads/2020/02/awesome.logo.png) Inspire Labs Maximum order amount for payment Merchant Api Key (Token) Merchant ID Minimum order amount for payment No Order status: Cancelled Order status: Completed Order status: Processing Pay via PayPo PayPo - Buy today, pay in 30 days. PayPo Delayed Payments Integration Payment is pending Set merchant api key provided by PayPo. Set merchant id provided by PayPo. Set the maximum order amount for payment. Choose when PayPo should be displayed on checkout page.
													Min value: 1001. Max value: 10 000 Set the minimum order amount for payment. Choose when PayPo should be displayed on checkout page. 
													Min value: 10. Max value: 1000 Settings This controls method description in checkout page This controls the method title in checkout page Title URL to custom logo Yes You can enable test mode for sandbox integration. This is only for testing purposes, no real payments will be made https://inspirelabs.pl Project-Id-Version: WooCommerce PayPo Payment Gateway 1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woocommerce-gateway-paypo
POT-Creation-Date: 2020-03-09T08:45:48+01:00
PO-Revision-Date: 2020-03-09 09:33+0000
Last-Translator: 
Language-Team: Polish (Poland)
Language: pl-PL
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Loco-Source-Locale: en_PL
X-Generator: Loco https://localise.biz/
X-Domain: woocommerce-paypo-payment-gateway
X-Loco-Parser: loco_parse_po Anuluj zamówienie &amp; przywróć koszyk Opis Dokumentacja Włącz Płatność PayPo Włącz tryb testowy Włącz / Wyłącz Wpisz adres URL własnego  logo (np. Https://your-site.com/wp-content/uploads/2020/02/awesome.logo.png) Inspire Labs Maksymalna kwota zamówienia Klucz Api sprzedawcy (token)  Identyfikator sprzedawcy Minimalna kwota zamówienia Nie Status zamówienia: Anulowane Status zamówienia: Zrealizowane Status zamówienia: Wtrakcie realizacji Zapłać przez PayPo PayPo - Kup dziś, zapłać za 30 dni. PayPo - płatnośc odroczona. Płatność jest w toku Ustaw klucz API podany przez PayPo. Ustaw identyfikator sprzedawcy podany przez PayPo. Ustaw maksymalną kwotę zamówienia. Wybierz, kiedy PayPo ma być wyświetlany na stronie  zamówienia.
													Wartość min: 1001. Wartość maksymalna: 10 000 Ustaw minimalną kwotę zamówienia. Wybierz, kiedy PayPo ma być wyświetlany na stronie zamówienia. 
													Wartość min: 10. Wartość maksymalna: 1000 Ustawienia Kontroluje opis metody na stronie zamówienia Kontroluje tytuł metody na stronie zamówienia Nagłówek URL do własnego logo Tak Możesz włączyć tryb testowy dla integracji z trybu testowego PayPo. Jest to wyłącznie do celów testowych, nie zostaną dokonane żadne prawdziwe płatności https://www.inspirelabs.pl/ 