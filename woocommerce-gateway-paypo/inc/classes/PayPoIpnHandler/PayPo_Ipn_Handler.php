<?php
/**
 * PayPo_Ipn_Handler class.
 *
 * @package PayPo
 */

namespace PayPo\PayPoIpnHandler;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

use PayPo\PayPo\PayPo;
use PayPo\PayPoConnect\PayPo_Connect;
use PayPo\PayPoSettings\PayPo_Settings;

/**
 * Class PayPoIpnHandler. Should handles responses from PayPo IPN.
 */
class PayPo_Ipn_Handler {
	
	/** @var PayPo_Connect $paypo_connect */
	protected $paypo_connect;
	/** @var PayPo $paypo */
	protected $paypo;
	
	/**
	 * @param PayPo $paypo
	 * @param \PayPoConnect $paypo_connect
	 */
	public function __construct( $paypo_connect, $paypo ) {
		$this->paypo_connect = $paypo_connect;
		$this->paypo         = $paypo;
		
		add_action( 'woocommerce_api_' . str_replace( '\\', '/', strtolower( PayPo_Settings::class ) ), array(
			$this,
			'handle_request'
		) );
		add_action( 'valid-paypo-standard-ipn-request', array( $this, 'valid_paypo_response' ) );
	}
	
	/**
	 * Can handle PayPo notify url request -> wc-api/paypo/paypoipnhandler/paypo_ipn_handler/
	 *
	 * @throws \WC_Data_Exception
	 */
	public function handle_request() {
		$result = json_decode( file_get_contents( 'php://input' ), true );
		if ( ! empty( $result ) && $this->paypo_connect->validateResponse( $result ) ) {
			do_action( 'valid-paypo-standard-ipn-request', $result );
			http_response_code( 200 );
			exit();
			
		}
		
		wp_die( 'PayPo IPN Request Failure', 'PayPo IPN', array( 'response' => 500 ) );
	}
	
	/**
	 * @return void
	 * @var string $paypo_order_id
	 *
	 * @var int $order_id
	 */
	private function add_paypo_order_id( $order_id, $paypo_order_id ) {
		if ( empty( get_post_meta( $order_id, '_paypo_order_id', true ) ) ) {
			add_post_meta( $order_id, '_paypo_order_id', $paypo_order_id, true );
		}
	}
	
	/**
	 * @return void
	 * @var string $order_crc
	 *
	 * @var int $order_id
	 */
	private function add_paypo_order_crc( $order_id, $order_crc ) {
		if ( empty( get_post_meta( $order_id, '_paypo_order_crc', true ) ) ) {
			add_post_meta( $order_id, '_paypo_order_crc', $order_crc, true );
		}
	}
	
	
	/**
	 * Handle response from PayPo
	 *
	 * @return void|bool
	 *
	 * @var array $result
	 *
	 */
	public function valid_paypo_response( $result ) {
		$confirm_result = array();
		$order_id       = wc_get_order_id_by_order_key( $result['foreign_id'] );
		$order          = new \WC_Order( $order_id );
		if ( ! is_object( $order ) ) {
			$this->paypo_connect->log( 'No Order found with ID: ' . var_export( $order ) );
			
			return false;
		}
		if ( $result['status'] === 'OK' ) {
			
			if ( $result['order_crc'] === md5( $this->paypo->settings['merchant_id'] . '|' . $order->get_order_key() .
			                                   '|' . ceil( $order->get_total() * 100 ) .
			                                   '|' . $this->paypo->settings['merchant_api_token'] ) ) {
				$this->paypo->set_order_details_for_paypo( $order );
				$this->add_paypo_order_crc( $order->get_id(), $result['order_crc'] );
				$this->add_paypo_order_id( $order->get_id(), $result['order_id'] );
				try {
					$order->set_transaction_id( $result['order_id'] );
				} catch ( \WC_Data_Exception $e ) {
					//
				}
			}
			
			if ( $result['order_status'] === 'NEW' ) {
				$order->update_status( 'pending', __( 'Oczekuje na płatność', 'woocommerce-paypo-payment-gateway' ) );
				
			} else {
				$missed_statuses = array( 'CLOSED', 'EXCEPTION' );
				if ( ! in_array( $result['order_status'], $missed_statuses ) ) {
					$order->update_status( strtolower( $result['order_status'] ), __( 'Order Status: ' . strtolower( ucfirst( $result['order_status'] ) ) ) );
				}
			}
			// We are here so everything is ok, send Order confirm back to PayPo and handle response
			$confirm_result = $this->paypo_connect->sendConfirm( $order );
			if ( $confirm_result['knk_status'] === 'OK' ) {
				if ( (int) $confirm_result['knk_status_code'] === 210 ) {
					switch ( $confirm_result['knk_order_status'] ) {
						case 'PROCESSING' :
							$order->update_status( 'processing', __( 'Status zamówienia: W trakcie realizacji', 'woocommerce-paypo-payment-gateway' ) );
							break;
						case 'DELIVERED':
						case 'COMPLETED' :
							$order->update_status( 'completed', __( 'Order status: Completed', 'woocommerce-paypo-payment-gateway' ) );
							break;
						case 'REFUND' :
							$order->update_status( 'refund', __( 'Order status: Refund', 'woocommerce-paypo-payment-gateway' ) );
							break;
						default:
							$order->update_status( 'pending', __( 'Oczekuje na płatność', 'woocommerce-paypo-payment-gateway' ) );
							break;
					}
					$this->paypo_connect->log( 'log date: ' .  date("Y-m-d") .  "\n" .
					                           'log time: ' . date("h:i:s")  . "\n" .
					                           'ORDER CONFIRM RESPONSE : ' . json_encode($confirm_result) . "\n".
					                           'END ORDER CONFIRM RESPONSE ' . "\n" . ' =====================================' . "\n" );
					$hash = get_post_meta( $order->get_id(), '_order_confirm_hash', true );
					
					delete_post_meta( $order->get_id(), '_order_confirm_hash', $hash );
				}
			}
			
		} else {
			if ( $result['status'] === 'ERR' ) {
				$order->update_status( 'pending', __( 'Oczekuje na płatność', 'woocommerce-paypo-payment-gateway' ) );
			}
		}
	}
}