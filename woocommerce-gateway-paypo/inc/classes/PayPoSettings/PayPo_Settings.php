<?php
/**
 * Plugin main class.
 *
 * @package PayPo
 */

namespace PayPo\PayPoSettings;

/**
 * Main plugin class. The most plugin flow are made here.
 *
 * @package PayPo
 */
class PayPo_Settings extends \WC_Payment_Gateway {
	
	/** @var string $merchant_id */
	protected $merchant_id;
	/**@var string $merchant_api_token */
	protected $merchant_api_token;
	/**@var bool $sandbox ; */
	protected $sandbox;
	/** @var string $notify_url */
	protected $notify_url;
	/** @var string $log_file */
	private $log_file;
	
	public function __construct() {
		$this->id                 = 'paypo';
		$this->method_title       = 'PayPo';
		$this->method_description = __( 'PayPo - płatnośc odroczona.', 'woocommerce-paypo-payment-gateway' );
		$this->icon               = ( '' !== $this->get_custom_logo_url() ? $this->get_custom_logo_url() : WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_URL . '/assets/images/PayPo_logo.png' );
		$this->init_form_fields();
		$this->init_settings();
		
		$this->merchant_id        = $this->get_option( 'merchant_id' );
		$this->merchant_api_token = $this->get_option( 'merchant_api_token' );
		$this->sandbox            = $this->is_sandbox_url();
		
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array(
			$this,
			'process_admin_options'
		) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
	}
	
	/**
	 * Initialize admin form fields .
	 */
	public function init_form_fields() {
		try {
			$log_file          = $this->get_log_file();
			$this->form_fields = include( WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEMPLATE_DIR . 'admin/form-fields.php' );
		} catch ( \Exception $e ) {
			echo __( 'No fields found for this method. ' . $e->getMessage() );
		}
	}
	
	/**
	 * Enqueue admin scripts
	 */
	public function admin_scripts() {
		$current_screen = get_current_screen();
		if ( $current_screen->base === 'woocommerce_page_wc-settings' ) {
			if ( isset( $_GET['tab'] ) && $_GET['tab'] === 'checkout' ) {
				if ( isset( $_GET['section'] ) && $_GET['section'] === 'paypo' ) {
					wp_enqueue_script( 'paypo-admin-script', WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_URL . '/assets/js/admin_script.js', array( 'jquery' ), '1.0', true );
					wp_localize_script( 'paypo-admin-script', 'paypo', array(
						'debug_mode' => $this->settings['debug_log'],
					) );
				}
			}
		}
	}
	
	/**
	 * Retrieve custom logo if set
	 *
	 * @return string url to custom logo for PayPo;
	 */
	protected function get_custom_logo_url() {
		$custom_logo = '';
		if ( ! empty( $this->get_option( 'custom_logo_url' ) ) ) {
			$custom_logo = esc_url( $this->get_option( 'custom_logo_url' ) );
		}
		
		return $custom_logo;
	}
	
	/**
	 * Check if sandbox url is set
	 *
	 * @return bool
	 */
	protected function is_sandbox_url() {
		if ( 'yes' === $this->get_option( 'test_mode', 'no' ) ) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Get PayPo log file
	 *
	 * @return string
	 */
	private function get_log_file() {
		$log_file_url = '';
		if ( file_exists( WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_DIR . '/log/paypo_log_' . date( 'd-M-Y' ) . '.log' ) ) {
			$log_file_url = WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_URL . '/log/paypo_log_' . date( 'd-M-Y' ) . '.log';
		}
		
		return $log_file_url;
	}
}