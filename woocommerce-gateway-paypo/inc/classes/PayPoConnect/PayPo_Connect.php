<?php
/**
 * PayPo_Connect class.
 *
 * @package PayPo
 */

namespace PayPo\PayPoConnect;

/**
 * PayPo connect class
 */
class PayPo_Connect {
	protected $version = 0.1;
	protected $apiVersion = '2_0';
	protected $auth = 'HMAC';
	protected $merchantId;
	protected $merchantKey;
	protected $shopId;
	protected $sandbox = false;
	protected $apiProd = 'https://api.paypo.pl/';
	protected $apiSand = 'https://api.sandbox.paypo.pl/';
	protected $logFile;
	protected $debug = false;
	protected $urlReturn;
	protected $urlNotify;
	protected $urlCancel;
	protected $sessionId;
	protected $error;
	protected $order_crc;
	
	
	/**
	 * Order related information
	 * @var array
	 */
	protected $infoOrder = [
		'foreign_id'   => null,
		'provider_id'  => null,
		'order_descr'  => null,
		'order_amount' => null,
		'order_id'     => null,
		'order_crc'    => null,
	];
	
	/**
	 * Customer related information
	 * @var array
	 */
	protected $infoCustomer = [
		'customer'         => null,
		'email'            => null,
		'phone'            => null,
		'address'          => null,
		'postal'           => null,
		'city'             => null,
		'country'          => null,
		'trusted_customer' => null
	];
	
	/**
	 * Shippment related information
	 * @var array
	 */
	protected $infoShipment = [
		'shipment'         => null,
		'shipping_address' => null,
		'shipping_postal'  => null,
		'shipping_city'    => null,
		'shipping_country' => null
	];
	
	/**
	 * Set shop configuration
	 *
	 * @param int $merchantId
	 * @param string $merchantKey
	 * @param int $shopId
	 * @param bool $sandbox
	 * @param bool $debug
	 *
	 * @return boolean
	 */
	public function __construct( $merchantId, $merchantKey, $shopId = null, $sandbox = false, $debug = false ) {
		if ( empty( $merchantId ) || empty( $merchantKey ) ) {
			return false;
		}
		$this->merchantId  = $merchantId;
		$this->merchantKey = $merchantKey;
		$this->shopId      = $shopId;
		$this->sandbox     = $sandbox;
		$this->debug       = $debug;
		$this->setLogFile( WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_DIR . '/log/' );
		date_default_timezone_set('Europe/Berlin');
		return true;
	}
	
	/**
	 * Prepare data for order create call
	 *
	 * @return array|bool
	 */
	public function prepareCreateCall() {
		$req = [];
		$this->appendBaseData( $req );
		if ( ! empty( $this->infoOrder['provider_id'] ) ) {
			$req['provider_id'] = $this->infoOrder['provider_id'];
		}
		if ( ! empty( $this->infoOrder['order_descr'] ) ) {
			$req['order_descr'] = $this->infoOrder['order_descr'];
		}
		if ( empty( $this->infoOrder['order_amount'] ) ) {
			$this->log( 'Order amount is empty' );
			
			return false;
		}
		$req['order_amount'] = $this->infoOrder['order_amount'];
		if ( empty( $this->infoCustomer['customer'] ) ) {
			$this->log( 'Customer is empty' );
			
			return false;
		}
		$req['customer'] = $this->infoCustomer['customer'];
		if ( empty( $this->infoCustomer['email'] ) ) {
			$this->log( 'Email is empty' );
			
			return false;
		}
		$req['email'] = $this->infoCustomer['email'];
		if ( ! empty( $this->infoCustomer['phone'] ) ) {
			$req['phone'] = $this->infoCustomer['phone'];
		}
		if ( empty( $this->infoCustomer['address'] ) ) {
			$this->log( 'Address is empty' );
			
			return false;
		}
		$req['address'] = $this->infoCustomer['address'];
		if ( empty( $this->infoCustomer['postal'] ) ) {
			$this->log( 'Postal is empty' );
			
			return false;
		}
		$req['postal'] = $this->infoCustomer['postal'];
		if ( empty( $this->infoCustomer['city'] ) ) {
			$this->log( 'City is empty' );
			
			return false;
		}
		$req['city'] = $this->infoCustomer['city'];
		if ( ! empty( $this->infoCustomer['country'] ) ) {
			$req['country'] = $this->infoCustomer['country'];
		}
		if ( ! empty( $this->infoCustomer['trusted_customer'] ) ) {
			$req['trusted_customer'] = $this->infoCustomer['trusted_customer'];
		}
		if ( ! empty( $this->infoShipment['shipment'] ) ) {
			$req['shipment'] = $this->infoShipment['shipment'];
		}
		if ( ! empty( $this->infoShipment['shipping_address'] ) ) {
			$req['shipping_address'] = $this->infoShipment['shipping_address'];
		}
		if ( ! empty( $this->infoShipment['shipping_postal'] ) ) {
			$req['shipping_postal'] = $this->infoShipment['shipping_postal'];
		}
		if ( ! empty( $this->infoShipment['shipping_city'] ) ) {
			$req['shipping_city'] = $this->infoShipment['shipping_city'];
		}
		if ( ! empty( $this->infoShipment['shipping_country'] ) ) {
			$req['shipping_country'] = $this->infoShipment['shipping_country'];
		}
		
		return $req;
	}
	
	/**
	 * Append base call data to array
	 */
	protected function appendBaseData( &$arr ) {
		$arr['api_ver'] = $this->apiVersion;
		if ( ! empty( $this->sessionId ) ) {
			$arr['session_id'] = $this->sessionId;
		}
		if ( empty( $this->merchantId ) ) {
			$this->log( 'Merchant id not set' );
			
			return false;
		}
		$arr['auth']        = $this->auth;
		$arr['merchant_id'] = $this->merchantId;
		if ( ! empty( $this->shopId ) ) {
			$arr['shop_id'] = $this->shopId;
		}
		if ( empty( $this->infoOrder['foreign_id'] ) ) {
			$this->log( 'Foreign id not set' );
			
			return false;
		}
		$arr['foreign_id'] = $this->infoOrder['foreign_id'];
		
		if ( empty( $this->infoOrder['order_id'] ) ) {
			$this->log( 'Order id not set' );
			
			return false;
		}
		$arr['order_id'] = $this->infoOrder['order_id'];
		
		if ( empty( $this->urlReturn ) ) {
			$this->log( 'Return url not set' );
			
			return false;
		}
		$arr['return_url'] = $this->urlReturn;
		
		if ( empty( $this->urlNotify ) ) {
			$this->log( 'Notify url not set' );
			
			return false;
		}
		$arr['notify_url'] = $this->urlNotify;
		
		if ( ! empty( $this->urlCancel ) ) {
			$arr['cancel_url'] = $this->urlCancel;
		}
	}
	
	/**
	 * Session id for redirections
	 *
	 * @param string $id
	 */
	public function setSessionId( $id ) {
		$this->sessionId = $id;
	}
	
	/**
	 * Set thank you page url
	 *
	 * @param string $url
	 */
	public function setReturnUrl( $url ) {
		$this->urlReturn = $url;
	}
	
	/**
	 * Set api notification url
	 *
	 * @param string $url
	 */
	public function setNotifyUrl( $url ) {
		$this->urlNotify = $url;
	}
	
	/**
	 * Set cancel page url
	 *
	 * @param string $url
	 */
	public function setCancelUrl( $url ) {
		$this->urlCancel = $url;
	}
	
	/**
	 * Generate API url based on env flag
	 *
	 * @param string $url
	 *
	 * @return string
	 */
	protected function urlGenerator( $url ) {
		if ( $this->sandbox ) {
			$url = $this->apiSand . $url;
		} else {
			$url = $this->apiProd . $url;
		}
		
		return $url;
	}
	
	/**
	 * Append message to log file
	 *
	 * @param string $message
	 *
	 * @return bool
	 */
	public function log( $message ) {
		if ( 'no' === get_option('is_debug_mod') ) {
			return false;
		}
		if ( ! file_exists( $this->logFile ) ) {
			mkdir( $this->logFile, 0777, true );
		}
		$log = $this->logFile . '/paypo_log_' . date( 'd-M-Y' ) . '.log';
		
		file_put_contents( $log, $message . "\n", FILE_APPEND );
	}
	
	/**
	 * Set path to log file
	 *
	 * @param string $path
	 *
	 * @return string
	 */
	public function setLogFile( $path ) {
		
		$this->logFile = $path;
		
		return $this->logFile;
	}
	
	/**
	 * Set param in specified group
	 *
	 * @param string $group (Customer|Order|Shipment)
	 * @param string $param
	 * @param mixed $val
	 *
	 * @return bool
	 */
	public function setParam( $group, $param, $val ) {
		if ( ! in_array( $group, [ 'Customer', 'Order', 'Shipment' ] ) ) {
			return false;
		}
		$group                  = 'info' . $group;
		$this->$group[ $param ] = $val;
		
		return true;
	}
	
	/**
	 * Sends notification confirmation to PayPo
	 *
	 * @param \WC_Order $order
	 *
	 */
	public function sendConfirm( $order ) {
		$confirm_data = array(
			"merchant_id"  => $this->merchantId,
			"foreign_id"   => $this->infoOrder['foreign_id'],
			"order_id"     => get_post_meta( $order->get_id(), '_paypo_order_id', true ),
			"order_amount" => $this->infoOrder['order_amount'],
			"order_crc"    => get_post_meta( $order->get_id(), '_paypo_order_crc', true ),
		);
		
		$endpoint = $this->urlGenerator( 'v2/orders/confirm' );
		$hash     = get_post_meta( $order->get_id(), '_order_confirm_hash', true );
		return $this->curl( $endpoint, 'PUT', $hash, time(), $confirm_data );
	}
	
	/**
	 * Retrieve order details from PayPo
	 *
	 * @param \WC_Order $order
	 *
	 * @return array|bool
	 */
	public function retrieve_paypo_order_details( $order ) {
		$order_id = $this->infoOrder['order_id'];
		if ( ! empty( $this->get_order_id( $order->get_id() ) ) ) {
			$order_id = $this->get_order_id( $order->get_id() );
		}
		$order_details_data_to_send = array(
			"merchant_id" => $this->merchantId,
			"foreign_id"  => $order->get_order_key(),
			"order_id"    => $order_id,
			"order_crc"   => $this->get_order_crc( $order->get_id() ),
		);
		
		$url = $this->urlGenerator( 'v2/orders/details' );
		
		return $this->curl( $url, 'POST', null, null, $order_details_data_to_send );
	}
	
	/**
	 * http://codepad.org/NW4e9hQH
	 */
	protected function refund_amount_to_float( $num ) {
		$dotPos   = strrpos( $num, '.' );
		$commaPos = strrpos( $num, ',' );
		$sep      = ( ( $dotPos > $commaPos ) && $dotPos ) ? $dotPos :
			( ( ( $commaPos > $dotPos ) && $commaPos ) ? $commaPos : false );
		
		if ( ! $sep ) {
			return floatval( preg_replace( "/[^0-9]/", "", $num ) );
		}
		
		return floatval(
			preg_replace( "/[^0-9]/", "", substr( $num, 0, $sep ) ) . '.' .
			preg_replace( "/[^0-9]/", "", substr( $num, $sep + 1, strlen( $num ) ) )
		);
	}
	
	/**
	 * Send order modification request
	 *
	 * @param \WC_Order $order
	 * @param string $newStatus
	 * @param float $refund_amount
	 */
	public function sendModify( $order, $newStatus, $refund_amount = 0 ) {
		$order_id = $this->infoOrder['order_id'];
		if ( ! empty( $this->get_order_id( $order->get_id() ) ) ) {
			$order_id = $this->get_order_id( $order->get_id() );
		}
		
		//$client      = new \GuzzleHttp\Client();
		//$url         = $this->urlGenerator( 'v2/orders/modify' );
		$modify_data = array(
			"merchant_id"  => $this->merchantId,
			"foreign_id"   => $order->get_order_key(),
			"order_id"     => $order_id,
			"order_amount" => (int) $order->get_total() * 100,
			"set_status"   => $newStatus,
			"order_crc"    => $this->get_order_crc( $order->get_id() ),
			"notifyme"     => 0,
		);
		
		$total_refund = $this->refund_amount_to_float( $refund_amount );
		
		if ( '' !== $refund_amount ) {
			$modify_data['new_order_amount'] = $total_refund * 100;
		}
		
		$url = $this->urlGenerator( 'v2/orders/modify' );
		
		$response = $this->curl( $url, 'PUT', null, null, $modify_data );
		// knk_status_code":210
		if ( ! empty( $response ) ) {
			if ( isset( $response['knk_status'] ) ) {
				if ( $response['knk_status'] === 'OK' ) {
					$this->log( 'log date: ' .  date("Y-m-d") .  "\n" .
					            'log time: ' . date("h:i:s a", time())  . ' | ' . microtime(true) . "\n" .
					            'Order Modify Status : ' . esc_attr( $response['knk_order_status'] )
					            . '. Response: ' . var_export( $response, true ). "\n" .
					            'END SUCCESS MODIFY RESPONSE'.  "\n" .  ' =====================================' . "\n" );
				} else {
					$this->log( 'log date: ' .  date("Y-m-d") .  "\n" .
					            'log time: ' . date("h:i:s a", time())  . ' | ' . microtime(true) . "\n" .
					            'Response modify error: ' . var_export( $modify_data, true ) . ' response: ' . var_export( $response, true ). "\n".
					            'END MODIFY ORDER STATUS ERROR RESPONSE'.  "\n" .  ' =====================================' . "\n" );
				}
			}
		}
		
		return $response;
	}
	
	/**
	 * @param string $hash
	 * @param string $timestamp
	 * @param array $data
	 *
	 * @return mixed
	 */
	public function orderRegister( $hash, $timestamp, array $data ) {
		return $this->curl( $this->urlGenerator( 'v2/orders/register' ), 'POST', $hash, $timestamp, $data );
	}
	
	/**
	 * @param int $order_id
	 *
	 * @return string
	 */
	protected function get_order_crc( $order_id ) {
		return get_post_meta( $order_id, '_paypo_order_crc', true );
	}
	
	/**
	 * @param int $order_id
	 *
	 * @return string
	 */
	protected function get_order_id( $order_id ) {
		return get_post_meta( $order_id, '_paypo_order_id', true );
	}
	
	/**
	 * @param int $order_id
	 * @param int $key
	 *
	 * @return bool
	 */
	protected function delete_meta( $order_id, $key ) {
		return delete_post_meta( $order_id, $key );
	}
	
	/**
	 * Curl request
	 *
	 * @param string $url
	 * @param string $type
	 * @param string $hash
	 * @param string $timestamp
	 * @param array $data
	 *
	 * @return array|bool
	 */
	public function curl( $url, $type, $hash = null, $timestamp = null, array $data ) {
		$dataJson = null;
		$response = array();
		if ( $type !== 'GET' ) {
			$dataJson = json_encode( $data, JSON_PRETTY_PRINT );
		}
		$authorization   = '';
		$order_timestamp = '';
		if ( null !== $hash ) {
			$authorization = "Authorization: $hash";
		}
		if ( null !== $timestamp ) {
			$order_timestamp = "Timestamp: $timestamp";
		}
		$headers = array(
			'Cache-Control: no-cache',
			'Content-Type: application/json',
			'Accept: application/json',
			$authorization,
			$order_timestamp
		);
		$curl = curl_init();
		curl_setopt_array(
			$curl, [
				CURLOPT_URL            => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING       => 'UTF-8',
				CURLOPT_MAXREDIRS      => 10,
				CURLOPT_TIMEOUT        => 30,
				CURLOPT_CUSTOMREQUEST  => $type,
				CURLOPT_POSTFIELDS     => $dataJson,
				CURLOPT_HTTPHEADER     => $headers,
			]
		);
		
		$response = curl_exec( $curl );
		$this->checkError( curl_getinfo( $curl ) );
		if ( ! curl_errno( $curl ) ) {
			if ( ! empty( $response ) ) {
				return json_decode( $response, true );
			}
		}
		
		curl_close( $curl );
		
		return false;
	}
	
	/**
	 * @param array $info
	 *
	 */
	protected function checkError( $info ) {
		$msg = null;
		if ( ! in_array( $info['http_code'], array(
				200,
				201
			)
		) ) {
			
			switch ( $info['http_code'] ) {
				case 400:
					$msg = '400 Bad Request – niepoprawne dane lub format komunikatu';
					break;
				case 401:
					$msg = '401 Unauthorized – błąd uwierzytelnienia';
					break;
				case 403:
					$msg = '403 Forbidden – brak uprawnień do zasobu';
					break;
				case 404:
					$msg = '404 Not Found – URL lub zasób nie został odnaleziony';
					break;
				case 409:
					$msg = '409 Confict – konfikt identyfkacci zasobu (przykładowo numer zamówienia PayPo i numer zamówienia Sprzedawcy identyfkucą różne obiekty)';
					break;
				case 503:
					$msg = '503 Service Unavailable';
					break;
				default:
					$msg = $info['http_code'] . ' Unknown Error';
			}
			$this->log( 'log date: ' .  date("Y-m-d") .  "\n" .
			            'log time: ' . date("h:i:s a", time())  . ' | ' . microtime(true) . "\n" . ' Error during request. Reason: ' . $msg . "\n" .
			            'Request URL: '. $info['url'] . "\n" .
			            'END ERROR RESPONSE'.  "\n" .  ' =====================================' . "\n");
		} else{
			$this->log( 'log date: ' .  date("Y-m-d") .  "\n" .
			            'log time: ' . date("h:i:s a", time())  . ' | ' . microtime(true) . "\n" .
			            'Response Code: ' . $info['http_code'] . ' Response Data: ' . var_export( $info, true ) . "\n".
			            'END RESPONSE FROM : ' . $info['url'] .  "\n" .  '  =====================================' . "\n"  );
		}
	}
	
	/**
	 * Validate response params
	 *
	 * @param array $response
	 *
	 * @return boolean
	 */
	public function validateResponse( $response ) {
		if ( $response['order_crc'] !== md5( $this->merchantId . '|' . $response['foreign_id'] . '|' . $response['order_amount'] . '|' . $this->merchantKey ) ) {
			$this->log('log date: ' .  date("Y-m-d") .  "\n" .
			           'log time: ' . date("h:i:s a", time()) . ' | ' . microtime(true) . "\n" . 'Invalid CRC' );
			return false;
		}
		$this->log( 'log date: ' .  date("Y-m-d") .  "\n" .
		            'log time: ' . date("h:i:s a", time())  . ' | ' . microtime(true) . "\n" . 'Response Validate data: '
		            . var_export( $response, true ). "\n" .
		            'END VALIDATE RESPONSE'.  "\n" .  ' =====================================' . "\n" );
		
		return true;
	}
}