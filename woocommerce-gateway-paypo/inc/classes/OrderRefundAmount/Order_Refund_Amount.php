<?php

namespace PayPo\OrderRefundAmount;


use PayPo\HandleOrderStatuses\Handle_Order_statuses;
use PayPo\PayPo\PayPo;
use PayPo\PayPoConnect\PayPo_Connect;

class Order_Refund_Amount {
	/**@var string $metabox_id */
	private $metabox_id = 'paypo_order_refund';
	
	const PAYPO_REFUND_NONCE = 'paypo_refund_nonce';
	const PAYPO_ORDER_REFUND_AMOUNT = 'paypo_order_refund_amount';
	
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'paypo_refund_metabox' ) );
		add_action( 'save_post', array( $this, 'save_order_refund_amount' ), 9999, 1 );
	}
	
	public function paypo_refund_metabox() {
		global $post;
		
		$order         = wc_get_order( $post->ID );
		$refund_amount = get_post_meta( $post->ID, '_paypo_refund_amount', true );
		if ( ! empty( $order ) ) {
			if ( $order->get_payment_method() === 'paypo' ) {
				if ( $order->get_status() === 'completed' ) {
					add_meta_box( $this->metabox_id, __( 'Zwrot (Nowa wartość pozostała do zapłaty)', 'woocommerce-paypo-payment-gateway' ),
						array( $this, 'refund_metabox' ), 'shop_order', 'side', 'core' );
				}
			}
		}
	}
	
	public function refund_metabox() {
		global $post;
		$order         = new \WC_Order( $post->ID );
		$refund_amount = get_post_meta( $post->ID, '_paypo_refund_amount', true );
		
		include( WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEMPLATE_DIR . 'admin/order-refund-metabox.php' );
	}
	
	/**
	 * @param $order_id
	 *
	 * @return bool|mixed
	 */
	public function save_order_refund_amount( $order_id ) {
		global $post;
		if ( true === $this->check_capabilities_and_nonce( $order_id ) ) {
			$order = new \WC_Order( $post->ID );
			$refund_amount = $_POST[ self::PAYPO_ORDER_REFUND_AMOUNT ];
			if ( '' !== $refund_amount ) {
				$order_total = (float) $order->get_total();
				if ( $order_total < (float) $refund_amount ) {
					$order->add_order_note( __( 'Nowa kwota do zapłaty nie może być większa niż kwota zamówienia' ) );
					$refund_amount = '';
				}
				if ( $order->get_status() !== 'refunded' ) {
					$res = $this->paypo_connect()->sendModify( $order, 'REFUND', $refund_amount );
					if ( $res['knk_status'] === 'OK' ) {
						if ( $res['knk_order_status'] === 'REFUND' ) {
							if ( (int) $res['knk_status_code'] === 210 ) {
								if ( 0 === (int) $refund_amount ) {
									$order->add_order_note( __( 'Zwrót całkowity' ) );
									$order->update_status('refunded', __( 'Status zamówienia: Zwrócone:' ) );
									$order->save();
								}else{
									$order->add_order_note( __( 'Nowa kwota zamówienia: ' . $refund_amount . 'zł. (Zwrót częściowy)' ) );
								}
							}
						}
					}else{
						if (  0 === (int) $refund_amount ) {
							return false;
						}
						update_post_meta( $order_id, '_wrong_amount', $refund_amount );
						$order->add_order_note( __( 'Nowa kwota zamówienia nie może być większa niż poprzednio ustalona kwota' ) );
						return false;
					}
					update_post_meta( $order_id, '_paypo_refund_amount', esc_attr( $refund_amount ) );
					update_post_meta( $order_id, '_wrong_amount', '' );
				}
			}
		}
	}
	
	/**
	 * PayPo_Connect
	 */
	private function paypo_connect() {
		$merchant_id  = get_option( 'paypo_merchant_id' );
		$merchant_key = get_option( 'paypo_merchant_key' );
		$is_sandbox   = get_option( 'is_paypo_sandbox_url_set' );
		
		if ( ! empty( $merchant_id ) && ! empty( $merchant_key ) ) {
			return new PayPo_Connect( $merchant_id, $merchant_key, null, $is_sandbox );
		}
		
		return false;
	}
	
	/**
	 * Check the capabilities and nonce for order
	 *
	 * @param $order_id
	 *
	 * @return bool
	 */
	private function check_capabilities_and_nonce( $order_id ) {
		if ( ! isset( $_POST[ self::PAYPO_REFUND_NONCE ] ) ) {
			return false;
		}
		
		$nonce = $_REQUEST[ self::PAYPO_REFUND_NONCE ];
		
		if ( ! wp_verify_nonce( $nonce ) ) {
			return false;
		}
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return false;
		}
		
		if ( 'page' === $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $order_id ) ) {
				return false;
			}
		} else {
			if ( ! current_user_can( 'edit_post', $order_id ) ) {
				return false;
			}
		}
		
		return true;
	}
}