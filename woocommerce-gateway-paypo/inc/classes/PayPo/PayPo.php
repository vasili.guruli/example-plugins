<?php
/**
 * PayPo order class.
 *
 * @package PayPo
 */

namespace PayPo\PayPo;

use PayPo\HandleOrderStatuses\Handle_Order_statuses;
use PayPo\OrderRefundAmount\Order_Refund_Amount;
use PayPo\PayPoConnect\PayPo_Connect;
use PayPo\PayPoIpnHandler\PayPo_Ipn_Handler;
use PayPo\PayPoSettings\PayPo_Settings;


/**
 * PayPoOrder class. The flow for order are made here.
 *
 * @package PayPo
 */
class PayPo extends PayPo_Settings {
	
	/**@var PayPo_Connect $paypo_connect */
	public static $paypo_connect;
	/**@var array $paypo_return_url_status */
	protected $paypo_return_url_status = array(
		'ok'  => 'OK',
		'err' => 'ERR',
	);
	/**@var \DateTime $timestamp */
	protected $dateTime;
	/**@var int $order_timestamp*/
	protected $order_timestamp;
	
	public function __construct() {
		parent::__construct();
		
		if ( null === self::$paypo_connect ) {
			self::$paypo_connect = new PayPo_Connect(
				$this->settings['merchant_id'],
				$this->settings['merchant_api_token'],
				null, $this->sandbox
			);
		}
		
		$this->title       = $this->get_option( 'method_title' );
		$this->description = $this->get_option( 'method_description' );
		$this->notify_url  = WC()->api_request_url( str_replace( '\\', '/', strtolower( PayPo_Settings::class ) ) );
		
		new PayPo_Ipn_Handler( self::$paypo_connect, $this );
		new Handle_Order_statuses( self::$paypo_connect, $this );
		$this->dateTime = new \DateTime();
		$this->order_timestamp = $this->dateTime->getTimestamp();
		
		$this->hooks();
		$this->get_paypo_settings_options();
	}
	
	/**
	 * Plugin hooks
	 */
	protected function hooks() {
		add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page' ) );
		add_filter( 'woocommerce_available_payment_gateways', array(
			$this,
			'display_paypo_gateway_in_checkout_page'
		), 999, 1 );
		
		add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thank_you_page' ), 999, 1 );
	}
	
	/**
	 * Thank you page. Order have received
	 *
	 * @param $order_id
	 */
	public function thank_you_page( $order_id ) {
		if ( ! empty( $order_id ) ) {
			$order = new \WC_Order( $order_id );
			if ( isset( $_GET['paypo_redirect'] ) && $_GET['paypo_redirect'] === 'true' ) {
				if ( isset( $_GET['?status'] ) ) {
					$paypo_status = str_replace( '?', '', $_GET['?status'] );
					if ( $paypo_status === $this->paypo_return_url_status['ok'] ) {
						$status = $order->get_status();
						if ( ! in_array( $status, [ 'pending', 'processing', 'completed', 'cancelled' ] ) ) {
							$order->update_status( 'on-hold', __( 'Płatność wstrzymana.', 'woocommerce-paypo-payment-gateway' ) );
							WC()->cart->empty_cart();
						}
					}
					
					if ( $paypo_status === $this->paypo_return_url_status['err'] ) {
						$order->update_status( 'pending', __( 'Payment is pending', 'woocommerce-paypo-payment-gateway' ) );
						wp_redirect( $order->get_checkout_payment_url() );
						exit();
					}
				}
			}
		}
	}
	
	/**
	 * Get options from paypo settings
	 *
	 */
	private function get_paypo_settings_options() {
		if ( ! empty( $this->settings['merchant_id'] ) && ! empty( $this->settings['merchant_api_token'] ) ) {
			$this->add_paypo_settings_options( 'paypo_merchant_id', $this->settings['merchant_id'] );
			$this->add_paypo_settings_options( 'paypo_merchant_key', $this->settings['merchant_api_token'] );
		}
		
		$this->add_paypo_settings_options( 'is_paypo_sandbox_url_set', $this->sandbox );
		$this->add_paypo_settings_options( 'is_debug_mod', $this->settings['debug_log'] );
	}
	
	/**
	 * @param string $option_name
	 * @param string $option_value
	 *
	 * @return void
	 */
	protected function add_paypo_settings_options( $option_name, $option_value ) {
		if ( false !== get_option( $option_name ) ) {
			update_option( $option_name, $option_value );
		} else {
			add_option( $option_name, $option_value, null, 'no' );
		}
	}
	
	/**
	 * Display PayPo Gateway in checkout page
	 *
	 * @param array $payment_gateways
	 *
	 * @return array available payment methods
	 */
	public function display_paypo_gateway_in_checkout_page( $payment_gateways ) {
		if ( ! empty( $payment_gateways ) ) {
			$order_total = \WC_Payment_Gateway::get_order_total();
			if ( isset( $payment_gateways['paypo'] ) ) {
				if ( 'yes' === $payment_gateways['paypo']->enabled ) {
					if ( isset( $payment_gateways['paypo']->settings['order_limit_min_amount'] ) ) {
						$order_minimum_amount = (float) $payment_gateways['paypo']->settings['order_limit_min_amount'];
						$order_maximum_amount = (float) $payment_gateways['paypo']->settings['order_limit_max_amount'];
						if ( $order_minimum_amount > 0 ) {
							if ( $order_total < $order_minimum_amount ) {
								unset( $payment_gateways['paypo'] );
							}
						}
						if ( $order_maximum_amount > 0 ) {
							if ( $order_total > $order_maximum_amount ) {
								unset( $payment_gateways['paypo'] );
							}
						}
					}
				}
			}
		}
		
		return $payment_gateways;
	}
	
	/**
	 * Fill PayPoConnect class with data.
	 *
	 * @param \WC_Order $order
	 */
	public function set_order_details_for_paypo( $order ) {
		self::$paypo_connect->setParam( 'Order', 'foreign_id', $order->get_order_key() );
		self::$paypo_connect->setParam( 'Order', 'order_id', 'paypo_' . $order->get_order_number() );
		self::$paypo_connect->setParam( 'Order', 'order_amount', ceil( $order->get_total() * 100 ) );
		self::$paypo_connect->setParam( 'Customer', 'customer', $order->get_billing_first_name() . ' ' . $order->get_billing_last_name() );
		self::$paypo_connect->setParam( 'Customer', 'email', $order->get_billing_email() );
		self::$paypo_connect->setParam( 'Customer', 'phone', $order->get_billing_phone() );
		self::$paypo_connect->setParam( 'Customer', 'address', $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() );
		self::$paypo_connect->setParam( 'Customer', 'postal', $order->get_billing_postcode() );
		self::$paypo_connect->setParam( 'Customer', 'city', $order->get_billing_city() );
		self::$paypo_connect->setParam( 'Customer', 'country', $order->get_billing_country() );
		self::$paypo_connect->setParam( 'Customer', 'trusted_customer', '' );
		
		self::$paypo_connect->setParam( 'Shipment', 'shipping_address', $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2() );
		self::$paypo_connect->setParam( 'Shipment', 'shipping_postal', $order->get_shipping_postcode() );
		self::$paypo_connect->setParam( 'Shipment', 'shipping_city', $order->get_shipping_city() );
		self::$paypo_connect->setParam( 'Shipment', 'shipping_country', $order->get_shipping_country() );
		
		self::$paypo_connect->setReturnUrl( $order->get_checkout_order_received_url() . '&paypo_redirect=true&' );
		self::$paypo_connect->setNotifyUrl( $this->notify_url );
		self::$paypo_connect->setCancelUrl( $order->get_cancel_order_url_raw() );
	}
	
	/**
	 * Calculate HMAC to sign api request
	 *
	 * @param string $method
	 * @param string $endpoint
	 * @param int $time
	 *
	 * @return string|boolean
	 */
	protected function calculate_hmac( $method, $endpoint, $time ) {
		if ( '' === $this->settings['merchant_api_token'] ) {
			return false;
		}
		
		$json = json_encode( self::$paypo_connect->prepareCreateCall(), JSON_UNESCAPED_SLASHES );
		
		$data = $method . "+" . $endpoint . "+" . $json . "+" . $time;
		
		return base64_encode( hash_hmac( 'sha256', $data, $this->settings['merchant_api_token'], true ) );
	}
	
	/**
	 * Generate payment form
	 *
	 * @param int $order_id
	 *
	 */
	public function generate_payment_form( $order_id ) {
		$order = new \WC_Order( $order_id );
		if ( empty( get_post_meta( $order_id, '_paypo_order_status', true ) ) ) {
			add_post_meta( $order_id, '_paypo_order_status', 'paypo_order_created', true );
		}
		$this->set_order_details_for_paypo( $order );
		$hash     = $this->calculate_hmac( 'POST', 'orders/register', $this->order_timestamp );
		$redirect = self::$paypo_connect->orderRegister( $hash, $this->order_timestamp, self::$paypo_connect->prepareCreateCall() );
		if ( ! empty( $redirect ) ) {
			if ( isset( $redirect['status'] ) ) {
				if ( $redirect['status'] === 201 || $redirect['status'] === 200 ) {
					$this->order_confirm_hash( $order_id, 'PUT', 'orders/confirm' );
					if ( isset( $redirect['redirect_url'] ) ) {
						include_once( WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEMPLATE_DIR . 'frontend/checkout-form-fields.php' );
					}
				} else {
					include_once( WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEMPLATE_DIR . 'frontend/checkout-form-fields-error.php' );
				}
			}
		}
	}
	
	
	/**
	 * @param $order_id
	 * @param $method
	 * @param $endpoint
	 */
	protected function order_confirm_hash( $order_id, $method, $endpoint ) {
		$order_confirm_hash = $this->calculate_hmac( $method, $endpoint, $this->order_timestamp );
		
		update_post_meta( $order_id, '_order_confirm_hash', $order_confirm_hash );
	}
	
	/**
	 * Process payment for order
	 *
	 * @param int $order_id
	 *
	 * @return array
	 */
	public function process_payment( $order_id ) {
		return array(
			'result'   => 'success',
			'redirect' => ( new \WC_Order( $order_id ) )->get_checkout_payment_url( true )
		);
	}
	
	/**
	 * Display payment form
	 *
	 * @param int $order_id
	 */
	public function receipt_page( $order_id ) {
		echo $this->generate_payment_form( $order_id );
	}
}