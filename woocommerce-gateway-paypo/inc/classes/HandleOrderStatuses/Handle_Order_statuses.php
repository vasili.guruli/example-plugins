<?php
/**
 * Handle_Order_statuses class.
 *
 * @package PayPo
 */

namespace PayPo\HandleOrderStatuses;

use PayPo\PayPo\PayPo;
use PayPo\PayPoConnect\PayPo_Connect;

/**
 * Class Handle_Order_statuses. Should handles order statuses change.
 */
class Handle_Order_statuses {
	
	/** @var PayPo_Connect $paypo_connect */
	protected $paypo_connect;
	/** @var PayPo $paypo */
	protected $paypo;
	
	const PAYPO_ORDER_COMPLETED = 'COMPLETED';
	const PAYPO_ORDER_CANCELED = 'CANCELED';
	const PAYPO_ORDER_REFUND = 'REFUND';
	const PAYPO_ORDER_NEW = 'NEW';
	const PAYPO_ORDER_PROCESSING = 'PROCESSING';
	const PAYPO_ORDER_DELIVERED = 'DELIVERED';
	
	/**
	 * @param PayPo $paypo
	 * @param PayPo_Connect $paypo_connect
	 */
	public function __construct( $paypo_connect, $paypo ) {
		$this->paypo_connect = $paypo_connect;
		$this->paypo         = $paypo;
		
		add_action( 'woocommerce_order_status_changed', array( $this, 'handle_order_status_changed' ), 10, 3 );
	}
	
	/**
	 * Handle order status changes
	 *
	 * @param int $order_id
	 * @param string $status_old
	 * @param string $status_new
	 *
	 * @return bool|void
	 */
	public function handle_order_status_changed( $order_id, $status_old, $status_new ) {
		$order = new \WC_Order( $order_id );
		if ( $order->get_meta( '_old_status' ) ) {
			update_post_meta( $order_id, '_old_status', $status_old );
		} else {
			update_post_meta( $order_id, '_old_status', 'pending' );
		}
		if ( $order->get_payment_method() !== 'paypo' ) {
			return;
		}
		$this->paypo->set_order_details_for_paypo( $order );
		switch ( $status_new ) {
			case 'completed':
				if ( in_array( $this->get_statuses_from_paypo_order_details( $order ), array(self::PAYPO_ORDER_PROCESSING) ) ){
					$this->paypo_connect->sendModify( $order, self::PAYPO_ORDER_COMPLETED );
				}
				break;
			case 'cancelled':
				if ( ! in_array( $status_old, [ 'pending', 'processing', 'on-hold' ] ) ) {
					$order->add_order_note( 'Zmiana statusu zamówienia z ' . $status_old . ' na anulowanie jest niemożliwe' );
					
					return false;
				}
				if ( in_array( $this->get_statuses_from_paypo_order_details( $order ),
					array(self::PAYPO_ORDER_NEW, self::PAYPO_ORDER_PROCESSING) ) ){
					$this->paypo_connect->sendModify( $order, self::PAYPO_ORDER_CANCELED );
				} else {
					$order->add_order_note( 'Zmiana statusu zamówienia z ' . $status_old . ' na anulowanie jest niemożliwe' );
				}
				break;
			case 'refunded' :
				$refund_amount = get_post_meta( $order_id, '_paypo_refund_amount', true );
				if ( isset( $refund_amount ) ) {
					$refund_amount = (int) $refund_amount;
					if ( $refund_amount === 0 ) {
						return false;
					}
				}
				if ( in_array( $this->get_statuses_from_paypo_order_details( $order ),
					array(self::PAYPO_ORDER_DELIVERED, self::PAYPO_ORDER_COMPLETED) )  ){
					$this->paypo_connect->sendModify( $order, self::PAYPO_ORDER_REFUND );
				}
				break;
			default:
				break;
		}
	}
	
	/**
	 * @param \WC_Order $order
	 *
	 * @return string
	 */
	protected function get_statuses_from_paypo_order_details( $order ) {
		$order_details = $this->paypo_connect->retrieve_paypo_order_details( $order );
		$order_status  = '';
		if ( $order_details['knk_status'] === 'OK' ) {
			if ( $order_details['knk_status_code'] === 200 ) {
				$order_status = $order_details['knk_order_status'];
			}
		}
		
		return $order_status;
	}
}