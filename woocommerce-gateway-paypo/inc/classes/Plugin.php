<?php
/**
 * Plugin init class.
 *
 * @package PayPo
 */

namespace PayPo;
use PayPo\OrderRefundAmount\Order_Refund_Amount;

/**
 * Class Plugin. Register PayPo Payment gateway
 *
 * @package PayPo
 * */
class Plugin {
	public function __construct() {
		add_filter( 'woocommerce_payment_gateways', array( $this, 'register_paypo_payment' ) );
		add_filter( 'plugin_action_links_' . WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_BASENAME, array(
			$this,
			'action_links'
		), 10, 2 );
		
		$this->text_domain();
		$this->add_order_metabox();
	}
	
	/**
	 * Custom Order Meta Box for refunds
	 */
	protected function add_order_metabox() {
		if ( is_admin() ) {
			new Order_Refund_Amount();
		}
	}

	public function register_paypo_payment( $methods ) {
		$methods[] = 'PayPo\\PayPo\\PayPo';

		return $methods;
	}

	/**
	 * Show action links on plugins page.
	 *
	 * @param array $links
	 *
	 * @return array
	 */
	public function action_links( $links ) {
		$custom_links = array(
			'settings' => sprintf( '<a href="%s">%s</a>', esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=paypo' ) ),
				__( 'Settings', 'woocommerce-paypo-payment-gateway' ) ),
			'docs'     => sprintf( '<a href="%s" target="_blank">%s</a>', esc_url( 'https://paypo.pl/home/jaktodziala' ),
				__( 'Documentation', 'woocommerce-paypo-payment-gateway' ) )
		);

		return array_merge( $custom_links, $links );
	}
	
	/**
	 * Load Plugin text Domain
	 *
	 */
	public function text_domain() {
		load_plugin_textdomain( 'woocommerce-paypo-payment-gateway', false,
			WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEXT_DOMAIN_BASENAME );
	}
}