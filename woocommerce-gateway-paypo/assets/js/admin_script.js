(function($){
    $('#woocommerce_paypo_debug_log').on('change', function(){
        if ( $(this).val() === 'yes' ) {
            $('#woocommerce_paypo_log_file').closest("tr").show();
        } else{
            $('#woocommerce_paypo_log_file').closest("tr").hide();
        }
    });
    if ( paypo.debug_mode === 'yes'){
        $('#woocommerce_paypo_log_file').closest("tr").show();
    }else{
        $('#woocommerce_paypo_log_file').closest("tr").hide();
    }
})(jQuery);