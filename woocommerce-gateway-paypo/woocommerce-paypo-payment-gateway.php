<?php
/**
 * Plugin Name: WooCommerce PayPo Payment Gateway
 * Description: Integracja z systemem płatności PayPo
 * Version: 1.0
 * Author: vasilguruli|Inspire Labs
 * Author URI: https://inspirelabs.pl
 * Text Domain: woocommerce-paypo-payment-gateway
 *
 * @package PayPo
 */

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'You don\'t have enaugh permissions to access this page' );
}
include __DIR__ . '/vendor/autoload.php';
add_action( 'plugins_loaded', function () {
	// Define plugin directory for inclusions
	if ( ! defined( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_DIR' ) ) {
		define( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_DIR', dirname( __FILE__ ) );
	}
	// Define plugin URL for assets
	if ( ! defined( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_URL' ) ) {
		define( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_URL', plugins_url( '', __FILE__ ) );
	}
	// Define templates directory
	if ( ! defined( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEMPLATE_DIR' ) ) {
		define( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEMPLATE_DIR', WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_DIR . '/templates/' );
	}
	// Define plugin basename
	if ( ! defined( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_BASENAME' ) ) {
		define( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_BASENAME', plugin_basename( __FILE__ ) );
	}
	// Define text domain basename
	if ( ! defined( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEXT_DOMAIN_BASENAME' ) ) {
		define( 'WOOCOMMERCE_PAYPO_PAYMENT_GATEWAY_TEXT_DOMAIN_BASENAME', dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
	if ( class_exists( 'PayPo\\Plugin' ) ) {
		new \PayPo\Plugin();
	}
}, 99 );