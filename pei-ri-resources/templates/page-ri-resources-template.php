<?php

/**
 * Template Name: RI Resource Page Template
 *
 *
 * Demonstration purpose only - NEED TO BE CHANGED LATER
 */

get_header();

$posts    = array( 'post_type' => 'content', 'posts_per_page' => 10, 'post_status' => 'publish' );

$contents = new WP_Query( $posts );


if ( ! empty( $contents ) ) {
	foreach ( $contents as $content ) {
		$is_product   = get_field( 'is_this_product', $content->ID );

		$content_type = ( true === $is_product ? 'product_content_type' : 'related_content' );
		if ( have_rows( $content_type, $content->ID ) ) : ?>

			<?php while ( have_rows( $content_type, $content->ID ) ) : the_row(); ?>
				<?php
				$page_title = get_the_title( $content->ID );
				?>

			<?php endwhile; ?>

		<?php endif;
	}
}


get_footer();