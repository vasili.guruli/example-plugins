<?php
/**
 * @var WP_Post $post
 */
$institution = get_post_meta( $post->ID, 'contact_institution', true );

$primary_address    = get_field( 'field_63a462bad982d', $institution );
$additional_address = get_field( 'additional_address', $institution );

$addresses = array(
		'primary_address'    => $primary_address,
		'additional_address' => $additional_address,
);

$i = 1;

$selected                  = get_post_meta( $post->ID, 'ri_contact_address', true );
$primary_address           = json_encode( $addresses['primary_address'] );
$institution_title         = esc_attr( get_the_title( $institution ) );


$sorted_addresses = array();

foreach ($addresses['additional_address'] as $address ) {
	$key = $address['country']['value'];
	// Allow duplicate countries keys e.g PL|PL
	$sorted_addresses[ $key . '_' . $i ] = $address;
	$i++;
}

ksort($sorted_addresses);

?>
<div class="acf-field">
	<div class="acf-input">
		<h4 class="acf-select ri-addresses-title">
			<?php esc_html_e( "Select one address from", RI_RESOURCES_PLUGIN_TEXT_DOMAIN ); ?>
			<i class="institution-title">
				<a href="<?php echo esc_url( get_edit_post_link( $institution ) ) ?>">
					<?php echo $institution_title ?></a>
			</i><?php esc_html_e( " addresses for contact", RI_RESOURCES_PLUGIN_TEXT_DOMAIN ); ?>
		</h4>
		<select id="acf-select ri-contact-addresses" class="ri-contact-addresses" name="<?php echo esc_attr( 'ri_contact_address' ) ?>">
			<option value="select_address"><?php esc_html_e( 'Select Address', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ) ?></option>
			<?php if ( $institution ) : ?>
				<?php if ( isset( $addresses['primary_address'] ) ) :  ?>
					<option <?php echo selected( $selected, $primary_address ); ?> value='<?php echo esc_attr( $primary_address ); ?>'><?php esc_html_e( 'Primary Address' ) ?></option>
				<?php endif; ?>
				<?php foreach( $sorted_addresses as $address ) : ?>
					<?php $secondary_address = json_encode( $address ); ?>
					<option <?php echo selected( $selected, $secondary_address ); ?> value='<?php echo esc_attr( $secondary_address ) ?>'>&nbsp;<?php echo $address['country']['label'] ?> - <?php echo $address['city'] ?></option>

				<?php endforeach; ?>
			<?php else : ?>
				<option value="no_address"><?php esc_html_e( 'No Addresses are available', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ) ?></option>
			<?php endif; ?>
		</select>
	</div>
</div>
