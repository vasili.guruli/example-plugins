<?php
/**
 * @var \WP_Post $post
 */
$contacts = get_post_meta( $post->ID, 'contacts', true );

$statuses = array(
	'trash',
	'draft',
	'pending'
);

$new_data   = array();
$update     = false;
$label      = '';
$class      = '';
$trash_icon = '';
?>
    <div class="acf-field">
        <div class="acf-input">
            <table class="acf-table">
                <thead>
                <tr>
                    <th><?php echo esc_attr( 'ID' ); ?></th>
                    <th><?php echo esc_attr( 'Name' ); ?></th>
                    <th><?php echo esc_attr( 'Email' ); ?></th>
                    <th><?php echo esc_attr( 'Phone' ); ?></th>
                    <th><?php echo esc_attr( 'Job Title' ); ?></th>
                </tr>
                </thead>
                <tbody>
				<?php
				foreach ( $contacts as $contact => $id ) : ?>
					<?php
					$contact_details = array(
						'ID'        => $id,
						'full_name' => esc_attr( get_the_title( $id ) ),
						'email'     => get_post_meta( $id, 'email', true ),
						'phone'     => get_post_meta( $id, 'phone', true ),
						'job_title' => get_post_meta( $id, 'job_title', true ),
					);
					$status          = get_post_status( $id );
					$action_link     = admin_url( 'post.php?post=' . $id . '&action=edit' );
					if ( false === $status ) {
						unset( $contacts[ $contact ] );
						$new_data = $contacts;
						$update   = true;
					}
					if ( 'trash' === $status ) {
						$action_link = admin_url( 'edit.php?post_status=trash&post_type=contact' );
						$label       = '&nbsp; - Trash';
						$class       = 'trash';
						$trash_icon  = '<i class="trash-icon"></i>';
					}
					?>
                    <tr class="acf-row">
						<?php if ( false !== $status ) : ?>
                            <td class="acf-field <?php echo esc_attr( $class ); ?>"><a
                                        class="<?php echo esc_attr( $class ); ?>"
                                        href="<?php echo esc_url( $action_link ); ?>"><?php echo esc_attr( $contact_details['ID'] ); ?></a>
                            </td>
                            <td class="acf-field <?php echo esc_attr( $class ); ?>"><a
                                        class="<?php echo esc_attr( $class ); ?>"
                                        href="<?php echo esc_url( $action_link ); ?>"><?php echo esc_attr( $contact_details['full_name'] ); ?><?php echo $label ?></a>
                            </td>
                            <td class="acf-field <?php echo esc_attr( $class ); ?>"><?php echo esc_attr( $contact_details['email'] ); ?></td>
                            <td class="acf-field <?php echo esc_attr( $class ); ?>"><?php echo esc_attr( $contact_details['phone'] ); ?></td>
                            <td class="acf-field <?php echo esc_attr( $class ); ?>"><?php echo esc_attr( $contact_details['job_title'] ); ?><?php echo $trash_icon; ?></td>
						<?php endif; ?>
                    </tr>
				<?php endforeach; ?>
				<?php if ( $update ) : ?>
					<?php update_post_meta( $post->ID, 'contacts', $new_data ); ?>
				<?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php