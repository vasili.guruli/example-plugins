<!-- BLAIZE_FEATURE ri-resource-view -->
<div id="theresource-directory">
	<div class="theresource-search" id="directorysearch"></div>
	<div id="theresource-results">
		<main id="directoryhits"></main>
		<div id="directory-infinite-hits"></div>
		<div id="hits-per-page"></div>
		<div id="directory-pagination"></div>
	</div>
	<script type="text/html" id="tmpl-directory-hit">
		<div class="item-header">
			 <# if ( data.images.full ) { #>
                    <div class="ais-hits--thumbnail">
                        <# if ( data.images.full.url ) { #>
                        <a href="{{ data.permalink }}" title="{{ data.post_title }}" class="ais-hits--thumbnail-link">
                            <img src="{{ data.images.full.url }}" alt="{{ data.post_title }}" title="{{ data.post_title }}" itemprop="image" />
                        </a>
                        <# } #>
                    </div>
			<# }else{ #>
				<div class="ri-no-image-placeholder"></div>
			<# } #>				
			<h3 class="entry-title td-module-title directory-title" itemprop="name headline">
				<a id="directory-title" href="{{ data.permalink }}" title="{{ data.post_title }}" class="ais-hits--title-link ri-directory-title" itemprop="url">
					{{ data.post_title }}
				</a>
			</h3>
		</div>
	</script>

</div>
<!-- BLAIZE_FEATURE ri-resource-view -->
