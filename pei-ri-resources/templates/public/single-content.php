<?php get_header(); ?>

	<!-- <div id="td-outer-wrap" class="td-theme-wrap"> -->
	<div class="td-main-content-wrap td-container-wrap">
		<div class="td-container">
			<div class="td-pb-row">
				<?php
				while ( have_posts() ) : the_post();

					include ( RI_RESOURCES_PLUGIN_DIR_PATH . 'templates/public/parts/' . get_post_type() . '.php' );

				endwhile; // End of the loop.
				?>
			</div>
		</div>
	</div>
	<!-- </div> -->

<?php get_footer(); ?>