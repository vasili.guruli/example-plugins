<!-- BLAIZE_FEATURE ri-resource-view -->
<div id="content-results">
    <div id="content-search"></div>
    <main id="content-hits-wrapper">
        <ul class="content-type-filters">
            <li class="rtcontent__category Fund ">Funds</li>
            <li class="rtcontent__category Report">Reports</li>
            <li class="rtcontent__category Research">Research</li>
            <li class="rtcontent__category Podcast">Podcasts</li>
            <li class="rtcontent__category Webinar">Webinars</li>
            <li class="rtcontent__category Video">Videos</li>
        </ul>
        <div id="content-hits"></div>
        <div id="content-pagination"></div>
    </main>
    <aside id="content-filters">

    </aside>
</div>

<script type="text/html" id="tmpl-content-hit">
    <div class="rtcontent__item">
        <div class="rtcontent__image-container">
            <div class="rtcontent__image-container">
                <# if ( data.institution_thumb ) { #>
                <a href="{{ data.permalink }}"><img class="rtcontent__logo" alt="{{ data.post_title }}" src="{{ data.institution_thumb }}" title="{{ data.post_title }}" itemprop="image"></a>
                <# }else{ #>
                <# if ( data.images.full ) { #>
                <# if ( data.images.full.url ) { #>
                <a href="{{ data.permalink }}" title="{{ data.post_title }}" class="ais-hits--thumbnail-link">
                    <img class="rtcontent__logo" src="{{ data.images.full.url }}" alt="{{ data.post_title }}" title="{{ data.post_title }}" itemprop="image" />
                </a>
                <# } #>
                <# } else{ #>
                <div class="funds-image-placeholder"></div>
                <# } #>
                <# } #>
            </div>

        </div>
        <div class="rtcontent__content">
            <div class="rtcontent__description">
                <span class="rtcontent__title">{{ data.institution_name }}</span>
                <h2><a class="rtcontent__link" href="{{ data.permalink }}">{{{ data.post_title }}}</a></h2>
                {{{ data.description }}}
            </div>
            <span class="rtcontent__category {{ data.content_type_slug }}">{{{ data.content_type_slug }}}</span>
        </div>
    </div>
</script>




<?php /*
	<div id="theresource-results">
		<main id="theresource-hits-wrapper">
			<p>
				You can type in what you are looking for in <span>Search for product</span> input or refine your search using the filters and click on <span>Apply filters</span>
			</p>
			<div id="theresource-hits">

			</div>
			<div id="theresource-pagination">

			</div>
		</main>
		<aside id="theresource-filters">
		<?php foreach ($this->facets as $facet) { ?>
			<section class="ais-facets" id="theresource-<?= $facet['slug']; ?>">
				<div class="ais-Panel-header">
					<span>
						<h4 class="widgettitle"><?= $facet['title']; ?></h4>
					</span>
				</div>
				<div class="ais-Panel-body">
					<div>
						<div class="ais-RefinementList"></div>
					</div>
				</div>
			</section>
		<?php } ?>
			<section class="ais-facets" id="theresource-clear">
			</section>
		</aside>
	</div>
	</div> */ ?>
<!-- BLAIZE_FEATURE ri-resource-view -->