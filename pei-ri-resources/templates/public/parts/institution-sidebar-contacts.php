<?php if ( ! empty( $contacts ) ) : ?>
	<div class="theresource-contacts">
	  <div class="section-header">CONTACTS</div>
		<ul class="contacts-list">
			<?php foreach ( $contacts as $contact_id ) : ?>
				<?php
				// First/Last name
				$title = get_the_title( $contact_id );

				// Other fields
				$job_title = get_field( 'job_title', $contact_id );
				$email     = get_field( 'email', $contact_id );
				$address   = get_field( 'address', $contact_id );

				// Address
				$has_address    = is_array( $address );
				$address_line_1 = $has_address && ! empty( $address['address_line_1'] ) ? $address['address_line_1'] : '';
				$address_line_2 = $has_address && ! empty( $address['address_line_2'] ) ? $address['address_line_2'] : '';
				$address_line_3 = $has_address && ! empty( $address['address_line_3'] ) ? $address['address_line_3'] : '';
				$city           = $has_address && ! empty( $address['city'] ) ? $address['city'] : '';
				$region         = $has_address && ! empty( $address['region'] ) ? $address['region'] : '';
				$country        = $has_address && ! empty( $address['country']['label'] ) ? $address['country']['label'] : '';
				$postal_code    = $has_address && ! empty( $address['postal_code'] ) ? $address['postal_code'] : '';

				// City, country, region
				$city_country_region = array();
				if ( ! empty( $city ) ) {
					$city_country_region[] = $city;
				}
				if ( ! empty( $country ) ) {
					$city_country_region[] = $country;
				}
				if ( ! empty( $region ) ) {
					$city_country_region[] = $region;
				}

				// Address lines
				$address_lines = array();
				if ( ! empty( $address_line_1 ) ) {
					$address_lines[] = $address_line_1;
				}
				if ( ! empty( $address_line_2 ) ) {
					$address_lines[] = $address_line_2;
				}
				if ( ! empty( $address_line_3 ) ) {
					$address_lines[] = $address_line_3;
				}
				if ( ! empty( $postal_code ) ) {
					$address_lines[] = $postal_code;
				}
				?>
				<div class="contact-container">
					<?php if ( ! empty( $title ) ) : ?>
						<div class="row name"><?php echo esc_html( $title ); ?></div>
					<?php endif; ?>
					<?php if ( ! empty( $job_title ) ) : ?>
						<div class="row job-title"><?php echo esc_html( $job_title ); ?></div>
					<?php endif; ?>
					<?php if ( ! empty( $city_country_region ) ) : ?>
						<div class="row city-country-region">
							<img src="<?php echo esc_url( RI_RESOURCES_PLUGIN_DIR_URL . 'assets/icons/map-marker-alt.svg' ); ?>" width="10" height="10" />
							<span><?php echo esc_html( implode( ', ', $city_country_region ) ); ?></span>
						</div>
					<?php endif; ?>
					<?php if ( ! empty( $address_lines ) ) : ?>
						<div class="row address-lines"><?php echo esc_html( implode( ', ', $address_lines ) ); ?></div>
					<?php endif; ?>
					<?php if ( ! empty( $email ) ) : ?>
						<div class="row email">
							<img src="<?php echo esc_url( RI_RESOURCES_PLUGIN_DIR_URL . 'assets/icons/envelope.svg' ); ?>" width="14" height="14" />
							<span><?php echo esc_html( $email ); ?></span>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>
