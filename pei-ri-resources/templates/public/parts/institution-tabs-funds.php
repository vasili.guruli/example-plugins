<?php if ( $funds->have_posts() ) {
	while ( $funds->have_posts() ) :
		$funds->the_post();
		$content_type = get_field( 'field_634d6b77b4fa0' );
		?>
		<div class="rtcontent__item">
			<div class="rtcontent__image-container">
				<a href="<?php echo esc_url( get_permalink() ); ?>"><img class="rtcontent__logo" alt="<?php echo esc_attr( get_the_title() ); ?>" src="<?php echo esc_url( get_the_post_thumbnail_url() ); ?>"></a>
			</div>
			<div class="rtcontent__content">
				<div class="rtcontent__description">
					<h2><a class="rtcontent__link" href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_attr( get_the_title() ); ?></a></h2>
					<?php the_field( 'ri_product_product_description' ); ?>
				</div>
				<span class="rtcontent__category"><?php echo esc_attr( $content_type->name ); ?></span>
			</div>
		</div>
		<?php
	endwhile;
	wp_reset_query();
} ?>
