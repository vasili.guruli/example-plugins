<?php
/**
 * Template part for displaying single Content
 *
 * @param array $post
 */

use \Pei_Ri_Resources\Helper;

$fields             = array();
$institution        = Helper::get_meta_slug_for_content_type( 'content', 'institution' );
$content_type       = 'field_634d72cc15375';
$content_categories = 'field_634e9201c2266';
$content_link_type  = Helper::get_meta_slug_for_content_type( 'content', 'content_link_type' );
$external_link      = Helper::get_meta_slug_for_content_type( 'content', 'content_url' );
$internal_link      = Helper::get_meta_slug_for_content_type( 'content', 'content_file' );
$description        = Helper::get_meta_slug_for_content_type( 'content', 'content_description' );
$is_product         = get_post_meta( get_the_ID(), 'is_this_product', true );
if ( '1' === $is_product ) {
	$institution       = Helper::get_meta_slug_for_content_type( 'product', 'product_institution' );
	$content_type      = 'field_634d6b77b4fa0';
	$external_link     = Helper::get_meta_slug_for_content_type( 'product', 'product_url' );
	$internal_link     = Helper::get_meta_slug_for_content_type( 'product', 'product_file' );
	$description       = Helper::get_meta_slug_for_content_type( 'product', 'product_description' );
	$content_link_type = Helper::get_meta_slug_for_content_type( 'product', 'product_url_type' );

}

$institution_id      = get_post_meta( get_the_ID(), $institution, true );
$institution_title   = get_the_title( (int) $institution_id );
$content_title       = get_the_title();
$content_type        = get_field( $content_type, get_the_ID() );
$content_type_name   = ( $content_type->name ?? '' );
$content_type_slug   = ( $content_type->slug ?? '' );
$url                 = get_field( $external_link, get_the_ID() );
$content_description = get_field( $description, get_the_ID() );
$categories          = get_field( $content_categories, get_the_ID() );
$link_type           = get_field( $content_link_type, get_the_ID() );

if ( 'internal' === $link_type ) {
	$url = get_field( $internal_link, get_the_ID() );
}
?>

<?php if ( 'content' === get_post_type() ) : ?>
    <div role="main" class="td-pb-span8 td-main-content">
        <div id="main" class="td-ss-main-content">
            <div class="clearfix"></div>
            <article id="post-<?php the_ID(); ?>"
                     class=" td-boxed-layout <?php echo get_post_type(); ?> type-<?php echo get_post_type(); ?> status-publish has-post-thumbnail hentry post <?php echo join( ' ', get_post_class() ); ?>">


				<?php echo Pei_Ri_Resources\Helper::ri_content_breadcrumbs( (int) $institution_id, $institution_title ); ?>
                <div class="td-module-meta-info ri-content-info">
                    <div class="wpb_column vc_column_container td-pb-span8 content_type_info_container">
                        <div class="vc_column_inner content_type">
                            <h3 class="title ri-content-name">
                                <a href="<?php echo esc_url( get_permalink( $institution_id ) ) ?>">
									<?php esc_html_e( $institution_title, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ); ?>
                                </a>
                            </h3>
							<?php the_title( '<h1 class="ri-content-title top-margin">', '</h1>' ); ?>

                                <a href="<?php echo esc_url_raw( $url ); ?>"
                                   class="ri-content-button external-link wpb_button btn <?php echo esc_attr( $content_type_slug ) ?>">
		                            <?php esc_html_e( 'Go to ' ) ?>
		                            <?php echo $content_type_slug; ?>
                                </a>


                        </div>

                    </div>
                    <div class="content-image-container">
                        <div class="ri-image">
	                        <?php
	                        if ( '1' === $is_product ) {
		                        if ( has_post_thumbnail( $institution_id ) ){
			                        echo get_the_post_thumbnail( $institution_id, 'medium', '' );
		                        }
	                        } else{
		                        if ( has_post_thumbnail() ) {
			                        the_post_thumbnail( 'medium' );
		                        }
	                        }
	                        ?>
                        </div>
                    </div>
                </div>

                <div class="td-post-content institution-description">
                    <div class="td-post-source-tags content-type">
                        <span class="content-type <?php echo esc_attr( $content_type_slug ) ?>"><?php esc_html_e( $content_type_name ); ?></span>
                    </div>
					<?php echo $content_description; ?>
                </div>
                <div class="td-post-content institution-description">
                    <div class="categories">
						<?php if ( isset( $categories ) && 0 < count( $categories ) && false !== $categories ) : ?>
                            <div class="content-categories">
                                <span class="categories"><?php esc_html_e( 'CATEGORIES: ' ); ?></span>
								<?php foreach ( $categories

								as $category ) : ?>
								<?php
								$cat_parent  = $category->parent;
								$parent      = get_term( $cat_parent );
								$parent_name = strtolower( $parent->name );
								$class = ( '' !== $parent_name ? 'child-term ' : 'parent-term ' );
								if ( '' !== $parent_name ) {
									$parent_name = ucwords( $parent_name . ': ' );
                                }
                                ?>
                                <span class="<?php echo esc_attr( $category->slug ); ?>">
                                        <?php echo $parent_name; ?>
										<span class="<?php echo $class; ?>">
                                            <a href="<?php echo get_term_link( $category->term_id ) ?>"><?php echo esc_attr( $category->name ); ?></a></span>
                                    </span>
								<?php endforeach; ?>
                            </div>
						<?php endif; ?>
                    </div>
                </div>

            </article><!-- #post-<?php the_ID(); ?> -->

        </div><!-- #main -->
    </div><!--#primary -->
    <div class="td-pb-span4 td-main-sidebar">
        <div class="td-ss-main-sidebar" style="width: auto; position: static; top: auto; bottom: auto;">
            <div class="clearfix"></div>
            <div class="clearfix"></div>
        </div><!-- main sidebar -->
    </div><!-- main sidebar -->
<?php endif; ?>
