<?php
/**
 * Template part for displaying single Institution
 *
 * @param array $post
 */

$fields = array();
?>
<?php if ( 'institution' === get_post_type() ) : ?>
	<?php $institution_id = get_field( 'institution_id', get_the_ID() ); ?>
	<?php $institution_post_id = get_the_ID(); ?>
	<?php
	$contacts = get_posts(
		array(
			'posts_per_page' => -1,
			'post_status'    => 'publish',
			'post_type'      => 'contact',
			'fields'         => 'ids',
			'orderby'        => array( 'title' => 'ASC' ),
			'meta_query' => array(
				array(
					'key'     => 'contact_institution',
					'value'   => $institution_post_id,
					'compare' => '=',
				),
			),
		)
	);
?>
<div role="main" class="td-pb-span8 td-main-content">
	<div id="main" class="td-ss-main-content">
	<div class="clearfix"></div>
	<article id="post-<?php the_ID(); ?>"
			 class=" td-boxed-layout <?php echo get_post_type(); ?> type-<?php echo get_post_type(); ?> status-publish has-post-thumbnail hentry post <?php echo join( ' ', get_post_class() ); ?>">



		<?php // bread ?>
		<div class="td-module-meta-info">
			<ul class="list-group">
				<li class="list-group-item">
					<?php if ( has_post_thumbnail() ) : ?>
						<?php the_post_thumbnail( 'medium' ); ?>
					
					<?php endif; ?>
				</li>
				<li class="list-group-item">
					<?php the_title( '<h1 class="institution-title top-margin">', '</h1>' ); ?>
				</li>
				<li class="list-group-item">
					<p class="top-margin">
						
					</p>
				</li>
			</ul>
		</div>

		<div class="td-post-content institution-description">
			<div class="td-post-source-tags">
				<span class="about-us"><?php echo 'About Us'; ?></span>
			</div>
			<h2 class="institution-title"><?php echo 'About Us'; ?></h2>
			<?php echo get_field( 'description', get_the_ID() ); ?>
		</div>
		<?php include ( RI_RESOURCES_PLUGIN_DIR_PATH . 'templates/public/parts/institution-tabs.php' ); ?>
	</article><!-- #post-<?php the_ID(); ?> -->

	</div><!-- #main -->
</div><!--#primary -->
<div class="td-pb-span4 td-main-sidebar">
	<div class="td-ss-main-sidebar" style="width: auto; position: static; top: auto; bottom: auto;">
		<div class="clearfix"></div>
		<?php include ( RI_RESOURCES_PLUGIN_DIR_PATH . 'templates/public/parts/institution-sidebar.php' ); ?>
		<div class="clearfix"></div>
	</div><!-- main sidebar -->
</div><!-- main sidebar -->
<?php endif; ?>
