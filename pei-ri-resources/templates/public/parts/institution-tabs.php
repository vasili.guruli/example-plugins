<?php
wp_reset_query();

$funds_args = array(
    'post_type'      => 'content',
    'post_status'    => 'publish',
    'posts_per_page' => -1,
    'tax_query'      => array(
        array(
            'taxonomy' => 'content-type',
            'field'    => 'slug',
            'terms'    => 'fund',
        ),
    ),
    'meta_key' => 'ri_product_product_institution',
    'meta_value' => $post->ID
);

$funds = new WP_Query( $funds_args );
?>




<div class="rtabs__container">
    <ul class="rtabs__tab-list | js-rtabs">
        <li class="rtabs__tab-item active"><button data-href="#rtabs-content" class="rtabs__tab-button | js-rtabs-button">Our content</button></li>
        <?php if ( $funds->have_posts() ) : ?>
        <li class="rtabs__tab-item"><button data-href="#rtabs-funds" class="rtabs__tab-button | js-rtabs-button">Our funds</button></li>
        <?php endif; ?>
    </ul>
</div>

<div class="rtcontent__container">
    <div id="rtabs-content" class="rtcontent__box | js-rtabs-content active">
        <?php include ( RI_RESOURCES_PLUGIN_DIR_PATH . 'templates/public/parts/institution-tabs-content.php' ); ?>
    </div>
    <?php if ( $funds->have_posts() ) : ?>
    <div id="rtabs-funds" class="rtcontent__box | js-rtabs-content">
       <?php include ( RI_RESOURCES_PLUGIN_DIR_PATH . 'templates/public/parts/institution-tabs-funds.php' ); ?>
    </div>
     <?php endif; ?>
</div>
