<?php
$post_id    = get_the_ID();
$aum_amount = get_field( 'aum_amount', $post_id );
$aum_year   = get_field( 'aum_year', $post_id );
?>

<?php if ( ! empty( $aum_amount ) ) : ?>
	<?php
	if ( (float) $aum_amount >= 1.0 ) {
		$aum_amount = round( (float) $aum_amount, 3 );
		$suffix     = 'billion';
	} else {
		$aum_amount = round( 1000 * (float) $aum_amount, 0 );
		$suffix     = 'million';
	}
	?>

	<?php if ( '0.000' !== number_format( $aum_amount, 3, '.', '' ) ) : ?>
		<div class="theresource-aum">
			<div class="info-row">
				<span class="aum-label">AUM</span>
				<span>USD&nbsp;<?php echo esc_html( $aum_amount . '&nbsp;' . $suffix ); ?></span>
			</div>
			<?php if ( ! empty( $aum_year ) && preg_match( '/^\d{1,2}\/\d{1,2}\/\d{4}$/', $aum_year ) > 0 ) : ?>
				<?php
				$aum_date = date_parse_from_format( 'd/m/Y', $aum_year );
				$aum_date = gmdate( 'd M Y', gmmktime( 0, 0, 0, $aum_date['month'], $aum_date['day'], $aum_date['year'] ) );
				?>
				<div class="date-row">
					<span>@&nbsp;<?php echo esc_html( str_replace( ' ', '&nbsp;', $aum_date ) ); ?></span>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
<?php endif; ?>
