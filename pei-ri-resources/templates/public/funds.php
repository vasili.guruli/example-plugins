<!-- BLAIZE_FEATURE ri-resource-view -->
<div id="funds-results">
	<div id="funds-search"></div>
	<div id="funds-clear"></div>
	<div id="funds-current-refinements"></div>
	<div id="funds-current-refinements-options" class="hide">
		<button id="funds-show-all-current" class="hide" data-open="no"></button>
		<div id="funds-clear-refi"></div>
	</div>
	<main id="funds-hits-wrapper">
		<div id="funds-hits"></div>
		<div id="funds-pagination"></div>
	</main>
	<aside id="funds-filters">
		<h4 class="ri_widget__title">Strategy</h4>	
		<section class="fake-radio" id="fr-strategy">
			<div id="facet-funds-strategy-clear"></div>
			<div id="facet-funds-strategy"></div>
		</section>
		
		<h4 class="ri_widget__title">Region</h4>	
		<section class="fake-select" id="fs-region">
			<div class="fs-select">See all</div>
			<div class="fs-box">
				<div class="fs-list" id="facet-funds-region">
				</div>
				<div class="fs-buttons" id="facet-funds-region-buttons">
				</div>
			</div>
		</section>
		<h4 class="ri_widget__title">Style</h4>	
		<section class="fake-select" id="fs-style">
			<div class="fs-select">See all</div>
			<div class="fs-box">
				<div class="fs-list" id="facet-funds-style">
				</div>
				<div class="fs-buttons" id="facet-funds-style-buttons">
				</div>
			</div>
		</section>
		<h4 class="ri_widget__title">Asset class</h4>	
		<section class="fake-select" id="fs-assetclass">
			<div class="fs-select">See all</div>
			<div class="fs-box">
				<div class="fs-list" id="facet-funds-assetclass">
				</div>
				<div class="fs-buttons" id="facet-funds-assetclass-buttons">
				</div>
			</div>
		</section>
		<h4 class="ri_widget__title">Asset subclass</h4>	
		<section class="fake-select" id="fs-assetsubclass">
			<div class="fs-select">See all</div>
			<div class="fs-box">
				<div class="fs-list" id="facet-funds-assetsubclass">
				</div>
				<div class="fs-buttons" id="facet-funds-assetsubclass-buttons">
				</div>
			</div>
		</section>
	</aside>
</div>

	<script type="text/html" id="tmpl-funds-hit">
		<div class="rtcontent__item">
			<div class="rtcontent__image-container">
				 <# if ( data.institution_thumb ) { #>
				<a href="{{ data.permalink }}"><img class="rtcontent__logo" alt="{{ data.post_title }}" src="{{ data.institution_thumb }}" title="{{ data.post_title }}" itemprop="image"></a>
				<# }else{ #>
				<div class="funds-image-placeholder"></div>
				<# } #>		
			</div>
			<div class="rtcontent__content">
				<div class="rtcontent__description">
					<span class="rtcontent__title">{{ data.institution_name }}</span>
					<h2><a class="rtcontent__link" href="{{ data.permalink }}">{{{ data.post_title }}}</a></h2>
					{{{ data.description }}}
				</div>
				<span class="rtcontent__category">Funds</span>
			</div>
		</div>
	</script>



	
	<?php /*
	<div id="theresource-results">
		<main id="theresource-hits-wrapper">
			<p>
				You can type in what you are looking for in <span>Search for product</span> input or refine your search using the filters and click on <span>Apply filters</span>
			</p>
			<div id="theresource-hits">

			</div>
			<div id="theresource-pagination">

			</div>
		</main>
		<aside id="theresource-filters">
		<?php foreach ($this->facets as $facet) { ?>
			<section class="ais-facets" id="theresource-<?= $facet['slug']; ?>">
				<div class="ais-Panel-header">
					<span>
						<h4 class="widgettitle"><?= $facet['title']; ?></h4>
					</span>
				</div>
				<div class="ais-Panel-body">
					<div>
						<div class="ais-RefinementList"></div>
					</div>
				</div>
			</section>
		<?php } ?>
			<section class="ais-facets" id="theresource-clear">
			</section>
		</aside>
	</div>
	</div> */ ?>
<!-- BLAIZE_FEATURE ri-resource-view -->
