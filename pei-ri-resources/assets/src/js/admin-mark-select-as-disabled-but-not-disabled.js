( function( $, acf ) {
	const fields = [
		[ '#acf-field_634d6b4fb4f9f-field_634d6b77b4fa0', '.acf-field-634d6b77b4fa0', true, true ], //  Product Content Type
	];
	
	/**
	 * Disable all required Multiselect fields in one go.
	 */
	function initMultiDisableMultiSelectField() {
		for ( let i = 0; i < fields.length; i++ ) {
			const field = fields[ i ];
			
			disableMultiselectField( field[0], field[1], field[2] );
			
			if ( field[2] ) {
				confirmUponRemoval( field[0] );
			}
			
			if ( field[3] ) {
				hideDropdown( field[0] );
			}
		}
	}
	
	/**
	 * Hide dropdown.
	 *
	 * @param string acfFieldID ACF field identifier.
	 */
	function hideDropdown( acfFieldID ) {
		const $field = $( acfFieldID );
		
		if ( ! $field.length ) {
			return;
		}
		
		$field.on( 'select2:open', function ( e ) {
			const $selectList = $( '#select2-' + $field.attr( 'id' ) + '-results' );
			
			$selectList.hide();
			$selectList.closest( 'select2-container' ).hide();
		});
	}
	
	/**
	 * Disable multiselect ACF field so that it is read only.
	 *
	 * @param string  acfFieldID ACF field identifier.
	 * @param string  acfFieldClass ACF field class name.
	 * @param boolean enableRemoval Enable removal?
	 */
	function disableMultiselectField( acfFieldID, acfFieldClass, enableRemoval = false ) {
		const $field = $( acfFieldID );
		
		if ( ! $field.length ) {
			return;
		}
		
		const $row = $( acfFieldClass );
		const $searchField = $( acfFieldClass + ' .select ul li.select2-search' );
		
		$field.prop( 'disabled', false );
		$field.on( 'select2:opening', function ( e ) {
			if ( $( this ).attr( 'readonly' ) || $( this ).is( ':hidden' ) ) {
				e.preventDefault();
			}
		});
		
		$searchField.css( 'display', 'none' );
		
		if ( enableRemoval ) {
			$row.addClass( 'disabled-select enable-removal' );
		} else {
			$row.addClass( 'disabled-select' );
		}
		
		$field.each( function() {
			if ( $( this ).is( '[readonly]' ) ) {
				const $formGroup = $( this ).closest( '.form-group' );
				
				$formGroup.find( 'span.select2-selection__choice__remove' ).first().remove();
				$formGroup.find( 'li.select2-search' ).first().remove();
				$formGroup.find( 'span.select2-selection__clear' ).first().remove();
			}
		} );
	}
	
	/**
	 * Request confirmation from user upon removing selections in multiselect.
	 *
	 * @param string acfFieldID ACF field identifier.
	 */
	function confirmUponRemoval( acfFieldID ) {
		const $field = $( acfFieldID );
		
		if ( ! $field.length ) {
			return;
		}
		
		$field.on( 'select2:unselecting', function( e ) {
			const answer = confirm( 'Please confirm removal' );
			
			if( ! answer ) {
				e.preventDefault();
			}
		} );
	}
	
	/**
	 * Disable ajax requests for disabled fields.
	 */
	function disableAjaxForDisabledFields() {
		const fieldIds = fields.map( function( f ) { return f[0]; } );
		
		acf.add_filter( 'select2_args', function( options, $select ) {
			const idSelector = '#' + $select.attr( 'id' );
			
			if ( fieldIds.includes( idSelector ) ) {
				delete options.ajax;
			}
			
			return options;
		} );
	}
	
	/**
	 * Document ready handler.
	 */
	$( document ).ready( function() {
		initMultiDisableMultiSelectField();
		disableAjaxForDisabledFields();
	} );
} )( jQuery, acf );
