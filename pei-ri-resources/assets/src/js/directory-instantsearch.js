jQuery(function () {
	if (jQuery('#directorysearch').length > 0) {
		var directory = instantsearch({
			indexName: ri_the_resource_directory.index_name,
			searchClient: algoliasearch(algolia.application_id, algolia.search_api_key)
		});

		directory.addWidgets([
			instantsearch.widgets.searchBox({
				container: '#directorysearch',
				placeholder: 'Search...',
				showReset: false,
				showSubmit: false,
				showLoadingIndicator: false,
			}),

			instantsearch.widgets.hits({
				container: '#directoryhits',
				hitsPerPage: 10,
				templates: {
					empty: 'No results matched your query.',
					item: wp.template('directory-hit'),
				},

				transformItems(items) {
					return items.filter(function (item) {
						let letter = directory.renderState[ri_the_resource_directory.index_name].alphabetical;
						if (item.post_title.toLowerCase().startsWith(letter)) {
							return item;
						} else {
							return item;
						}
					});
				}
			}),

			instantsearch.widgets.hitsPerPage({
				container: document.querySelector('#hits-per-page'),
				items: [
					{label: '10 articles per page', value: 10, default: true},
					{label: '50 articles per page', value: 50},
					{label: '100 articles per page', value: 100},
				],
			}),

			/* Pagination widget */
			instantsearch.widgets.pagination({
				container: document.querySelector('#directory-pagination'),
				showFirst: false,
				showLast: false,
				templates: {
					previous: '‹ Previous',
					next: 'Next ›',
				},
			}),

			instantsearch.widgets.infiniteHits({
				container: '#directory-infinite-hits',
				hitsPerPage: 10,
				templates: {
					empty: 'No results matched your query.',
					item: wp.template('directory-hit'),
					showMoreText: 'Load more'
				},
			}),


		]);

		directory.start();
	}

	jQuery("#theresource-directory .theresource-letter-filter li").click(function () {

		let element = jQuery(this);
		jQuery("#theresource-directory .theresource-letter-filter li").removeClass("selected");
		element.addClass("selected");

		directory.renderState[ri_the_resource_directory.index_name].alphabetical = element.html();

		directory.helper.state.query = element.html();
		directory.refresh();
	});
});
