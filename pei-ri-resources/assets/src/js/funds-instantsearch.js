jQuery(function () {
/*
**
** Strategy
**
*/
const renderRefinementListStrategy = (renderOptions, isFirstRender) => {
  const {
   items,
    isFromSearch,
    refine,
    createURL,
    isShowingMore,
    canToggleShowMore,
    searchForItems,
    toggleShowMore,
    widgetParams,
  } = renderOptions;

  if (isFirstRender) {
	const div = document.createElement('div');
	widgetParams.container.appendChild(div);
  }

  widgetParams.container.querySelector('div').innerHTML = items
    .map(
      item => `
          <button
            data-value="${item.value}"
            class="${item.isRefined ? 'active' : ''}"
          >
            ${item.label}
          </button>
      `
    )
    .join('');

  [...widgetParams.container.querySelectorAll('button')].forEach(element => {
    element.addEventListener('click', event => {
		event.preventDefault();
		console.log(items);
		console.log(items.length);
		if (items.length) {
			items.forEach(item => {
				if (item.isRefined && item.value != event.currentTarget.dataset.value) {
					refine(item.value);
				}
			});
			
		}
		
		refine(event.currentTarget.dataset.value);
    });
  });

};

const customRefinementListStrategy = instantsearch.connectors.connectRefinementList(
  renderRefinementListStrategy
);

/*
**
** Region
**
*/
const renderRefinementListRegion = (renderOptions, isFirstRender) => {
  const {
    items,
    refine,
    createURL,
    widgetParams,
  } = renderOptions;

  if (isFirstRender) {
	  const confirmButton = document.createElement('button');
	  const cancelButton = document.createElement('button');
	  const ul = document.createElement('ul');
	  const buttons = jQuery('#facet-funds-region-buttons');

	  confirmButton.textContent = 'Confirm';
	  confirmButton.setAttribute('id', 'facet-funds-region-confirm');
	  confirmButton.setAttribute('class', 'fs-button fs-ok');

	  cancelButton.textContent = 'Cancel';
	  cancelButton.setAttribute('id', 'facet-funds-region-cancel');
	  cancelButton.setAttribute('class', 'fs-button fs-cancel');
	  
		confirmButton.addEventListener('click', (event) => {
			event.preventDefault();
			var all = jQuery('.useThisRegion');

			all.each(function() { 
				refine(jQuery(this).attr('data-value'));
			});

			jQuery('#fs-region .fs-box').fadeOut();
		});
		
		cancelButton.addEventListener('click', (event) => {
			jQuery('#fs-region .fs-box').fadeOut();
			jQuery('#fs-region .useThisRegion').removeClass('useThisRegion active');
		});

		buttons.append(cancelButton);
		buttons.append(confirmButton);
		widgetParams.container.appendChild(ul);
  }

  widgetParams.container.querySelector('ul').innerHTML = items
    .map(
      item => `
        <li>
          <a
            href="${createURL(item.value)}"
            data-value="${item.value}"
            class="${item.isRefined ? 'active' : ''}"
          >
            ${item.label}
          </a>
        </li>
      `
    )
    .join('');

  [...widgetParams.container.querySelectorAll('a')].forEach(element => {
    element.addEventListener('click', event => {
		event.preventDefault();
		event.currentTarget.classList.add('useThisRegion', 'active');
    });
  });

};

const customRefinementListRegion = instantsearch.connectors.connectRefinementList(
  renderRefinementListRegion
);


/*
**
** Style
**
*/
const renderRefinementListStyle = (renderOptions, isFirstRender) => {
  const {
    items,
    refine,
    createURL,
    widgetParams,
  } = renderOptions;

  if (isFirstRender) {
	  const confirmButton = document.createElement('button');
	  const cancelButton = document.createElement('button');
	  const ul = document.createElement('ul');
	  const buttons = jQuery('#facet-funds-style-buttons');

	  confirmButton.textContent = 'Confirm';
	  confirmButton.setAttribute('id', 'facet-funds-style-confirm');
	  confirmButton.setAttribute('class', 'fs-button fs-ok');

	  cancelButton.textContent = 'Cancel';
	  cancelButton.setAttribute('id', 'facet-funds-style-cancel');
	  cancelButton.setAttribute('class', 'fs-button fs-cancel');
	  
		confirmButton.addEventListener('click', (event) => {
			event.preventDefault();
			var all = jQuery('.useThisStyle');

			all.each(function() { 
				refine(jQuery(this).attr('data-value'));
			});

			jQuery('#fs-style .fs-box').fadeOut();
		});
		
		cancelButton.addEventListener('click', (event) => {
			jQuery('#fs-style .fs-box').fadeOut();
			jQuery('#fs-style .useThisStyle').removeClass('useThisStyle active');
		});

		buttons.append(cancelButton);
		buttons.append(confirmButton);
		widgetParams.container.appendChild(ul);
  }

  widgetParams.container.querySelector('ul').innerHTML = items
    .map(
      item => `
        <li>
          <a
            href="${createURL(item.value)}"
            data-value="${item.value}"
            class="${item.isRefined ? 'active' : ''}"
          >
            ${item.label}
          </a>
        </li>
      `
    )
    .join('');

  [...widgetParams.container.querySelectorAll('a')].forEach(element => {
    element.addEventListener('click', event => {
		event.preventDefault();
		event.currentTarget.classList.add('useThisStyle', 'active');
    });
  });

};

const customRefinementListStyle = instantsearch.connectors.connectRefinementList(
  renderRefinementListStyle
);

/*
**
** Asset Class
**
*/
const renderRefinementListAssetclass = (renderOptions, isFirstRender) => {
  const {
    items,
    refine,
    createURL,
    widgetParams,
  } = renderOptions;

  if (isFirstRender) {
	  const confirmButton = document.createElement('button');
	  const cancelButton = document.createElement('button');
	  const ul = document.createElement('ul');
	  const buttons = jQuery('#facet-funds-assetclass-buttons');

	  confirmButton.textContent = 'Confirm';
	  confirmButton.setAttribute('id', 'facet-funds-assetclass-confirm');
	  confirmButton.setAttribute('class', 'fs-button fs-ok');

	  cancelButton.textContent = 'Cancel';
	  cancelButton.setAttribute('id', 'facet-funds-assetclass-cancel');
	  cancelButton.setAttribute('class', 'fs-button fs-cancel');
	  
		confirmButton.addEventListener('click', (event) => {
			event.preventDefault();
			var all = jQuery('.useThisAssetClass');

			all.each(function() { 
				refine(jQuery(this).attr('data-value'));
			});

			jQuery('#fs-assetclass .fs-box').fadeOut();
		});
		
		cancelButton.addEventListener('click', (event) => {
			jQuery('#fs-assetclass .fs-box').fadeOut();
			jQuery('#fs-assetclass .useThisAssetClass').removeClass('useThisAssetClass active');
		});

		buttons.append(cancelButton);
		buttons.append(confirmButton);
		widgetParams.container.appendChild(ul);
  }

  widgetParams.container.querySelector('ul').innerHTML = items
    .map(
      item => `
        <li>
          <a
            href="${createURL(item.value)}"
            data-value="${item.value}"
            class="${item.isRefined ? 'active' : ''}"
          >
            ${item.label}
          </a>
        </li>
      `
    )
    .join('');

  [...widgetParams.container.querySelectorAll('a')].forEach(element => {
    element.addEventListener('click', event => {
		event.preventDefault();
		event.currentTarget.classList.add('useThisAssetClass', 'active');
    });
  });

};

const customRefinementListAssetclass = instantsearch.connectors.connectRefinementList(
  renderRefinementListAssetclass
);


/*
**
** Asset Sub Class
**
*/
	
const renderRefinementListAssetsubclass = (renderOptions, isFirstRender) => {
  const {
    items,
    refine,
    createURL,
    widgetParams,
  } = renderOptions;

  if (isFirstRender) {
	  const confirmButton = document.createElement('button');
	  const cancelButton = document.createElement('button');
	  const ul = document.createElement('ul');
	  const buttons = jQuery('#facet-funds-assetsubclass-buttons');

	  confirmButton.textContent = 'Confirm';
	  confirmButton.setAttribute('id', 'facet-funds-assetsubclass-confirm');
	  confirmButton.setAttribute('class', 'fs-button fs-ok');

	  cancelButton.textContent = 'Cancel';
	  cancelButton.setAttribute('id', 'facet-funds-assetsubclass-cancel');
	  cancelButton.setAttribute('class', 'fs-button fs-cancel');
	  
		confirmButton.addEventListener('click', (event) => {
			event.preventDefault();
			var all = jQuery('.useThisAssetSubClass');

			all.each(function() { 
				refine(jQuery(this).attr('data-value'));
			});

			jQuery('#fs-assetsubclass .fs-box').fadeOut();
		});
		
		cancelButton.addEventListener('click', (event) => {
			jQuery('#fs-assetsubclass .fs-box').fadeOut();
			jQuery('#fs-assetsubclass .useThisAssetSubClass').removeClass('useThisAssetSubClass active');
		});

		buttons.append(cancelButton);
		buttons.append(confirmButton);
		widgetParams.container.appendChild(ul);
  }

  widgetParams.container.querySelector('ul').innerHTML = items
    .map(
      item => `
        <li>
          <a
            href="${createURL(item.value)}"
            data-value="${item.value}"
            class="${item.isRefined ? 'active' : ''}"
          >
            ${item.label} (${item.count})
          </a>
        </li>
      `
    )
    .join('');

  [...widgetParams.container.querySelectorAll('a')].forEach(element => {
    element.addEventListener('click', event => {
		event.preventDefault();
		event.currentTarget.classList.add('useThisAssetSubClass', 'active');
    });
  });

};

const customRefinementListAssetsubclass = instantsearch.connectors.connectRefinementList(
  renderRefinementListAssetsubclass
);
	
	
	
	
	
	if (jQuery('#funds-search').length > 0) {

		var funds = instantsearch({
			indexName: ri_the_resource_funds.index_name,
			searchClient: algoliasearch(algolia.application_id, algolia.search_api_key)
		});

		funds.addWidgets([
			instantsearch.widgets.configure({
				filters: 'taxonomies.content-type:"Fund"',
			}),
			instantsearch.widgets.searchBox({
				container: '#funds-search',
				placeholder: 'Search...',
				showReset: false,
				showSubmit: false,
				showLoadingIndicator: false,
			}),
			instantsearch.widgets.currentRefinements({
				container: '#funds-current-refinements',
			}),

			instantsearch.widgets.clearRefinements({
				container: '#funds-clear-refi',
				templates: {
					resetLabel() {
						return 'Clear all filters';
					},
				},
			}),

			instantsearch.widgets.clearRefinements({
				container: '#funds-clear',
				templates: {
					resetLabel() {
						return 'Clear all filters';
					},
				},
			}),

			instantsearch.widgets.clearRefinements({
				container: '#facet-funds-strategy-clear',
				includedAttributes: ['strategy'],
				templates: {
					resetLabel() {
						return 'All';
					},
				},
				cssClasses: {
					root: 'fr-radio fr-clear',
					button: [
					'fr-clear-button fr-strategy-clear-button',
					],
				},
			}),

			instantsearch.widgets.clearRefinements({
				container: '#facet-funds-region-buttons',
				includedAttributes: ['region'],
				templates: {
					resetLabel() {
						return 'Clear filters';
					},
				},
				cssClasses: {
					root: 'fs-button fs-clear',
					button: [
					'fs-clear-button',
					],
				},
			}),

			instantsearch.widgets.clearRefinements({
				container: '#facet-funds-style-buttons',
				includedAttributes: ['style'],
				templates: {
					resetLabel() {
						return 'Clear filters';
					},
				},
				cssClasses: {
					root: 'fs-button fs-clear',
					button: [
					'fs-clear-button',
					],
				},
			}),

			instantsearch.widgets.clearRefinements({
				container: '#facet-funds-assetclass-buttons',
				includedAttributes: ['asset_class'],
				templates: {
					resetLabel() {
						return 'Clear filters';
					},
				},
				cssClasses: {
					root: 'fs-button fs-clear',
					button: [
					'fs-clear-button',
					],
				},
			}),

			instantsearch.widgets.clearRefinements({
				container: '#facet-funds-assetsubclass-buttons',
				includedAttributes: ['asset_subclass'],
				templates: {
					resetLabel() {
						return 'Clear filters';
					},
				},
				cssClasses: {
					root: 'fs-button fs-clear',
					button: [
					'fs-clear-button',
					],
				},
			}),
			

			instantsearch.widgets.hits({
				container: '#funds-hits',
				hitsPerPage: 10,
				templates: {
					empty: 'No results matched your query.',
					item: wp.template('funds-hit'),
				},

				transformItems(items) {
					return items.filter(function (item) {
						let letter = funds.renderState[ri_the_resource_funds.index_name].alphabetical;
						if (item.post_title.toLowerCase().startsWith(letter)) {
							return item;
						} else {
							return item;
						}
					});
				}
			}),

			customRefinementListStrategy({
				container: document.querySelector('#facet-funds-strategy'),
				attribute: 'strategy',
				operator: 'or'
			}),

			customRefinementListRegion({
				container: document.querySelector('#facet-funds-region'),
				attribute: 'region',
				showMoreLimit: 20,
			}),

			customRefinementListStyle({
				container: document.querySelector('#facet-funds-style'),
				attribute: 'style',
				showMoreLimit: 20,
			}),

			customRefinementListAssetclass({
				container: document.querySelector('#facet-funds-assetclass'),
				attribute: 'asset_class',
				showMoreLimit: 20,
			}),

			customRefinementListAssetsubclass({
				container: document.querySelector('#facet-funds-assetsubclass'),
				attribute: 'asset_subclass',
				showMoreLimit: 20,
			}),

			instantsearch.widgets.pagination({
				container: document.querySelector('#funds-pagination'),
				showFirst: false,
				showLast: false,
				templates: {
					previous: '‹ Previous',
					next: 'Next ›',
				},
			}),

		

			
		]);

		funds.start();

		console.log(funds);
	}

	var fakeSelects = jQuery('.fake-select');

	if (fakeSelects.length) {
		fakeSelects.each(function () {
			var sel = $(this).find('.fs-select'),
				box = $(this).find('.fs-box');


			sel.on('click', function () {
				box.fadeIn();
			});



		});
	}



	var fundsObserverTarget = document.querySelector('#funds-current-refinements'),
		fundsRefiOptions = jQuery('#funds-current-refinements-options'),
		fundsClear = jQuery('#funds-clear'),
		fundsShowAllCurrent = jQuery('#funds-show-all-current');

	var fundsObserver = new MutationObserver(function(mutations) {
		mutations.forEach(function (mutation) {

			if ('childList' == mutation.type) {
				var allCurrentRefi = jQuery('.ais-CurrentRefinements-category');

				if (allCurrentRefi.length) {
					fundsRefiOptions.removeClass('hide');
					fundsClear.addClass('hide');
				} else {
					fundsRefiOptions.addClass('hide');
					fundsClear.removeClass('hide');
				}
				 
				if (allCurrentRefi.length > 4) {
					fundsShowAllCurrent.removeClass('hide').text('Show all ' + allCurrentRefi.length + ' filters');

					allCurrentRefi.each(function ( index ) { 
						if (index > 3) {
							jQuery(this).addClass('hide');
						} else {
							jQuery(this).removeClass('hide');
						}
					});
				} else {
					fundsShowAllCurrent.addClass('hide');

					allCurrentRefi.each(function ( index ) { 
						jQuery(this).removeClass('hide');
					});
				}
			}
		});
	});

	var fundsObserverConfig = { childList: true, subtree: true };
	fundsObserver.observe(fundsObserverTarget, fundsObserverConfig);

	fundsShowAllCurrent.on('click', function () { 
		var allCurrentRefi = jQuery('.ais-CurrentRefinements-category');

		if (jQuery(this).attr('data-open') == 'no') {
			fundsShowAllCurrent.text('Hide filters');
			jQuery('.ais-CurrentRefinements-category').removeClass('hide');
			jQuery(this).attr('data-open', 'yes');
		} else {
			fundsShowAllCurrent.removeClass('hide').text('Show all ' + allCurrentRefi.length + ' filters');
			jQuery('.ais-CurrentRefinements-category').each(function (index) {
				if (index > 3) {
					jQuery(this).addClass('hide');
				} else {
					jQuery(this).removeClass('hide');
				}
			});
			jQuery(this).attr('data-open', 'no');
		}
	});



});
