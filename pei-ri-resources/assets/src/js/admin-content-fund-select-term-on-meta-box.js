( function( $ ) {

    function handleIsProductFundChange(){
        var isProduct = $('#acf-field_634d63f97a229').is(':checked');
        
        modifyContentTypeMetabox( isProduct );
    }

    function modifyContentTypeMetabox( isProduct ) {
        if( isProduct === true ){
            $('#content-type-all input[value="' + taxonomy_data.fund_id + '"]').prop('checked', true);
            $('#content-type-all').css('pointer-events', 'none');
            $('#content-type-all').css('background-color', '#eee');
        } else {
            $('#content-type-all input[value="' + taxonomy_data.fund_id + '"]').prop('checked', false);
            $('#content-type-all').css('pointer-events', 'all');
            $('#content-type-all').css('background-color', '#fff');
            $('#content-type-' + taxonomy_data.fund_id).css('pointer-events', 'none');
        }
    }

	/**
	 * Document ready handler.
	 */
	$( document ).ready( function() {
		handleIsProductFundChange();
        $('#acf-field_634d63f97a229').on('change', function(){
            handleIsProductFundChange();
        });
	} );
} )( jQuery );
