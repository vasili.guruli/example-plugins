(function ($) {
    $('.toplevel_page_pei_ri_resources').attr('href', '#');
    $('li.toplevel_page_pei_ri_resources ul.wp-submenu li.wp-first-item a ').attr('href', '#');
    $('.ri-resources-admin-menu').on('click', function () {
        return false;
    });

    let ri_contact_addresses = $('select.ri-contact-addresses');
    ri_contact_addresses.on('change', function () {

        let ri_address = $(this).val();

        if ('select_address' !== ri_address) {
            let data = $.parseJSON(ri_address);
            let ri_address_line_1 = $('#acf-field_63a5b256ada8b-field_63a5b53953b73');
            let ri_address_line_2 = $('#acf-field_63a5b256ada8b-field_63a5b54453b74');
            let ri_address_line_3 = $('#acf-field_63a5b256ada8b-field_63a5b54753b75');
            let ri_city = $('#acf-field_63a5b256ada8b-field_63a5b54953b76');
            let ri_region = $('#acf-field_63a5b256ada8b-field_63a5b55053b77');
            let ri_country = $('#acf-field_63a5b256ada8b-field_63a5b55353b78');
            let ri_postal_code = $('#acf-field_63a5b256ada8b-field_63a5b55653b79');

            ri_set_value(data.address_line_1, ri_address_line_1);
            ri_set_value(data.address_line_2, ri_address_line_2);
            ri_set_value(data.address_line_3, ri_address_line_3);
            ri_set_value(data.city, ri_city);
            ri_set_value(data.region, ri_region);
            set_option_for_select(ri_country, data.country.value);
            ri_set_value(data.postal_code, ri_postal_code);
        }
    });

    /**
     * Check address fields
     *
     */
    function ri_set_value(value, field) {
        if ('undefined' != typeof value) {
            field.val(value);
        }
    }

    function set_option_for_select(select, value) {
        if ('undefined' != typeof value) {
            if (select.find("option[value='" + value + "']").length) {
                select.val(value).trigger('change');
            }
        }
    }

})(jQuery);