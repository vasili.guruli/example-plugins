jQuery(function () {

    if (jQuery('#content-search').length > 0) {
        var content = instantsearch({
            indexName: ri_content.index_name,
            searchClient: algoliasearch(algolia.application_id, algolia.search_api_key)
        });

        content.addWidgets([
            instantsearch.widgets.searchBox({
                container: '#content-search',
                placeholder: 'Search...',
                showReset: false,
                showSubmit: false,
                showLoadingIndicator: false,
            }),

            instantsearch.widgets.hits({
                container: '#content-hits',
                hitsPerPage: 10,
                templates: {
                    empty: 'No results matched your query.',
                    item: wp.template('content-hit'),
                },

                transformItems(items) {
                    return items.filter(function (item) {
                        let letter = content.renderState[ri_content.index_name].alphabetical;
                        if (item.post_title.toLowerCase().startsWith(letter)) {
                            return item;
                        } else {
                            return item;
                        }
                    });
                }
            }),

            instantsearch.widgets.pagination({
                container: document.querySelector('#content-pagination'),
                showFirst: false,
                showLast: false,
                templates: {
                    previous: '‹ Previous',
                    next: 'Next ›',
                },
            }),

        ]);

        content.start();
    }

});
