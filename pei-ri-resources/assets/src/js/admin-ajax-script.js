( function( $ ){

    let ri_contact_institution = $('#acf-field_637f6ce128a9e');
    const { __, _x, _n, _nx } = wp.i18n;

    ri_contact_institution.on( 'change', function () {
        let institution_id = $( this ).val();
        $.ajax({
            type : 'post',
            dataType : 'json',
            url : ri_resource.ajax_url,
            data : {
                action: 'ri_ajax_addresses_action',
                institution_id : institution_id,
                ri_nonce: ri_resource.nonce
            },
            beforeSend: function () {
            },
            success: function(response) {
                let ri_addresses       = $('.ri-contact-addresses');
                let institution_title  = $('.institution-title');
                institution_title.html( '<a href="' + response.institution_url + '">' + response.institution + '</a>' );
                if ( null !== response.primary_address ) {
                    let first_child     = $(".ri-contact-addresses option:first-child");
                    let primary_address = JSON.stringify( response.primary_address );
                    ri_addresses.empty().append( first_child );
                    ri_addresses.append( "<option value='" + primary_address + "'>"+ __('Primary Address') +"</option>" );

                    if ( null !== response.additional_address ) {
                        let i = 1;
                        $.each( response.additional_address, function ( key, value ) {
                            let additional_address = JSON.stringify( value );
                            ri_addresses.append( "<option value='" + additional_address + "'>" + i +
                                " " +__( 'Address - ' ) + value.country.label +"</option>" );
                            i++;
                        } );
                    }
                }
            }
        });
    } )

} )( jQuery );
