// Define variables
var rtabButtons = document.querySelectorAll(".js-rtabs li");
var rtabContents = document.getElementsByClassName("js-rtabs-content");

function rTabsActivateTab(e) {
  e.preventDefault();
  
  // Hide all tabs
  rtabButtons.forEach(function(button, index){
    button.classList.remove("active");
  });
  [].forEach.call(rtabContents, function(content, index){
    content.classList.remove("active");
  });
  
  // Make current tab active
  e.target.parentNode.classList.add("active");
  var activeTab = e.target.getAttribute("data-href");
  document.querySelector(activeTab).classList.add("active");
}

rtabButtons.forEach(function(button, index){
  button.addEventListener("click", rTabsActivateTab);
});