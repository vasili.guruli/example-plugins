<?php

namespace Pei_Ri_Resources\Tests;

use Pei_Ri_Resources\Frontend;
use PHPUnit\Framework\TestCase;
use Brain\Monkey\Functions;

class FrontendTest extends TestCase {
	public function test_init_method() {

		// mock expections
		$expected_prior = 10;

		// Act
		$sut = new \Pei_Ri_Resources\Frontend();
		$sut->init();

		$result = has_filter( 'the_content', array( $sut, 'automatic_optin' ) );

		// Assertions
		$this->assertSame( $expected_prior, $result );
	}

	public function test_if_automatic_optin_returns_content() {

		// mock expections
		$expected_output = 'the content';

        Functions\stubs(
            [
                'is_admin'    => false,
                'get_queried_object' => function () {     
                      $object = new \stdClass();
                      $object->post_name = 'post';
                      return $object;
                },
                'get_post_type' => 'post',   
                'is_page_template' => false                         
            ]
        );

		// Act
		$sut    = new \Pei_Ri_Resources\Frontend();
		$result = $sut->automatic_optin( $expected_output );

		// Assertions
		$this->assertSame( $expected_output, $result );
	}

}
