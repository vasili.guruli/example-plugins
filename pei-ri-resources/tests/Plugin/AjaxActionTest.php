<?php

class AjaxActionTest extends \Pei_Ri_Resources\TestCase {
	public function test_init_hooks_ajax_action() {
		
		$ajax_action = new \Pei_Ri_Resources\Ajax();
		$ajax_action->init();
		
		
		$this->assertIsInt( has_action( 'wp_ajax_ri_ajax_addresses_action', [ $ajax_action, 'ri_ajax_addresses_action' ] ),
			'Ajax should be hooked into wp_ajax_{action} hook' );
		
	}
	
	public function test_init_hook_doesnt_have_proper_actions_ajax_action() {
		
		$ajax_action = new \Pei_Ri_Resources\Ajax();
		$ajax_action->init();
		
		$this->assertFalse( has_action( 'wp_footer', [ $ajax_action, 'ri_ajax_addresses_action' ] ),
			'Ajax doesnt have wp_footer hook' );
	}
}