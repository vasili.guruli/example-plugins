<?php
use Brain\Monkey\Functions;

class ConentReletedContentIdTest extends \Pei_Ri_Resources\TestCase {
	
	public function test_if_ri_add_post_id_to_related_content_id_returning_post_id_as_value_if_post_is__not_an_object(){
		
		global $post;
		$post = Mockery::mock( '\WP_Post' );
		$post = (array)[ 'ID' => '123123' ];
		
		// mock vars
		$post = '';
		$field = array(
			'value' => 'some_value'
		);
		
		// sut
		$institutions = new \Pei_Ri_Resources\Content();
		
		// Act
		$result = $institutions->ri_related_content_add_post_id_to_acf_field_and_mark_this_field_as_read_only( $field );
		
		// Assertions
		$this->assertSame( $field, $result );
	}
	
	public function test_if_ri_add_post_id_to_related_content_id_returning_post_id_as_value_if_post_is_an_object(){
		
		// mock vars
		global $post;
		$post = Mockery::mock( '\WP_Post' );
		$post = (object)[ 'ID' => '123123' ];
		$field = array(
			'value' => 'some_value'
		);
		
		$expected_return = array(
			'value' => '123123',
			'readonly' => true
		);
		
		// sut
		$sut = new \Pei_Ri_Resources\Content();
		
		// Act
		$result = $sut->ri_related_content_add_post_id_to_acf_field_and_mark_this_field_as_read_only( $field );
		
		// Assertions
		$this->assertSame( $expected_return, $result );
	}
}
