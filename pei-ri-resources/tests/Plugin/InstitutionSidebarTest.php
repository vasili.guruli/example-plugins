<?php

use Brain\Monkey\Functions;

class InstitutionSidebarTest extends \Pei_Ri_Resources\TestCase {

	public function test_if_contacts_are_not_rendered_because_no_contacts_supplied() {
		
		// Mock vars
		$contacts = null;
		
		// Act
		ob_start();
		require 'templates/public/parts/institution-sidebar-contacts.php';
		$output = trim( ob_get_contents() );
		ob_end_clean();
		
		// Verify
		$this->assertEmpty( $output );
		
	}
	
	public function test_if_contacts_are_rendered() {
		
		// Mock vars
		$contact_id = 876543;
		$contacts = [ $contact_id ];
		$address = [
			'address_line_1' => 'Line 1',
			'address_line_2' => 'Line 2',
			'address_line_3' => 'Line 3',
			'city' => 'City',
			'region' => 'Region',
			'country' => [ 'label' => 'Country' ],
			'postal_code' => '11-222',
		];
		$expected_html1 = '<div class="theresource-contacts">';
		$expected_html2 = '<div class="row name">First Last</div>';
		$expected_html3 = '<div class="row job-title">Job Title</div>';
		$expected_html4 = '<span>City, Country, Region</span>';
		$expected_html5 = '<span>email@test.com</span>';
		$expected_html6 = '<div class="row address-lines">Line 1, Line 2, Line 3, 11-222</div>';
		
		// Mock functions
		Functions\expect( 'get_the_title' )->once()->with( $contact_id )->andReturn( 'First Last' );
		Functions\expect( 'get_field' )->once()->with( 'job_title', $contact_id )->andReturn( 'Job Title' );
		Functions\expect( 'get_field' )->once()->with( 'email', $contact_id )->andReturn( 'email@test.com' );
		Functions\expect( 'get_field' )->once()->with( 'address', $contact_id )->andReturn( $address );
		Functions\when( 'esc_html' )->returnArg();
		Functions\when( 'esc_url' )->returnArg();
		
		// Act
		ob_start();
		require 'templates/public/parts/institution-sidebar-contacts.php';
		$output = trim( ob_get_contents() );
		ob_end_clean();
		
		// Verify
		$this->assertNotEmpty( $output );
		$this->assertStringContainsString( $expected_html1, $output );
		$this->assertStringContainsString( $expected_html2, $output );
		$this->assertStringContainsString( $expected_html3, $output );
		$this->assertStringContainsString( $expected_html4, $output );
		$this->assertStringContainsString( $expected_html5, $output );
		$this->assertStringContainsString( $expected_html6, $output );
		
	}
	
	public function test_if_aum_info_is_not_rendered_because_no_aum_amount_found() {
		
		// Mock vars
		$institution_id = 123456;
		
		// Mock functions
		Functions\expect( 'get_the_ID' )->once()->andReturn( $institution_id );
		Functions\expect( 'get_field' )->once()->with( 'aum_amount', $institution_id )->andReturn( null );
		Functions\expect( 'get_field' )->once()->with( 'aum_year', $institution_id )->andReturn( null );
		
		// Act
		ob_start();
		require 'templates/public/parts/institution-sidebar-aum.php';
		$output = trim( ob_get_contents() );
		ob_end_clean();
		
		// Verify
		$this->assertEmpty( $output );
		
	}
	
	public function test_if_aum_info_is_rendered_in_million() {
		
		// Mock vars
		$institution_id = 123456;
		$aum_amount = '0.123';
		$aum_year = '12/02/2023';
		$expected_html1 = '<div class="theresource-aum">';
		$expected_html2 = '<span class="aum-label">AUM</span>';
		$expected_html3 = '<span>USD&nbsp;123&nbsp;million</span>';
		$expected_html4 = '<span>@&nbsp;12&nbsp;Feb&nbsp;2023</span>';
		
		// Mock functions
		Functions\expect( 'get_the_ID' )->once()->andReturn( $institution_id );
		Functions\expect( 'get_field' )->once()->with( 'aum_amount', $institution_id )->andReturn( $aum_amount );
		Functions\expect( 'get_field' )->once()->with( 'aum_year', $institution_id )->andReturn( $aum_year );
		Functions\when( 'esc_html' )->returnArg();
		
		// Act
		ob_start();
		require 'templates/public/parts/institution-sidebar-aum.php';
		$output = trim( ob_get_contents() );
		ob_end_clean();
		
		// Verify
		$this->assertNotEmpty( $output );
		$this->assertStringContainsString( $expected_html1, $output );
		$this->assertStringContainsString( $expected_html2, $output );
		$this->assertStringContainsString( $expected_html3, $output );
		$this->assertStringContainsString( $expected_html4, $output );
		
	}
	
	public function test_if_aum_info_is_rendered_in_billion() {
		
		// Mock vars
		$institution_id = 123456;
		$aum_amount = '15';
		$aum_year = '30/09/2022';
		$expected_html1 = '<div class="theresource-aum">';
		$expected_html2 = '<span class="aum-label">AUM</span>';
		$expected_html3 = '<span>USD&nbsp;15&nbsp;billion</span>';
		$expected_html4 = '<span>@&nbsp;30&nbsp;Sep&nbsp;2022</span>';
		
		// Mock functions
		Functions\expect( 'get_the_ID' )->once()->andReturn( $institution_id );
		Functions\expect( 'get_field' )->once()->with( 'aum_amount', $institution_id )->andReturn( $aum_amount );
		Functions\expect( 'get_field' )->once()->with( 'aum_year', $institution_id )->andReturn( $aum_year );
		Functions\when( 'esc_html' )->returnArg();
		
		// Act
		ob_start();
		require 'templates/public/parts/institution-sidebar-aum.php';
		$output = trim( ob_get_contents() );
		ob_end_clean();
		
		// Verify
		$this->assertNotEmpty( $output );
		$this->assertStringContainsString( $expected_html1, $output );
		$this->assertStringContainsString( $expected_html2, $output );
		$this->assertStringContainsString( $expected_html3, $output );
		$this->assertStringContainsString( $expected_html4, $output );
		
	}
		
}
