<?php
use Brain\Monkey\Functions;

class InstitutionsTemplateTest extends \Pei_Ri_Resources\TestCase {
	public function test_content_template_has_init_hook_filter() {
		$institutions_template = new \Pei_Ri_Resources\Institutions_Template();
		$institutions_template->init();


		$this->assertIsInt( has_filter( 'single_template', [ $institutions_template, 'load_appropriate_template' ] ),
			'Single Template should be hooked into single_template filter' );
	}

	public function test_content_template_has_not_init_hook_filter() {
		$institutions_template = new \Pei_Ri_Resources\Institutions_Template();
		$institutions_template->init();


		$this->assertFalse( has_filter( 'the_content', [ $institutions_template, 'load_appropriate_template' ] ),
			'The Content filter should be hooked into Institutions Template' );
	}
}