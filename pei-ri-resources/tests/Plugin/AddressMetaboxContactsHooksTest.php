<?php

class AddressMetaboxContactsHooksTest extends \Pei_Ri_Resources\TestCase {
	
	public function test_init_hooks_contact_address_metabox() {
		
		
		$address_metabox = new \Pei_Ri_Resources\Addresses_Metabox_For_Contacts();
		$address_metabox->init();
		
		$this->assertTrue( has_action( 'add_meta_boxes', $address_metabox->init() ),
			'Add Meta Box should be hooked into init hook' );
		
		$this->assertTrue( has_action( 'save_post', $address_metabox->init() ),
			'Save Post should be hooked into init hook' );
	}
	
	public function test_init_hook_doesnt_have_proper_actions_address_metabox() {
		$address_metabox = new \Pei_Ri_Resources\Addresses_Metabox_For_Contacts();
		$address_metabox->init();
		
		$this->assertFalse( has_action( 'wp_head', $address_metabox->init() ),
			'WP Head should not be hooked into init hook' );
		
		$this->assertFalse( has_action( 'init', $address_metabox->init() ),
			'INit should not be hooked into init hook' );
		
	}
}
