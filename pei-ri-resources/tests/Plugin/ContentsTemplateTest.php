<?php
use Brain\Monkey\Functions;

class ContentsTemplateTest extends \Pei_Ri_Resources\TestCase {
	public function test_content_template_has_init_hook_filter() {
		$contents_template = new \Pei_Ri_Resources\Contents_Template();
		$contents_template->init();


		$this->assertIsInt( has_filter( 'single_template', [ $contents_template, 'load_appropriate_template' ] ),
			'Single Template should be hooked into single_template filter' );
	}

	public function test_content_template_has_not_init_hook_filter() {
		$contents_template = new \Pei_Ri_Resources\Contents_Template();
		$contents_template->init();


		$this->assertFalse( has_filter( 'the_content', [ $contents_template, 'load_appropriate_template' ] ),
			'The Content filter should be hooked into Contents Template' );
	}
}