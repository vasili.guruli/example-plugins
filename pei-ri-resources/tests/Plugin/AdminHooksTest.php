<?php
use Brain\Monkey\Functions;

class AdminHooksTest extends \Pei_Ri_Resources\TestCase {
	public function test_init_hooks_admin() {

		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$admin = new \Pei_Ri_Resources\Admin();
		$admin->init();
		

		$this->assertIsInt( has_action( 'admin_menu', [ $admin, 'register_menu_page' ] ),
			'Admin menu should be hooked into admin_menu hook' );

		$this->assertIsInt( has_action( 'admin_menu', [ $admin, 'add_menu_items_for_custom_post_types' ] ),
			'Admin menu should be hooked into admin_menu hook' );

		$this->assertIsInt( has_action( 'admin_init', [ $admin, 'register_settings' ] ),
			'Register settings should be hooked into admin_init hook' );
	}

	public function test_init_hook_doesnt_have_proper_actions_admin() {
		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$admin = new \Pei_Ri_Resources\Admin();
		$admin->init();

		$this->assertFalse( has_action( 'wp_header', [ $admin, 'register_menu_page' ] ),
			'Admin menu doesnt have wp_header hook' );

		$this->assertFalse( has_action( 'wp_footer', [ $admin, 'add_menu_items_for_custom_post_types' ] ),
			'Register settings doesnt have wp_footer hook' );

		$this->assertFalse( has_action( 'wp_footer', [ $admin, 'register_settings' ] ),
			'Register settings doesnt have wp_footer hook' );
	}
}