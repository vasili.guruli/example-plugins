<?php
use Brain\Monkey\Functions;

class ACFConfigTest extends \Pei_Ri_Resources\TestCase {
	public function test_init_method() {

		// mock expections
		$expected_prior = 10;

		// Act
		$sut = new \Pei_Ri_Resources\ACFConfig();
		$sut->init();

		$result = has_filter( 'acf/settings/save_json', array( $sut, 'pei_ri_change_acf_json_path' ) );
		$result1 = has_filter( 'acf/settings/load_json', array( $sut, 'pei_acf_json_load_point' ) );
		$result2 = has_filter( 'acf/prepare_field/key=field_634d6b77b4fa0', array( $sut, 'mark_select_field_as_disabled_but_not_disabled' ) );

		// Assertions
		$this->assertSame( $expected_prior, $result );
		$this->assertSame( $expected_prior, $result1 );
		$this->assertSame( $expected_prior, $result2 );
	}

	public function test_if_pei_ri_change_acj_json_path_adding_acf_json() {

		// mock expections
		$fake_path = RI_RESOURCES_PLUGIN_DIR_PATH;
		$expected_result = RI_RESOURCES_PLUGIN_DIR_PATH .'/acf-json';
		// Act
		$sut = new \Pei_Ri_Resources\ACFConfig();
		$result = $sut->pei_ri_change_acf_json_path( $fake_path );

		// Assertions
		$this->assertSame( $expected_result, $result );
	}

	public function test_if_pei_acf_json_load_point_has_our_custom_path(){

		// mock expections
		$fake_paths = array(RI_RESOURCES_PLUGIN_DIR_PATH);
		$expected_result = array( 1 => RI_RESOURCES_PLUGIN_DIR_PATH .'/acf-json');
		// Act
		$sut = new \Pei_Ri_Resources\ACFConfig();
		$result = $sut->pei_acf_json_load_point( $fake_paths );

		// Assertions
		$this->assertSame( $expected_result, $result );
	}

	// check if mark_select_field_as_disabled_but_not_disabled set readonly and disabled values
	public function test_if_mark_select_field_as_disabled_but_not_disabled_set_readonly_and_disabled(){
		// Mock vars
		$field = array();

		// Expected result
		$expected_result = array(
			'readonly' => 1,
			'disabled' => 0,
		);

		// Act
		$sut = new \Pei_Ri_Resources\ACFConfig();
		$result = $sut->mark_select_field_as_disabled_but_not_disabled( $field );

		// Assertions
		$this->assertSame( $expected_result, $result );

	}
}
