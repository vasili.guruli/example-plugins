<?php

class AjaxAddressActionTest extends \Pei_Ri_Resources\TestCase {
	public function test_ri_ajax_addresses_action() {
		$addresses = array(
			'institution'        => 'Test Institution',
			'institution_url'    => 'https://institution-fake-url/?action=edit&insitution=12345',
			'primary_address'    => array(
				'address_line_1' => 'Test Address 1',
				'address_line_2' => 'Test Address 2',
				'address_line_3' => 'Test Address 3',
				'city'           => 'Test City 2',
				'region'         => 'Test Region 2',
				'country'        => 'AG',
				'postal_code'    => '1234578',
			),
			'additional_address' => array(
			)
		);
		
		sort( $addresses['additional_address'] );
		
		$expected = json_encode( $addresses );
		
		$ajax_action = new \Pei_Ri_Resources\Ajax();
		if ( ! isset( $_POST['ri_nonce'] ) ) {
			$_POST['ri_nonce'] = 'absd45e37';
		}
		
		if ( ! isset( $_POST['institution_id'] ) ) {
			$_POST['institution_id'] = 12345;
		}
		
		Brain\Monkey\Functions\when( 'get_field' )->justReturn(
			array(
				'address_line_1' => 'Test Address 1',
				'address_line_2' => 'Test Address 2',
				'address_line_3' => 'Test Address 3',
				'city'           => 'Test City 2',
				'region'         => 'Test Region 2',
				'country'        => 'AG',
				'postal_code'    => '1234578',
			)
		);
		
		Brain\Monkey\Functions\when( 'get_the_title' )->justReturn(
			'Test Institution'
		);
		
		Brain\Monkey\Functions\when( 'wp_verify_nonce' )->justReturn(
			$_POST['ri_nonce']
		);
		
		Brain\Monkey\Functions\when( 'sanitize_text_field' )->justReturn(
			$_POST['ri_nonce']
		);
		
		Brain\Monkey\Functions\when( 'esc_url' )->justReturn(
			'https://institution-fake-url/?action=edit&insitution=12345'
		);
		
		Brain\Monkey\Functions\when( 'get_edit_post_link' )->justReturn(
			'https://institution-fake-url/?action=edit&insitution=12345'
		);
		
		Brain\Monkey\Functions\when( 'wp_die' )->justReturn(
			$expected
		);
		
		$this->expectOutputString( $expected );
		
		$ajax_action->ri_ajax_addresses_action();
		
	}
}