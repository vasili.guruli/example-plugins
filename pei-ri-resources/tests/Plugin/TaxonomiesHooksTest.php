<?php
use Brain\Monkey\Functions;

class TaxonomiesHooksTest extends \Pei_Ri_Resources\TestCase {
	public function test_init_hooks_taxonomies() {

		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$taxonomies = new \Pei_Ri_Resources\Taxonomies();

		$this->assertTrue( has_action( 'init', $taxonomies->init() ),
			'Init should be hooked into init hook' );
	}

	public function test_init_hook_doesnt_have_proper_actions_taxonomies() {
		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$taxonomies = new \Pei_Ri_Resources\Taxonomies();

		$this->assertFalse( has_action( 'wp_header', $taxonomies->init() ),
			'Post type does not have wp_header hook' );

	}
}