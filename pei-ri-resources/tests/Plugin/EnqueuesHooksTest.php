<?php
use Brain\Monkey\Functions;

class EnqueuesHooksTest extends \Pei_Ri_Resources\TestCase {
	public function test_init_hooks_enqueues() {
		$enqueues =  new \Pei_Ri_Resources\Enqueues();
		$enqueues->init();

		$this->assertIsInt( has_action( 'admin_enqueue_scripts', [ $enqueues, 'register_ri_resources_admin_script' ] ),
			'Admin scripts should be hooked into admin_enqueue_scripts hook' );

		$this->assertIsInt( has_action( 'wp_enqueue_scripts', [ $enqueues, 'register_ri_resources_frontend_css' ] ),
			'Frontend scripts should be hooked into wp_enqueue_scripts hook' );

		$this->assertIsInt( has_action( 'admin_enqueue_scripts', [ $enqueues, 'register_ri_resources_admin_css' ] ),
			'Frontend scripts should be hooked into wp_enqueue_scripts hook' );

		$this->assertIsInt( has_action( 'admin_enqueue_scripts', [ $enqueues, 'register_ri_resources_required_post_title' ] ),
			'Admin scripts should be hooked into admin_enqueue_scripts hook' );

		$this->assertIsInt( has_action( 'wp_enqueue_scripts', [ $enqueues, 'register_ri_resources_frontend_rtabs' ] ),
			'Frontend scripts should be hooked into wp_enqueue_scripts hook' );

		$this->assertIsInt( has_action( 'admin_enqueue_scripts', [ $enqueues, 'register_disabled_but_not_disabled' ] ),
			'Admin scripts should be hooked into wp_enqueue_scripts hook' );
	}

	public function test_init_hooks_doesnt_have_proper_actions_enqueues() {
		$enqueues =  new \Pei_Ri_Resources\Enqueues();
		$enqueues->init();

		$this->assertFalse( has_action( 'wp_enqueue_scripts', [ $enqueues, 'register_ri_resources_admin_script' ] ),
			'Frontend scripts should not be enqueued with admin_enqueue_scripts hook' );

		$this->assertFalse( has_action( 'admin_enqueue_scripts', [ $enqueues, 'register_ri_resources_frontend_css' ] ),
			'Frontend scripts should not be enqueued with admin_enqueue_scripts hook' );

		$this->assertFalse( has_action( 'wp_enqueue_scripts', [ $enqueues, 'register_ri_resources_admin_css' ] ),
			'Frontend scripts should not be enqueued with admin_enqueue_scripts hook' );

		$this->assertFalse( has_action( 'admin_enqueue_scripts', [ $enqueues, 'register_ri_resources_frontend_rtabs' ] ),
			'Frontend scripts should not be enqueued with admin_enqueue_scripts hook' );

	}

	public function test_if_register_ri_resources_admin_css_register_admin_css_file() {
		Functions\expect('wp_register_style')->with( 'ri_wp_admin_css', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/css/admin-style.css', [], RI_RESOURCES_PLUGIN_CURRENT_VERSION )->once();
		Functions\expect('wp_enqueue_style')->with('ri_wp_admin_css')->once();

		// sut
		$sut =  new \Pei_Ri_Resources\Enqueues();
		$sut->register_ri_resources_admin_css();

		$this->assertNotNull( $sut );
	}

	public function test_if_register_register_ri_resources_frontend_css_register_frontend_css_file() {
		Functions\expect('wp_register_style')->with( 'ri_wp_frontend_css', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/css/style.css', [], RI_RESOURCES_PLUGIN_CURRENT_VERSION )->once();
		Functions\expect('wp_enqueue_style')->with('ri_wp_frontend_css')->once();

		// sut
		$sut =  new \Pei_Ri_Resources\Enqueues();
		$sut->register_ri_resources_frontend_css();

		$this->assertNotNull( $sut );
	}

	public function test_if_register_register_register_ri_resources_admin_script_register_admin_js_file() {
		Functions\expect('wp_register_script')->with( 'ri_wp_admin_script', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/admin-script.js', [ 'jquery' ], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true )->once();
		Functions\expect('wp_enqueue_script')->with('ri_wp_admin_script')->once();

		// sut
		$sut =  new \Pei_Ri_Resources\Enqueues();
		$sut->register_ri_resources_admin_script();

		$this->assertNotNull( $sut );
	}

	public function test_if_register_register_ri_resources_frontend_rtabs_register_frontend_js_file() {
		Functions\expect('wp_register_script')->with( 'ri_wp_frontend_rtabs_script', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/resource-tabs.js', [], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true )->once();
		Functions\expect('wp_enqueue_script')->with('ri_wp_frontend_rtabs_script')->once();

		// sut
		$sut =  new \Pei_Ri_Resources\Enqueues();
		$sut->register_ri_resources_frontend_rtabs();

		$this->assertNotNull( $sut );
	}

	public function test_if_register_ri_resources_required_post_title_dont_register_script_when_post_type_is_not_a_content_but_base_is_post(){
		
		Functions\expect('get_current_screen')->andReturn((object)['post_type' => 'any_post_type', 'base' => 'post'])->once();
		Functions\expect('wp_register_script')->never();
		Functions\expect('wp_enqueue_script')->never();
		// sut
		$sut =  new \Pei_Ri_Resources\Enqueues();
		$sut->register_ri_resources_required_post_title();

		$this->assertNotNull( $sut );
	}

	public function test_if_register_ri_resources_required_post_title_dont_register_script_when_post_type_is_a_content_but_base_is_not_a_post(){
		
		Functions\expect('get_current_screen')->andReturn((object)['post_type' => 'content', 'base' => 'something_else'])->once();
		Functions\expect('wp_register_script')->never();
		Functions\expect('wp_enqueue_script')->never();
		// sut
		$sut =  new \Pei_Ri_Resources\Enqueues();
		$sut->register_ri_resources_required_post_title();

		$this->assertNotNull( $sut );
	}

	public function test_if_register_ri_resources_required_post_title_register_script_when_post_type_is_a_content_and_base_is_post(){
		
		Functions\expect('get_current_screen')->andReturn((object)['post_type' => 'content', 'base' => 'post'])->once();
		Functions\expect('wp_register_script')->with( 'ri_wp_required_post_title', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/ri-required-post-title.js', [ 'jquery' ], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true )->once();
		Functions\expect('wp_enqueue_script')->with('ri_wp_required_post_title')->once();
		// sut
		$sut =  new \Pei_Ri_Resources\Enqueues();
		$sut->register_ri_resources_required_post_title();

		$this->assertNotNull( $sut );
	}

	// test if ri_get-term_id_by_name returning null when get_term_by is not object
	public function test_if_ri_get_term_id_by_name_returning_null_when_get_term_by_is_not_object() {

		// mock vars
		$term_name = 'some_term_name';

		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( 'some_value' );

		// sut
		$sut = new \Pei_Ri_Resources\Enqueues();

		// Act
		$result = $sut->ri_get_term_id_by_name( $term_name );

		// Assertions
		$this->assertNull( $result );
	}

	// test if ri_get-term_id_by_name returning term_taxonomy_id when get_term_by is object
	public function test_if_ri_get_term_id_by_name_returning_term_taxonomy_id_when_get_term_by_is_object() {

		// mock vars
		$term_name = 'some_term_name';

		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( (object)['term_taxonomy_id' => 123] );

		// sut
		$sut = new \Pei_Ri_Resources\Enqueues();

		// Act
		$result = $sut->ri_get_term_id_by_name( $term_name );

		// Assertions
		$this->assertSame( 123, $result );
	}

	// test if register_disaled_but_not_disabled doesn't enqueue anything if post type is not content and post base is not an post
	public function test_if_register_disabled_but_not_disabled_doesnt_enqueue_anything_if_post_type_is_not_a_content_and_post_base_is_not_a_post() {

		// mock vars
		$post_type = 'any_post_type';
		$post_base = 'any_post_base';

		//mock methods
		Functions\expect( 'get_current_screen' )
			->once()
			->andReturn( (object)['post_type' => $post_type, 'base' => $post_base] );

		Functions\expect( 'wp_register_script' )->never();
		Functions\expect( 'wp_enqueue_script' )->never();
		// sut
		$sut = new \Pei_Ri_Resources\Enqueues();

		// Act
		$result = $sut->register_disabled_but_not_disabled();

		// Assertions
		$this->assertNull( $result );
	}

	// test if register_disaled_but_not_disabled doesn't enqueue anything if post type is content and post base is not an post
	public function test_if_register_disabled_but_not_disabled_doesnt_enqueue_anything_if_post_type_is_a_content_and_post_base_is_not_a_post() {

		// mock vars
		$post_type = 'content';
		$post_base = 'any_post_base';

		//mock methods
		Functions\expect( 'get_current_screen' )
			->once()
			->andReturn( (object)['post_type' => $post_type, 'base' => $post_base] );

		Functions\expect( 'wp_register_script' )->never();
		Functions\expect( 'wp_enqueue_script' )->never();
		// sut
		$sut = new \Pei_Ri_Resources\Enqueues();

		// Act
		$result = $sut->register_disabled_but_not_disabled();

		// Assertions
		$this->assertNull( $result );
	}

	// test if register_disaled_but_not_disabled enqueue scripts if post type is content and post base is an post but not firing wp_localize_script when $get_fund_term_id is not an int
	public function test_if_register_disabled_but_not_disabled_doesnt_enqueue_anything_if_post_type_is_a_content_and_post_base_is_a_post_without_localize_script_when_get_fund_terms_id_is_not_an_int() {

		// mock vars
		$post_type = 'content';
		$post_base = 'post';

		//mock methods
		Functions\expect( 'get_current_screen' )
			->once()
			->andReturn( (object)['post_type' => $post_type, 'base' => $post_base] );

		Functions\expect( 'wp_register_script' )->twice();
		Functions\expect( 'wp_enqueue_script' )->twice();

		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( null );

		// sut
		$sut = new \Pei_Ri_Resources\Enqueues();

		// Act
		$result = $sut->register_disabled_but_not_disabled();

		// Assertions
		$this->assertNull( $result );
	}

	// test if register_disaled_but_not_disabled enqueue scripts if post type is content and post base is an post and firing wp_localize_script when $get_fund_term_id is an int
	public function test_if_register_disabled_but_not_disabled_doesnt_enqueue_anything_if_post_type_is_a_content_and_post_base_is_a_post_with_localize_script_when_get_fund_terms_id_is_an_int() {

		// mock vars
		$post_type = 'content';
		$post_base = 'post';

		//mock methods
		Functions\expect( 'get_current_screen' )
			->once()
			->andReturn( (object)['post_type' => $post_type, 'base' => $post_base] );

		Functions\expect( 'wp_register_script' )->twice();
		Functions\expect( 'wp_enqueue_script' )->twice();
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( (object)['term_taxonomy_id' => 321] );
		
			Functions\expect( 'wp_localize_script' )->once();
		// sut
		$sut = new \Pei_Ri_Resources\Enqueues();

		// Act
		$result = $sut->register_disabled_but_not_disabled();

		// Assertions
		$this->assertNull( $result );
	}
}