<?php
use Brain\Monkey\Functions;

class InstitutionsHooksTest extends \Pei_Ri_Resources\TestCase {

	protected $decimal_count = 3;

	public function test_init_hooks_institutions() {

		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		// Expected output
		$expected_prior = 10;

		$institutions = new \Pei_Ri_Resources\Institutions();
		$institutions->init();

		$result = has_action( 'init', array( $institutions, 'ri_institutions_post_type' ) );
		$result1 = has_filter( 'acf/prepare_field/name=ri_post_id', array( $institutions, 'ri_add_post_id_to_post_id_field' ) );
		$result2 = has_filter( 'acf/validate_value/name=aum_amount', array( $institutions, 'ri_validate_decimals' ) );

		// Assertions
		$this->assertSame( $expected_prior, $result );
		$this->assertSame( $expected_prior, $result1 );
		$this->assertSame( $expected_prior, $result2 );
	}

	public function test_init_hook_doesnt_have_proper_actions_institutions() {
		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$institutions = new \Pei_Ri_Resources\Institutions();

		$this->assertFalse( has_action( 'wp_header', $institutions->init() ),
			'Post type does not have wp_header hook' );

	}

	public function test_if_ri_add_post_id_to_post_id_field_returning_post_id_as_value_if_post_is__not_an_object(){

		global $post;
		$post = Mockery::mock( '\WP_Post' );
		$post = (array)[ 'ID' => '123123' ];

		// mock vars
		$post = '';
		$field = array(
			'value' => 'some_value'
		);

		// sut
		$institutions = new \Pei_Ri_Resources\Institutions();

		// Act
		$result = $institutions->ri_add_post_id_to_post_id_field( $field );

		// Assertions
		$this->assertSame( $field, $result );
	}

	public function test_if_ri_add_post_id_to_post_id_field_returning_post_id_as_value_if_post_is_an_object(){

		// mock vars
		global $post;
		$post = Mockery::mock( '\WP_Post' );
		$post = (object)[ 'ID' => '123123' ];
		$field = array(
			'value' => 'some_value'
		);

		$expected_return = array(
			'value' => '123123',
			'readonly' => true
		);

		// sut
		$institutions = new \Pei_Ri_Resources\Institutions();

		// Act
		$result = $institutions->ri_add_post_id_to_post_id_field( $field );

		// Assertions
		$this->assertSame( $expected_return, $result );
	}

	public function test_validate_ri_decimals_if_decimals_count_is_3() {

		$valid = true;

		$test_value = '1.456';

		$field = array();

		$input_name = 'test_field';

		$institutions = new \Pei_Ri_Resources\Institutions();

		$result = $institutions->ri_validate_decimals( $valid, $test_value, $field, $input_name );

		$this->assertSame( $valid, $result );
	}

	public function test_validate_ri_decimals_if_decimals_count_is_more_then_3() {

		$valid = true;

		$test_value = '1.4565';

		$field = array();

		$input_name = 'test_field';

		$institutions = new \Pei_Ri_Resources\Institutions();
		$message = "Max allowed numbers after '.' are {$this->decimal_count}";
		Functions\stubs(
			[
				'__'           => $message,
			]
		);

		$result = $institutions->ri_validate_decimals( $valid, $test_value, $field, $input_name );

		$this->assertSame( $message, $result );
	}

	public function test_return_false_if_valid_is_not_equal_true() {
		$valid = false;

		$test_value = '1.4565';

		$field = array();

		$input_name = 'test_field';

		$institutions = new \Pei_Ri_Resources\Institutions();
		$message = "Max allowed numbers after '.' are {$this->decimal_count}";
		Functions\stubs(
			[
				'__'           => $message,
			]
		);

		$result = $institutions->ri_validate_decimals( $valid, $test_value, $field, $input_name );

		$this->assertSame( false, $result );
	}
}
