<?php
use Brain\Monkey\Functions;

class ContactMetaBoxInstitutionsHooksTest extends \Pei_Ri_Resources\TestCase {

	public function test_init_hooks_contact_institutions_metabox() {
		
		
		$contacts_metabox = new \Pei_Ri_Resources\Contacts_Metabox_For_Institutions();
		$contacts_metabox->init();
		
		$this->assertTrue( has_action( 'add_meta_boxes', $contacts_metabox->init() ),
			'Add Meta Box should be hooked into init hook' );
		
	}

	public function test_init_hook_doesnt_have_proper_actions_contact() {
		$contacts_metabox = new \Pei_Ri_Resources\Contacts_Metabox_For_Institutions();
		$contacts_metabox->init();

		$this->assertFalse( has_action( 'wp_head', $contacts_metabox->init() ),
			'WP Head should not be hooked into init hook' );

		$this->assertFalse( has_action( 'init', $contacts_metabox->init() ),
			'INit should not be hooked into init hook' );

	}
}
