<?php

use Brain\Monkey\Functions;

class HelperFunctionsTest extends \Pei_Ri_Resources\TestCase {

	private $salithru_api_key = '34e1abe0ed4565f94cd23b3e49066b9i';

	private $api_url = 'https://develop.alerts-api.com';

	private $logged_in_user_session = '55e3opi7ac4565f94cd78b3e49041b9i';

	private $pei_user = 'peiuser@peimedia.com';

	private $odin_blaize_frontend_url = 'some-fake-url';

	public function test_get_sailthru_api_key() {
		$helper = new \Pei_Ri_Resources\Helper();

		Functions\when( 'get_option' )->justReturn( [
			'sailthru_api_user' => 'PEI',
			'sailthru_api_key'  => $this->salithru_api_key,
		] );

		$this->assertSame( $this->salithru_api_key, $helper->get_sailthru_api_key() );

	}

	public function test_get_sailthru_api_key_returns_empty_value() {
		$helper = new \Pei_Ri_Resources\Helper();

		Functions\when( 'get_option' )->justReturn( [
			'sailthru_api_user' => 'PEI',
			'sailthru_api_key'  => '',
		] );

		$this->assertEmpty( '', $helper->get_sailthru_api_key() );

	}

	public function test_get_brand_url() {
		$helper = new \Pei_Ri_Resources\Helper();

		Functions\when( 'get_option' )->justReturn( $this->odin_blaize_frontend_url );

		$this->assertSame( $this->odin_blaize_frontend_url, $helper->get_brand_url() );
	}

	public function test_get_brand_empty_url() {
		$helper = new \Pei_Ri_Resources\Helper();

		Functions\when( 'get_option' )->justReturn( '' );

		$this->assertEmpty( '', $helper->get_brand_url() );
	}

	public function test_get_api_url() {
		$expected_output = 'someurl';
		$exppected_response = array( 'sailthru_api_url' => 'someurl' );

		Functions\when( 'get_option' )->justReturn( $exppected_response );

		$helper = new \Pei_Ri_Resources\Helper();
		$result = $helper->get_api_url();

		$this->assertSame( $expected_output, $result );
	}

	public function test_get_api_empty_url() {
		$helper = new \Pei_Ri_Resources\Helper();

		Functions\when( 'get_option' )->justReturn( '' );

		$this->assertNotSame('', $helper->get_api_url() );
	}

	public function test_get_logged_in_user_email() {
		$helper = new \Pei_Ri_Resources\Helper();
		if ( ! isset( $_COOKIE['blaize_session'] ) ) {
			$_COOKIE['blaize_session'] = $this->logged_in_user_session;
		}

		Functions\when( 'get_option' )->justReturn( $this->odin_blaize_frontend_url );
		Functions\when( 'esc_url' )->justReturn( $this->odin_blaize_frontend_url . '/blaize/account' );


		Functions\when( 'wp_remote_get' )->justReturn(
			[
				'headers' => [
					'date'         => date('Y:m:s'),
					'server'       => 'apache',
					'x-powered-by' => 'PHP/7.4'
				],
				'body' => json_encode( [
					'identifiers' => [
						'email_address' => 'peiuser@peimedia.com'
					]
				] ),
				'response' => [
					'code'    => 200,
					'message' => 'OK'
				]
			]
		);
		Functions\when( 'wp_remote_retrieve_body' )->justReturn(
			json_encode( [
				'identifiers' => [
					'email_address' => 'peiuser@peimedia.com'
				]
			] )
		);

		$this->assertSame( $this->pei_user, $helper->get_logged_in_user_email() );
	}

	public function test_return_false_when_user_is_not_logged_in() {
		$helper = new \Pei_Ri_Resources\Helper();

		Functions\when( 'get_option' )->justReturn( $this->odin_blaize_frontend_url );
		Functions\when( 'esc_url' )->justReturn( $this->odin_blaize_frontend_url . '/blaize/account' );
		Functions\when( 'is_wp_error' )->justReturn( true );


		Functions\when( 'wp_remote_get' )->justReturn(
			[
				'headers' => [
					'date'         => date('Y:m:s'),
					'server'       => 'apache',
					'x-powered-by' => 'PHP/7.4'
				],
				'body' => json_encode( [
					'identifiers' => [
						'email_address' => false,
					]
				] ),
				'response' => [
					'code'    => 401,
					'message' => 'unauthorized'
				]
			]
		);


		$this->assertFalse( $helper->get_logged_in_user_email(), 'User does not exists!' );
	}

	public function test_get_meta_slug_for_content_type() {
		$helper = new \Pei_Ri_Resources\Helper();

		$content_type = 'content';
		$meta         = 'content_type';

		$expected = $helper::get_meta_slug_for_content_type( $content_type, $meta );

		$this->assertSame(
			'related_content_content_type',
			$expected
		);

	}

	public function test_get_meta_slug_for_product_type() {
		$helper = new \Pei_Ri_Resources\Helper();

		$content_type = 'product';
		$meta         = 'product_type';

		$expected = $helper::get_meta_slug_for_content_type( $content_type, $meta );

		$this->assertSame(
			'ri_product_product_type',
			$expected
		);

	}

	public function test_ri_breadcrumbs() {
		$helper = new \Pei_Ri_Resources\Helper();
		$institution_id = 12345;
		$institution_title = 'ESG Asset Class';
		Functions\when( 'esc_url' )->justReturn( 'http://testcontentesghub.test' );
		Functions\when( 'home_url' )->justReturn( 'http://testcontentesghub.test' );
		Functions\when( 'esc_html' )->justReturn( 'Home' );
		Functions\when( 'get_the_title' )->justReturn( 'ESG Asset Class' );
		Functions\when( 'get_permalink' )->justReturn( 'http://testcontentesghub.test/'.$institution_id.'/'.$institution_title );


		$actual = $helper::ri_content_breadcrumbs( $institution_id, $institution_title );
		$expected = $this->fake_bread_crumbs();
		$this->assertSame( $expected, $actual );
	}

	public function fake_bread_crumbs() {
		return '<div class="entry-crumbs ri-breadcrumbs" itemprop="breadcrumb"><a href="http://testcontentesghub.test">Home</a><i class="td-icon-right td-bread-sep"></i> <a href="http://testcontentesghub.test">Home</a><i class="td-icon-right td-bread-sep"></i> <a href="http://testcontentesghub.test">Home</a><i class="td-icon-right td-bread-sep"></i> ESG Asset Class</div>';
	}
}
