<?php
use Brain\Monkey\Functions;

class ContentShortcodeTest extends \Pei_Ri_Resources\TestCase {
	public function test_content_shortcode_does_have_proper_hooks() {
		Functions\stubs(
			[
				'add_shortcode'  => 1,
				'get_option'     => 1,
			]
		);

		$content =  new \Pei_Ri_Resources\Content_Shortcode();
		$content->init();



		$this->assertIsInt( has_action( 'init', [ $content, 'register_assets' ] ),
			'Content Scripts should be hooked into init hook' );

		$this->assertIsInt( has_action( 'wp_footer', [ $content, 'render_assets' ] ),
			'Content footer scripts should be hooked into wp_footer hook' );
	}

	public function test_init_hooks_doesnt_have_proper_actions_enqueues() {
		Functions\stubs(
			[
				'add_shortcode'  => 1,
				'get_option'     => 1,
			]
		);
		$content =  new \Pei_Ri_Resources\Content_Shortcode();
		$content->init();

		$this->assertFalse( has_action( 'wp_enqueue_scripts', [ $content, 'register_assets' ] ),
			'Content scripts should not be enqueued with admin_enqueue_scripts hook' );

		$this->assertFalse( has_action( 'admin_enqueue_scripts', [ $content, 'render_assets' ] ),
			'Content Frontend scripts should not be enqueued with admin_enqueue_scripts hook' );

	}
	public function test_content_has_shortcode() {
		$shortcode_content = $this->get_shortcode_content();

		$attributes = array(
			'shortcode' => $shortcode_content
		);

		$content = new \Pei_Ri_Resources\Content_Shortcode();

		Functions\stubs(
			[
				'get_option' => 1,
			]
		);

		Functions\when('add_shortcode')->justReturn(
			[
				'shortcode' => $shortcode_content
			]
		);


		$shortcode = $content->Content_Shortcode( $attributes );

		$this->assertSame($shortcode, $shortcode_content);
	}

	public function test_content_has_not_shortcode() {
		$shortcode_content = "<div class='test-shortcode'><p>RI Content SHORTCODE</p></div>";

		$attributes = array(
			'shortcode' => $shortcode_content
		);

		$content = new \Pei_Ri_Resources\Content_Shortcode();

		Functions\stubs(
			[
				'get_option' => 1,
			]
		);

		Functions\when('add_shortcode')->justReturn(
			[
				'shortcode' => $shortcode_content
			]
		);


		$shortcode = $content->Content_Shortcode( $attributes );

		$this->assertNotSame($shortcode, $shortcode_content);
	}

	public function get_shortcode_content() {
		ob_start();

		include RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/public/content.php';

		$html = ob_get_contents();

		ob_end_clean();

		return $html;
	}
}
