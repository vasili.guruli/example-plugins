<?php
use Brain\Monkey\Functions;

class RolesTest extends \Pei_Ri_Resources\TestCase {

    /**
	 *  @var string $managing_role
	 */
	protected $managing_role = 'resource_manager';

	/**
	 *  @var string $viewer_role
	 */
	protected $viewer_role = 'resource_viewer';

	/**
	 *  @var string $managing_role_name
	 */
	protected $managing_role_name = 'Resource Manager';

	/**
	 *  @var string $viewer_role_name
	 */
	protected $viewer_role_name = 'Resource Viewer';
    /**
	 * @var array $custom_capabilities_names
	 */
	protected $custom_capabilities_names = array(
		'contact'     => array(
			'singular' => 'contact',
			'plural'   => 'contacts',
		),
		'content'     => array(
			'singular' => 'content',
			'plural'   => 'contents',
		),
		'institution' => array(
			'singular' => 'institution',
			'plural'   => 'institutions',
		),
	);

    /**
     * Test if all needed actions and filters are added to init method
     */
    public function test_register_roles_method() {
        Functions\expect('add_role')->twice();

        // Sut
        $sut = new \Pei_Ri_Resources\Roles();
		$result = $sut->register_roles();

        // Assertions
		$this->assertNull( $result );
    }

    public function test_if_not_remove_admin_pages_remove_menu_pages_when_user_doesnt_have_any_roles(){
        global $current_user;
        $current_user = (object)['roles' => array()];

        // Sut
        $sut = new \Pei_Ri_Resources\Roles();
		$result = $sut->remove_admin_pages();

        // Assertions
		$this->assertNull( $result );
    }

    public function test_if_remove_admin_pages_remove_menu_pages_when_have_roles(){
        global $current_user;
        $current_user = (object)['roles' => array('resource_manager')];

        // Mock methods
        Functions\expect('remove_menu_page')->with('edit.php')->once();
        Functions\expect('remove_menu_page')->with('upload.php')->once(); // Media
		Functions\expect('remove_menu_page')->with('link-manager.php')->once(); // Links
		Functions\expect('remove_menu_page')->with('edit-comments.php')->once(); // Comments
		Functions\expect('remove_menu_page')->with('edit.php?post_type=page')->once(); // Pages
		Functions\expect('remove_menu_page')->with('plugins.php')->once(); // Plugins
		Functions\expect('remove_menu_page')->with('themes.php')->once(); // Appearance
		Functions\expect('remove_menu_page')->with('users.php')->once(); // Users
		Functions\expect('remove_menu_page')->with('tools.php')->once(); // Tools
		Functions\expect('remove_menu_page')->with('options-general.php')->once(); // Settings
		Functions\expect('remove_menu_page')->with('edit.php?post_type=events')->once();
		Functions\expect('remove_menu_page')->with('edit.php?post_type=pwl_custom_feed')->once();
		Functions\expect('remove_menu_page')->with('edit.php?post_type=print_editions')->once();
		Functions\expect('remove_menu_page')->with('edit.php?post_type=shareable_list')->once();
		Functions\expect('remove_menu_page')->with('delighted_api')->once();
		Functions\expect('remove_menu_page')->with('vc-welcome')->once();
		Functions\expect('remove_menu_page')->with('visualizer')->once();

        // Sut
        $sut = new \Pei_Ri_Resources\Roles();
		$result = $sut->remove_admin_pages();

        // Assertions
		$this->assertNull( $result );
    }

}
