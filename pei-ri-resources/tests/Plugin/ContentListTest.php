<?php
use Brain\Monkey\Functions;

class ContentListTest extends \Pei_Ri_Resources\TestCase {
	protected $post_type = 'content';
	
	public function test_ri_content_columns() {
		$defaults = array();
		
		$content = new \Pei_Ri_Resources\Content_List();
		
		$actual = $content->ri_content_columns( $defaults );
		$defaults['content_id']       = 'Id';
		$defaults['title']            = 'Title';
		$defaults['author']           = 'Author';
		$defaults['taxonomy-content-type']     = 'Content Type';
		$defaults['institution_name'] = 'Institution Name';
		$defaults['date']             = 'Date';
		
		$this->assertEquals( $defaults, $actual );
	}
	
	public function test_ri_content_columns_content_content_id() {
		$post_id = 12345;
		$column = 'content_id';
		
		$expected = 'AGS Institution';
		$expected2 = '<a href="https://test-institution.test?action=edit&institution='.$post_id.'">AGS Institution</a>';
		
		Functions\when( 'esc_attr' )->justReturn( 'AGS Institution' );
		Functions\when( 'get_post_meta' )->justReturn( '1' );
		
		Functions\when( 'get_the_title' )->justReturn( 'AGS Institution' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		$content = new \Pei_Ri_Resources\Content_List();
		
		$this->expectOutputString( $expected );
		
		$content->ri_content_columns_content( $column, $post_id );
	}
	
	public function test_ri_content_columns_content_institution_name_when_content_is_product() {
		$post_id = 12345;
		$column = 'institution_name';
		
		$expected = '<a href="https://test-institution.test?action=edit&institution='.$post_id.'">AGS Institution Product</a>';
		
		Functions\when( 'esc_attr' )->justReturn( 'AGS Institution Product' );
		Functions\when( 'get_post_meta' )->justReturn( '1' );
		
		Functions\when( 'get_the_title' )->justReturn( 'AGS Institution Product' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		$content = new \Pei_Ri_Resources\Content_List();
		
		$this->expectOutputString( $expected );
		
		$content->ri_content_columns_content( $column, $post_id );
	}
	
	public function test_ri_content_columns_content_institution_name_when_content_is_not_product() {
		$post_id = 12345;
		$column = 'institution_name';
		
		$expected = '<a href="https://test-institution.test?action=edit&institution='.$post_id.'">AGS Institution</a>';
		
		Functions\when( 'esc_attr' )->justReturn( 'AGS Institution' );
		Functions\when( 'get_post_meta' )->justReturn( '0' );
		
		Functions\when( 'get_the_title' )->justReturn( 'AGS Institution' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		$content = new \Pei_Ri_Resources\Content_List();
		
		$this->expectOutputString( $expected );
		
		$content->ri_content_columns_content( $column, $post_id );
	}
}