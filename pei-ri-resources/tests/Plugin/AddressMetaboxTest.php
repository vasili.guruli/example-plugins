<?php
namespace Pei_Ri_Resources;
use Brain\Monkey\Functions;

class AddressMetaboxTest extends \Pei_Ri_Resources\TestCase {
	
	private $contacts = array(
		1234      => 'James',
		4567      => 'Jack',
		99999999  => 'John'
	);
	
	private $existing_contact = array(
		'John' => 99999999,
	);
	
	private $addrress = array(
		'address_line_1' => 'Test Address',
		'address_line_2' => 'Test Address',
		'address_line_3' => 'Test Address',
		'city' => 'Test City',
		'region' => 'Test Region',
		'country' => 'TR',
		'postal_code' => '12345',
	);
	
	public function test_contacts_address_register_metabox() {
		$mock = $this->createMock( Addresses_Metabox_For_Contacts::class );
		
		$mock->expects( $this->never() )->method( 'contacts_addresses_metabox' );
		
		$this->register_contacts_metabox();
	}
	
	public function test_contacts_address_not_register_metabox() {
		$contacts_metabox = new Addresses_Metabox_For_Contacts();
		Functions\when(
			'add_meta_box'
		)->justReturn(
			[
				'contacts-addresses',
				'Address Metabox',
				array(__CLASS__, 'render_fake_metabox_for_addresses'),
				'contact',
			]
		);
		Functions\when(
			'__'
		)->justReturn(
			'Address Metabox'
		);
		$this->assertSame( null, $contacts_metabox->contacts_addresses_metabox() );
	}
	
	public function test_address_meta_box_for_content() {
		$contacts_metabox = new Addresses_Metabox_For_Contacts();
		
		$post = $this->getMockBuilder( 'WP_Post' )->getMock();
		
		$actual   = $this->render_fake_metabox_for_addresses();
		
		$expected = $contacts_metabox->add_meta_box_for_contacts( $post );
		
		$this->assertEquals( $expected, $actual );
		
	}
	
	public function test_save_contact_to_institution() {
		
		$contacts_metabox = new Addresses_Metabox_For_Contacts();
		$fake_post_id = 99999999;
		
		$fake_contacts = array(
			1234 => 'James',
			4567 => 'Jack'
		);
		
		Functions\when( 'get_post_type' )->justReturn( 'contact' );
		
		Functions\when( 'current_user_can' )->justReturn( true );
		
		Functions\when( 'is_admin' )->justReturn( true );
		
		Functions\when( 'get_post_meta' )->justReturn( [
			$fake_contacts
		] );
		
		$fake_contacts[$fake_post_id] = 'John';
		
		Functions\when( 'update_post_meta' )->justReturn( [
			$fake_contacts
		] );
	
		$contacts_metabox->save_contact_to_institution( $fake_post_id );
		
		$this->assertSame( $this->contacts, $fake_contacts );
		
	}
	
	public function test_do_not_save_contact_to_institution() {
		
		$contacts_metabox = new Addresses_Metabox_For_Contacts();
		$fake_post_id = 99999999;
		
		$fake_contact = array(
			'John'   => 99999999
		);
		
		Functions\when( 'get_post_type' )->justReturn( 'contact' );
		
		Functions\when( 'current_user_can' )->justReturn( true );
		
		Functions\when( 'is_admin' )->justReturn( true );
		
		Functions\when( 'get_post_meta' )->justReturn( $fake_contact );
		
		$contacts_metabox->save_contact_to_institution( $fake_post_id );
		
		$this->assertSame( $this->existing_contact, $fake_contact );
		
	}
	
	public function test_save_address_to_contact() {
		
		$contacts_metabox = new Addresses_Metabox_For_Contacts();
		$fake_post_id = 99999999;
		if ( ! isset( $_POST['ri_contact_address'] ) ) {
			$_POST['ri_contact_address'] = array(
				'address_line_1' => 'Test Address',
				'address_line_2' => 'Test Address',
				'address_line_3' => 'Test Address',
				'city' => 'Test City',
				'region' => 'Test Region',
				'country' => 'TR',
				'postal_code' => '12345',
			);
		}
		
		$ri_contact_address = $_POST['ri_contact_address'];
		
		Functions\when( 'get_post_type' )->justReturn( 'contact' );
		
		Functions\when( 'current_user_can' )->justReturn( true );
		
		Functions\when( 'is_admin' )->justReturn( true );
		
		Functions\when( 'sanitize_text_field' )->justReturn( $ri_contact_address );
		
		Functions\when( 'update_post_meta' )->justReturn( $ri_contact_address );
		
		
		
		$contacts_metabox->save_address_for_contact( $fake_post_id );
		
		$this->assertSame( $this->addrress, $ri_contact_address );
		
	}
	
	public function test_do_not_save_address_to_contact() {
		
		$contacts_metabox = new Addresses_Metabox_For_Contacts();
		$fake_post_id = 99999999;
		if ( ! isset( $_POST['ri-contact-address'] ) ) {
			$_POST['ri-contact-address'] = array(
				'address_line_1' => 'Test Address',
				'address_line_2' => 'Test Address',
				'address_line_3' => 'Test Address',
				'city' => 'Test City',
				'region' => 'Test Region',
				'country' => 'TR',
				'postal_code' => '12345',
			);
		}
		
		$ri_contact_address = $_POST['ri-contact-address'];
		
		Functions\when( 'get_post_type' )->justReturn( 'contact' );
		
		Functions\when( 'current_user_can' )->justReturn( true );
		
		Functions\when( 'is_admin' )->justReturn( true );
		
		Functions\when( 'sanitize_text_field' )->justReturn( 'Address Metabox' );
		
		Functions\when( 'update_post_meta' )->justReturn( [
			1234 => 'Fake Address'
		] );
		
		$contacts_metabox->save_address_for_contact( $fake_post_id );
		
		$this->assertEquals( $this->addrress, $ri_contact_address );
		
	}
	
	public function test_address_meta_box_for_content_not_have_metabox() {
		$fake_metabox = "<div class='test-metabox'><p>FAKE METABOX</p></div>";
		
		$actual   = $this->render_fake_metabox_for_addresses();
		
		$this->assertNotSame( $actual, $fake_metabox );
	}
	
	public function render_fake_metabox_for_addresses() {
		include RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/admin/contact-metabox.php';
	}
	
	private function register_contacts_metabox() {
		Functions\when( 'add_meta_box' )->justReturn(
			[
				'contacts-addresses',
				'Address Metabox',
				array(__CLASS__, 'render_fake_metabox_for_addresses'),
				'contact',
			]
		);
	}
}
