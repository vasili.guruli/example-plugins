<?php
use Brain\Monkey\Functions;

class FundsHooksTest extends \Pei_Ri_Resources\TestCase {

    /**
     * Test if all needed actions and filters are added to init method
     */
    public function test_init_method() {
        // Expected output
		$expected_prior = 10;

        // Sut
        $sut = new \Pei_Ri_Resources\Funds();
		$sut->init();

        // Act
        $result = has_filter( 'acf/prepare_field/name=fund_product_id', array( $sut, 'ri_fund_add_post_id_to_acf_field_and_mark_this_field_as_read_only' ) );
        $result1 = has_filter( 'acf/prepare_field/key=field_634d6b77b4fa0', array( $sut, 'ri_load_default_content_type' ) );

        // Assertions
		$this->assertSame( $expected_prior, $result );
		$this->assertSame( $expected_prior, $result1 );
    }

    public function test_if_ri_add_post_id_to_post_id_field_returning_post_id_as_value_if_post_is__not_an_object(){

		global $post;
		$post = Mockery::mock( '\WP_Post' );
		$post = (array)[ 'ID' => '123123' ];

		// mock vars
		$post = '';
		$field = array(
			'value' => 'some_value'
		);

		// sut
		$institutions = new \Pei_Ri_Resources\Institutions();

		// Act
		$result = $institutions->ri_add_post_id_to_post_id_field( $field );

		// Assertions
		$this->assertSame( $field, $result );
	}
    
    public function test_if_ri_add_post_id_to_post_id_field_returning_post_id_as_value_if_post_is_an_object(){

		// mock vars
		global $post;
		$post = Mockery::mock( '\WP_Post' );
		$post = (object)[ 'ID' => '123123' ];
		$field = array(
			'value' => 'some_value'
		);

		$expected_return = array(
			'value' => '123123',
			'readonly' => true
		);

		// sut
		$sut = new \Pei_Ri_Resources\Funds();

		// Act
		$result = $sut->ri_fund_add_post_id_to_acf_field_and_mark_this_field_as_read_only( $field );

		// Assertions
		$this->assertSame( $expected_return, $result );
	}

	// Test if ri_load_default_content_type returning default value when get_term_id is not int
	public function test_if_ri_load_default_content_type_returning_default_value_when_get_term_id_is_not_int() {

		// mock vars
		$field = array(
			'value' => 'some_value'
		);

		$expected_return = array(
			'value' => 'some_value'
		);
		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( 'some_value' );

		// sut
		$sut = new \Pei_Ri_Resources\Funds();

		// Act
		$result = $sut->ri_load_default_content_type( $field );

		// Assertions
		$this->assertSame( $expected_return, $result );
	}

	// Test if ri_load_default_content_type returning changed value when get_term_id is int
	public function test_if_ri_load_default_content_type_returning_changed_value_when_get_term_id_is_int() {

		// mock vars
		$field = array(
			'value' => 'some_value'
		);

		$expected_return = array(
			'value' => 123
		);
		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( (object)['term_taxonomy_id' => 123] );

		// sut
		$sut = new \Pei_Ri_Resources\Funds();

		// Act
		$result = $sut->ri_load_default_content_type( $field );

		// Assertions
		$this->assertSame( $expected_return, $result );
	}

	// test if ri_get-term_id_by_name returning null when get_term_by is not object
	public function test_if_ri_get_term_id_by_name_returning_null_when_get_term_by_is_not_object() {

		// mock vars
		$term_name = 'some_term_name';

		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( 'some_value' );

		// sut
		$sut = new \Pei_Ri_Resources\Funds();

		// Act
		$result = $sut->ri_get_term_id_by_name( $term_name );

		// Assertions
		$this->assertNull( $result );
	}

	// test if ri_get-term_id_by_name returning term_taxonomy_id when get_term_by is object
	public function test_if_ri_get_term_id_by_name_returning_term_taxonomy_id_when_get_term_by_is_object() {

		// mock vars
		$term_name = 'some_term_name';

		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( (object)['term_taxonomy_id' => 123] );

		// sut
		$sut = new \Pei_Ri_Resources\Funds();

		// Act
		$result = $sut->ri_get_term_id_by_name( $term_name );

		// Assertions
		$this->assertSame( 123, $result );
	}
	
}
