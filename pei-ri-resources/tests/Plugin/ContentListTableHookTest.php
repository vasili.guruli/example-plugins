<?php
use Brain\Monkey\Functions;

class ContentListTableHookTest extends \Pei_Ri_Resources\TestCase {
	protected $post_type = 'content';
	
	public function test_init_content_list_method() {
		
		// mock expections
		$expected_prior = 10;
		
		// Act
		$sut = new \Pei_Ri_Resources\Content_List();
		$sut->init();
		
		$result = has_filter( "manage_{$this->post_type}_posts_columns", array( $sut, "ri_{$this->post_type}_columns" ) );
		$result1 = has_action( "manage_{$this->post_type}_posts_custom_column", array(
			$sut,
			"ri_{$this->post_type}_columns_content"
		) );
		
		// Assertions
		$this->assertSame( $expected_prior, $result );
		$this->assertSame( $expected_prior, $result1 );
	}
}