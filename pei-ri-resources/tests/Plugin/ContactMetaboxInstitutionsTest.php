<?php
use Brain\Monkey\Functions;

class ContactMetaboxInstitutionsTest extends \Pei_Ri_Resources\TestCase {

	public function test_contact_institutions_metabox() {
		$contacts_metabox = new \Pei_Ri_Resources\Contacts_Metabox_For_Institutions();

		$post = $this->getMockBuilder( 'WP_Post' )->getMock();

		$actual   = $this->get_meta_box();
		$expected = $contacts_metabox->add_meta_box_for_institutions( $post );
		$this->setOutputCallback(function() {});
		$this->assertEquals( $expected, $actual );
	}

	public function get_meta_box() {
		 include RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/admin/institution-metabox.php';
	}

	public function test_contact_institution_not_have_metaxob() {
		$fake_metabox = "<div class='test-metabox'><p>FAKE METABOX</p></div>";

		$actual   = $this->get_meta_box();
		$this->setOutputCallback(function() {});
		$this->assertNotSame( $actual, $fake_metabox );
	}
}
