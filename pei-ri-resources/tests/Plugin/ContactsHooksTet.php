<?php
use Brain\Monkey\Functions;

class ContactsHooksTet extends \Pei_Ri_Resources\TestCase {
	public function test_init_hooks_contact() {

		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$contacts = new \Pei_Ri_Resources\Contacts();
		$contacts->init();

		$this->assertTrue( has_action( 'init', $contacts->init() ),
			'Init should be hooked into init hook' );
		
		$expected_prior = 10;
		
		$result = has_filter( 'acf/prepare_field/name=contact_id', array( $contacts, 'ri_add_contact_id_and_make_it_readonly' ) );
		
		// Assertions
		$this->assertSame( $expected_prior, $result );
	}

	public function test_init_hook_doesnt_have_proper_actions_contact() {
		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$contacts = new \Pei_Ri_Resources\Contacts();

		$this->assertFalse( has_action( 'wp_header', $contacts->init() ),
			'Post type does not have wp_header hook' );

	}
	
	public function test_ri_add_contact_id_and_make_it_readonly_if_post_id_is_not_an_object(){
		
		global $post;
		$post = Mockery::mock( '\WP_Post' );
		$post = (array)[ 'ID' => '123123' ];
		
		// mock vars
		$post = '';
		$field = array(
			'value' => 'some_value'
		);
		
		// sut
		$contacts = new \Pei_Ri_Resources\Contacts();
		
		// Act
		$result = $contacts->ri_add_contact_id_and_make_it_readonly( $field );
		
		// Assertions
		$this->assertSame( $field, $result );
	}
	
	public function test_ri_add_contact_id_and_make_it_readonly_if_post_id_is_object(){
		
		// mock vars
		global $post;
		$post = Mockery::mock( '\WP_Post' );
		$post = (object)[ 'ID' => '123123' ];
		$field = array(
			'value' => 'some_value'
		);
		
		$expected_return = array(
			'value' => '123123',
			'readonly' => true
		);
		
		// sut
		$contacts = new \Pei_Ri_Resources\Contacts();
		
		// Act
		$result = $contacts->ri_add_contact_id_and_make_it_readonly( $field );
		
		// Assertions
		$this->assertSame( $expected_return, $result );
	}
}