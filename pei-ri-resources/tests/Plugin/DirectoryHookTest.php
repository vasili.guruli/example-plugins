<?php
use Brain\Monkey\Functions;

class DirectoryHooksTest extends \Pei_Ri_Resources\TestCase {

	public function test_directory_shortcode_does_have_proper_hooks() {
		Functions\stubs(
			[
				'add_shortcode'  => 1,
				'get_option'     => 1,
			]
		);

		$directory =  new \Pei_Ri_Resources\Directory_Shortcode();
		$directory->init();



		$this->assertIsInt( has_action( 'init', [ $directory, 'register_assets' ] ),
			'Directory Scripts should be hooked into init hook' );

		$this->assertIsInt( has_action( 'wp_footer', [ $directory, 'render_assets' ] ),
			'Directory footer scripts should be hooked into wp_footer hook' );
	}

	public function test_init_hooks_doesnt_have_proper_actions_enqueues() {
		Functions\stubs(
			[
				'add_shortcode'  => 1,
				'get_option'     => 1,
			]
		);
		$directory =  new \Pei_Ri_Resources\Directory_Shortcode();
		$directory->init();

		$this->assertFalse( has_action( 'wp_enqueue_scripts', [ $directory, 'register_assets' ] ),
			'Directory scripts should not be enqueued with admin_enqueue_scripts hook' );

		$this->assertFalse( has_action( 'admin_enqueue_scripts', [ $directory, 'render_assets' ] ),
			'Directory Frontend scripts should not be enqueued with admin_enqueue_scripts hook' );

	}
}
