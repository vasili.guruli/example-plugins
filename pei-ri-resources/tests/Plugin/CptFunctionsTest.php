<?php
namespace Pei_Ri_Resources;
use Brain\Monkey\Functions;

class CptFunctionsTest extends TestCase {
	public $plugin_classes = [
		[
			'class' => 'Enqueues',
		],
		[
			'class' => 'Admin',
		],
		[
			'class' => 'Institutions',
		],
		[
			'class' => 'Contacts',
		],
		[
			'class' => 'Content',
		],
		[
			'class' => 'Taxonomies',
		],
	];
	public function test_init_cpt_setup() {

		Functions\stubs(
			[
				'get_option' => 1,
			]
		);
		
		foreach ( $this->plugin_classes as $classes => $class  ) {
			if ( isset( $classes ) ) {
				$namespace_class = __NAMESPACE__ . '\\' . $class['class'];
				if ( class_exists( $namespace_class ) ) {
					$called_class = new $namespace_class();
					if ( method_exists( $called_class, 'init' ) ) {
						$this->assertSame( null, $called_class->init() );
					}
				}
			}
		}


	}

	public function test_init_cpt_setup_with_no_init_methods() {
		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		foreach ( $this->plugin_classes as $classes => $class  ) {
			if ( isset( $classes ) ) {
				$namespace_class = __NAMESPACE__ . '\\' . $class['class'];
				if ( class_exists( $namespace_class ) ) {
					$called_class = new $namespace_class();
					if ( method_exists( $called_class, 'init' ) ) {
						$this->assertNotSame( 'init', $called_class->init() );
					}
				}
			}
		}
	}
}