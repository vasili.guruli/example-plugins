<?php
use Brain\Monkey\Functions;

class AlgoliaTest extends \Pei_Ri_Resources\TestCase {

    /**
     * Test if all needed actions and filters are added to init method
     */
    public function test_init_method() {
		$algolia = new \Pei_Ri_Resources\Algolia();

		Functions\expect( 'add_filter' )
			->once()
			->with( 'algolia_post_shared_attributes', [ $algolia, 'add_funds_attributes' ], 10, 2 );
		Functions\expect( 'add_filter' )
			->once()
			->with( 'algolia_post_print_editions_shared_attributes', [ $algolia, 'add_funds_attributes' ], 10, 2 );
		Functions\expect( 'add_filter' )
			->once()
			->with( 'algolia_searchable_post_shared_attributes', [ $algolia, 'add_funds_attributes' ], 10, 2 );

		$algolia->init();

		$this->assertTrue( true );
    }

    public function testAddFundsAttributes()
    {
        $post = (object) array('ID' => 123);

        Functions\when('get_post_type')
            ->justReturn('content');

		Functions\when('get_post_meta')
            ->justReturn('1');

        Functions\when('get_field')
            ->justReturn(array(
                'product_institution' => 456,
                'product_description' => 'Test Description',
				'content_description' => 'Test Description',
                'strategy' => (object) array('name' => 'Test strategy'),
                'region' => (object) array('name' => 'Test region'),
                'style' => 789,
                'asset_class' => 012,
                'asset_subclass' => 345,
                'content_type' => '',
                'content_type_slug' => ''
            ));

        Functions\when('get_the_post_thumbnail_url')
            ->justReturn('http://example.com/thumbnail.jpg');

        Functions\when('get_the_title')
            ->justReturn('Test institution');

        Functions\when('get_term_by')
            ->justReturn((object) array('name' => 'Test term'));

        $attributes = array();
        $class = new \Pei_Ri_Resources\Algolia();
        $result = $class->add_funds_attributes($attributes, $post);

        $expected = array(
            'institution_thumb' => 'http://example.com/thumbnail.jpg',
            'institution_name' => 'Test institution',
            'description' => 'Test Description',
            'strategy' => 'Test strategy',
            'region' => 'Test region',
            'style' => 'Test term',
            'asset_class' => 'Test term',
            'asset_subclass' => 'Test term',
            'content_type' => '',
            'content_type_slug' => ''
        );

        $this->assertEquals($expected, $result);
    }
}
