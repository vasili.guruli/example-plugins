<?php
use Brain\Monkey\Functions;

class ContentsHooksTest extends \Pei_Ri_Resources\TestCase {
	public function test_init_hooks_contents() {

		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$content = new \Pei_Ri_Resources\Content();
		$content->init();

		$this->assertTrue( has_action( 'init', $content->init() ),
			'Init should be hooked into init hook' );
		
		
		$expected_prior = 10;
		
		$result = has_filter( 'acf/prepare_field/name=related_content_id',
			array( $content, 'ri_related_content_add_post_id_to_acf_field_and_mark_this_field_as_read_only' ) );
		
		$result1 = has_filter( 'acf/fields/taxonomy/query/name=content_content_type', array( $content, 'ri_remove_fund_from_content_type_query' ) );

			// Assertions
		$this->assertSame( $expected_prior, $result );
		$this->assertSame( $expected_prior, $result1 );
	}

	public function test_init_hook_doesnt_have_proper_actions_contents() {
		Functions\stubs(
			[
				'get_option'           => 1,
			]
		);
		$content = new \Pei_Ri_Resources\Content();

		$this->assertFalse( has_action( 'wp_header', $content->init() ),
			'Post type does not have wp_header hook' );
	}

	// Test if ri_remove_fund_from_content_type_query returns unchanged args when get_fund_term_id is not int
	public function test_ri_remove_fund_from_content_type_query_returns_unchanged_args_when_get_fund_term_id_is_not_int() {
		// mock vars
		$term_name = 'some_term_name';
		$expected_output = array();
		$field = '';
		$post_id = 123;

		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( 'some_value' );

		// sut
		$sut = new \Pei_Ri_Resources\Content();

		// Act
		$result = $sut->ri_remove_fund_from_content_type_query( $expected_output, $field, $post_id );

		// Assertions
		$this->assertSame( $expected_output, $result );
	}

	// Test if ri_remove_fund_from_content_type_query returns exclude element in array args when get_fund_term_id is int
	public function test_ri_remove_fund_from_content_type_query_returns_exclude_args_when_get_fund_term_id_is_int() {
		// mock vars
		$term_name = 'some_term_name';
		$expected_output = array( 'exclude' => 321 );
		$field = '';
		$post_id = 123;

		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( 321 );

		// sut
		$sut = new \Pei_Ri_Resources\Content();

		// Act
		$result = $sut->ri_remove_fund_from_content_type_query( $expected_output, $field, $post_id );

		// Assertions
		$this->assertSame( $expected_output, $result );
	}

	// test if ri_get-term_id_by_name returning null when get_term_by is not object
	public function test_if_ri_get_term_id_by_name_returning_null_when_get_term_by_is_not_object() {

		// mock vars
		$term_name = 'some_term_name';

		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( 'some_value' );

		// sut
		$sut = new \Pei_Ri_Resources\Content();

		// Act
		$result = $sut->ri_get_term_id_by_name( $term_name );

		// Assertions
		$this->assertNull( $result );
	}

	// test if ri_get-term_id_by_name returning term_taxonomy_id when get_term_by is object
	public function test_if_ri_get_term_id_by_name_returning_term_taxonomy_id_when_get_term_by_is_object() {

		// mock vars
		$term_name = 'some_term_name';

		//mock methods
		Functions\expect( 'get_term_by' )
			->once()
			->andReturn( (object)['term_taxonomy_id' => 123] );

		// sut
		$sut = new \Pei_Ri_Resources\Content();

		// Act
		$result = $sut->ri_get_term_id_by_name( $term_name );

		// Assertions
		$this->assertSame( 123, $result );
	}
}