<?php
use Brain\Monkey\Functions;

class ContentListTest extends \Pei_Ri_Resources\TestCase {
	protected $post_type = 'institution';
	
	public function test_ri_institution_columns() {
		$defaults = array();
		
		$content = new \Pei_Ri_Resources\Institutions_List();
		
		$actual = $content->ri_institution_columns( $defaults );
		$defaults['organization_id'] = 'Organization Id';
		$defaults['title']           = 'Name';
		$defaults['sponsorship']     = 'Sponsorship';
		$defaults['starts_at']       = 'Starts At';
		$defaults['ends_at']         = 'Ends At';
		$defaults['company_type']    = 'Company Type';
		$defaults['author']          = 'Author';
		$defaults['date']            = 'Date';
		
		$this->assertEquals( $defaults, $actual );
	}
	
	public function test_ri_institution_columns_institution_organization_id() {
		$post_id = 12345;
		$column = 'organization_id';
		
		$expected = 'AGS Institution';
		Functions\when( 'esc_attr' )->justReturn( 'AGS Institution' );
		Functions\when( 'get_post_meta' )->justReturn( '1' );
		
		Functions\when( 'get_the_title' )->justReturn( 'AGS Institution' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		$content = new \Pei_Ri_Resources\Institutions_List();
		
		$this->expectOutputString( $expected );
		
		$content->ri_institution_columns_content( $column, $post_id );
	}
	
	public function test_ri_institution_columns_institution_institution_sponsorship() {
		$post_id = 12345;
		$column = 'sponsorship';
		
		$expected = 'Enhanced';
		Functions\when( 'esc_attr' )->justReturn( 'Enhanced' );
		Functions\when( 'get_post_meta' )->justReturn( 'Enhanced' );
		
		Functions\when( 'get_the_title' )->justReturn( 'AGS Institution' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		$content = new \Pei_Ri_Resources\Institutions_List();
		
		$this->expectOutputString( $expected );
		
		$content->ri_institution_columns_content( $column, $post_id );
	}
	
	public function test_ri_institution_columns_institution_institution_starts_at() {
		$post_id = 12345;
		$column = 'starts_at';
		
		$expected = '2023.01.01';
		
		Functions\when( 'esc_attr' )->justReturn( '2023.01.01' );
		Functions\when( 'get_post_meta' )->justReturn( '2023.01.01' );
		
		Functions\when( 'get_the_title' )->justReturn( '2023.01.01' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		$content = new \Pei_Ri_Resources\Institutions_List();
		
		$this->expectOutputString( $expected );
		
		$content->ri_institution_columns_content( $column, $post_id );
	}
	
	public function test_ri_institution_columns_institution_institution_ends_at() {
		$post_id = 12345;
		$column = 'starts_at';
		
		$expected = '2023.05.17';
		
		Functions\when( 'esc_attr' )->justReturn( '2023.05.17' );
		Functions\when( 'get_post_meta' )->justReturn( '2023.05.17' );
		
		Functions\when( 'get_the_title' )->justReturn( '2023.05.17' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		$content = new \Pei_Ri_Resources\Institutions_List();
		
		$this->expectOutputString( $expected );
		
		$content->ri_institution_columns_content( $column, $post_id );
	}
	
	public function test_ri_institution_columns_institution_institution_company_type() {
		$post_id = 12345;
		$column = 'company_type';
		
		
		$company_types = array(
			'company_1' => 'Test Company',
			'company_2' => 'Test Company 2',
		);
		
		$expected = 'Test Company';
		Functions\when( 'esc_attr' )->justReturn( 'Test Company' );
		Functions\when( 'get_field' )->justReturn( $company_types );
		Functions\when( 'get_post_meta' )->justReturn( 'Test Company' );
		
		Functions\when( 'get_the_title' )->justReturn( 'Test Company' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_term' )->justReturn(
			(object) array('name' => 'Test Company')
		);
		
		$content = new \Pei_Ri_Resources\Institutions_List();
		
		$this->expectOutputString( $expected );
		
		$content->ri_institution_columns_content( $column, $post_id );
	}
}