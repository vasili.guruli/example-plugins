<?php
use Brain\Monkey\Functions;

class ContactListTest extends \Pei_Ri_Resources\TestCase {
	protected $post_type = 'content';
	
	public function test_ri_contact_columns() {
		$defaults = array(
			'title' => 'Full Name',
			'organization' => 'Institution',
			'author' => 'Author',
			'date' => 'Date'
		);
		
		$contact = new \Pei_Ri_Resources\Contacts_List();
		
		$actual = $contact->ri_contact_columns( $defaults );
		
		
		$this->assertEquals( $defaults, $actual );
	}
	
	public function test_ri_contact_columns_content() {
		$column = 'organization';
		$post_id = 12345;
		$expected = '<a href="https://test-institution.test?action=edit&institution='.$post_id.'">AGS Institution</a>';
		
		Functions\when( 'get_post_meta' )->justReturn( 'AGS Institution' );
		Functions\when( 'esc_attr' )->justReturn( 'AGS Institution' );
		Functions\when( 'get_the_title' )->justReturn( 'AGS Institution' );
		
		Functions\when( 'esc_url' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		Functions\when( 'get_edit_post_link' )->justReturn( 'https://test-institution.test?action=edit&institution=' . $post_id );
		$contact = new \Pei_Ri_Resources\Contacts_List();
		
		$this->expectOutputString( $expected );
		
		$contact->ri_contact_columns_content( $column, $post_id );
	}
}