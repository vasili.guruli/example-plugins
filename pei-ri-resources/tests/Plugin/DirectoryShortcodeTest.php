<?php
use Brain\Monkey\Functions;

class DirectoryShortcodeTest extends \Pei_Ri_Resources\TestCase {

	public function test_directory_has_shortcode() {
		$shortcode_content = $this->get_shortcode_directory();

		$attributes = array(
			'shortcode' => $shortcode_content
		);

		$directory = new \Pei_Ri_Resources\Directory_Shortcode();

		Functions\stubs(
			[
				'get_option' => 1,
			]
		);

		Functions\when('add_shortcode')->justReturn(
			[
				'shortcode' => $shortcode_content
			]
		);


		$shortcode = $directory->directory_shortcode( $attributes );

		$this->assertSame($shortcode, $shortcode_content);
	}

	public function test_directory_has_not_shortcode() {
		$shortcode_content = "<div class='test-shortcode'><p>RI DIRECTORY SHORTCODE</p></div>";

		$attributes = array(
			'shortcode' => $shortcode_content
		);

		$directory = new \Pei_Ri_Resources\Directory_Shortcode();

		Functions\stubs(
			[
				'get_option' => 1,
			]
		);

		Functions\when('add_shortcode')->justReturn(
			[
				'shortcode' => $shortcode_content
			]
		);


		$shortcode = $directory->directory_shortcode( $attributes );

		$this->assertNotSame($shortcode, $shortcode_content);
	}

	public function get_shortcode_directory() {
		ob_start();

		include RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/public/directory.php';

		$html = ob_get_contents();

		ob_end_clean();

		return $html;
	}
}
