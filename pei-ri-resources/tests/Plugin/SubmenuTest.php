<?php
use Brain\Monkey\Functions;

class SubmenuItemsTest extends \Pei_Ri_Resources\TestCase {

    /**
     * Test if all needed actions and filters are added to init method
     */
    public function test_if_submenu_items_returning_all_submenu_items() {
        if ( ! defined( 'PAGE_NAME' ) ) {
            define( 'PAGE_NAME', 'some_page_name' );
        }
        
        // Expected output
        Functions\expect('__')->times(36);

        Functions\expect('apply_filters')->with('ri_resources_submenu_items')->once()->andReturn(false);

        // Sut
        $sut = new \Pei_Ri_Resources\Submenu_Items();
		$sut->submenu_items();

        // Act
        $result = has_filter( 'acf/prepare_field/name=fund_product_id', array( $sut, 'ri_fund_add_post_id_to_acf_field_and_mark_this_field_as_read_only' ) );

        // Assertions
		$this->assertFalse( $result );
    }

}
