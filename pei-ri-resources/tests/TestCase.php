<?php

namespace Pei_Ri_Resources;

use Brain\Monkey;

abstract class TestCase extends \PHPUnit\Framework\TestCase {

	protected function setUp(): void {

		parent::setUp();
		Monkey\setUp();
		// Require your plugin files that will be unit tested
		require_once dirname( __DIR__ ) . '/includes/classes/class-admin.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-enqueues.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-post-types.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-contacts.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-content.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-institutions.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-taxonomies.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-helper.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-acfconfig.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-frontend.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-directory-shortcode.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-contacts-metabox-for-institutions.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-funds.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-submenu-items.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-roles.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-addresses-metabox-for-contacts.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-ajax.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-ri-resource-templates.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-contents-template.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-institutions-template.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-algolia.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-content-shortcode.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-custom-list-table.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-contacts-list.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-content-list.php';
		require_once dirname( __DIR__ ) . '/includes/classes/class-institutions-list.php';

	}

	protected function tearDown(): void {

		Monkey\tearDown();
		parent::tearDown();
	}
}
