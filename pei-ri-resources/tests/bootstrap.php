<?php
if ( ! defined( 'ABSPATH' ) ) {
	define('ABSPATH', 'fake_path');
}

if ( ! defined( 'RI_RESOURCE_DB_SETTINGS_KEY' ) ) {
	define( 'RI_RESOURCE_DB_SETTINGS_KEY', '1234' );
}

if ( ! defined( 'RI_RESOURCE_INDEX_SETTINGS_PAGE' ) ) {
	define( 'RI_RESOURCE_INDEX_SETTINGS_PAGE', 'some_index_name' );
}

if ( ! defined( 'RI_RESOURCES_PLUGIN_DIR_PATH' ) ) {
	define( 'RI_RESOURCES_PLUGIN_DIR_PATH', __DIR__ . '/fake_path' );
}

if ( ! defined( 'RI_RESOURCES_PLUGIN_DIR_URL' ) ) {
	define( 'RI_RESOURCES_PLUGIN_DIR_URL', __DIR__ . '/fake_path' );
}

if ( ! defined( 'RI_RESOURCES_PLUGIN_CURRENT_VERSION' ) ) {
	define( 'RI_RESOURCES_PLUGIN_CURRENT_VERSION', '1234' );
}

if ( ! defined( 'RI_RESOURCES_PLUGIN_TEXT_DOMAIN' ) ) {
	define( 'RI_RESOURCES_PLUGIN_TEXT_DOMAIN', 'pei-ri-resources' );
}

require_once dirname( __DIR__ ) . '/vendor/autoload.php';

// The base test case class
require_once dirname( __DIR__ ) . '/tests/TestCase.php';

