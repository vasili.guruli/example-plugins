<?php
/**
 * @var \WP_Post $post
 */
$contacts = array(
	array(
		'ID' => 15,
		'full_name' => 'Jack Smith',
		'email' => 'jack@mail.com',
		'phone' => '123456789',
		'job_title' => 'SEO'
	),

		array(
		'ID' => 25,
		'full_name' => 'James Smith',
		'email' => 'james@mail.com',
		'phone' => '123456789',
		'job_title' => 'CTO'
	),
);
?>
	<div class="acf-field">
		<div class="acf-input">
			<table class="acf-table">
				<thead>
				<tr>
					<th><?php echo 'ID'; ?></th>
					<th><?php echo 'Name' ?></th>
					<th><?php echo 'Email' ?></th>
					<th><?php echo 'Phone' ?></th>
					<th><?php echo 'Job Title' ?></th>
				</tr>
				</thead>
				<tbody>
				<?php
				foreach ($contacts as $contact) : ?>
					<?php
					$contact_details = array(
						'ID' => $contact['ID'],
						'full_name' => $contact['full_name'],
						'email' => $contact['email'],
						'phone' => $contact['phone'],
						'job_title' => $contact['job_title'],
					);
					?>
					<tr class="acf-row">
						<td class="acf-field"><a
									href="#"><?php echo $contact_details['ID']; ?></a>
						</td>
						<td class="acf-field"><a
									href="#"><?php echo $contact_details['full_name']; ?></a>
						</td>
						<td class="acf-field"><?php echo $contact_details['email']; ?></td>
						<td class="acf-field"><?php echo $contact_details['phone']; ?></td>
						<td class="acf-field"><?php echo $contact_details['job_title']; ?></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
<?php
