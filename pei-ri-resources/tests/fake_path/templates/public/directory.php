<!-- BLAIZE_FEATURE ri-resource-view -->
<div id="theresource-directory">
	<div class="theresource-search" id="directorysearch"></div>
	<div class="theresource-letter-filter">
		<ul>
			<?php foreach ( range( 'a', 'z' ) as $letter ) : ?>
				<li><?php echo $letter; ?></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<div id="theresource-results">
		<main id="directoryhits"></main>
		<div id="directory-infinite-hits"></div>
		<div id="hits-per-page"></div>
		<div id="directory-pagination"></div>
	</div>
	<script type="text/html" id="tmpl-directory-hit">
		<div class="item-header">
			<h3 class="entry-title td-module-title directory-title" itemprop="name headline">
				<a id="directory-title" href="{{ data.permalink }}" title="{{ data.post_title }}" class="ais-hits--title-link ri-directory-title" itemprop="url">
					{{{ data._highlightResult.post_title.value }}}
				</a>
			</h3>
		</div>
	</script>

</div>
<!-- BLAIZE_FEATURE ri-resource-view -->
