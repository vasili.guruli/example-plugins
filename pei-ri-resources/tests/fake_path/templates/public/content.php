<!-- BLAIZE_FEATURE ri-resource-view -->
<div id="theresource-content">
	<div class="theresource-search" id="contentsearch"></div>
	<div id="theresource-results">
		<main id="contenthits"></main>
		<div id="content-infinite-hits"></div>
		<div id="hits-per-page"></div>
		<div id="content-pagination"></div>
	</div>
	<script type="text/html" id="tmpl-content-hit">
		<div class="item-header">
			<# if ( data.images.full ) { #>
			<div class="ais-hits--thumbnail">
				<# if ( data.images.full.url ) { #>
				<a href="{{ data.permalink }}" title="{{ data.post_title }}" class="ais-hits--thumbnail-link">
					<img src="{{ data.images.full.url }}" alt="{{ data.post_title }}" title="{{ data.post_title }}" itemprop="image" />
				</a>
				<# } #>
			</div>
			<# }else{ #>
			<div class="ri-no-image-placeholder"></div>
			<# } #>
			<h3 class="entry-title td-module-title content-title" itemprop="name headline">
				<a id="content-title" href="{{ data.permalink }}" title="{{ data.post_title }}" class="ais-hits--title-link ri-content-title" itemprop="url">
					{{ data.post_title }}
				</a>
			</h3>
		</div>
	</script>

</div>
<!-- BLAIZE_FEATURE ri-resource-view -->