<?php
/**
 *  Plugin Name: PEI RI Resources
 *  Description: RI Resources for Responsible Investor
 *  Version:     1.0.0
 *  Author:      PEI Media
 *  Text Domain: pei-ri-resources
 **/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! defined( 'RI_RESOURCES_PLUGIN_DIR_PATH' ) ) {
	define( 'RI_RESOURCES_PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'RI_RESOURCES_PLUGIN_DIR_URL' ) ) {
	define( 'RI_RESOURCES_PLUGIN_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'RI_RESOURCES_PLUGIN_CURRENT_VERSION' ) ) {
	define( 'RI_RESOURCES_PLUGIN_CURRENT_VERSION', '1.0.0.2' );
}

if ( ! defined( 'RI_RESOURCES_PLUGIN_TEXT_DOMAIN' ) ) {
	define( 'RI_RESOURCES_PLUGIN_TEXT_DOMAIN', 'pei-ri-resources' );
}

if ( ! defined( 'RI_RESOURCE_DB_SETTINGS_KEY' ) ) {
	define( 'RI_RESOURCE_DB_SETTINGS_KEY', 'ri-the-resource-settings' );
}

if ( ! defined( 'RI_RESOURCE_INDEX_SETTINGS_PAGE' ) ) {
	define( 'RI_RESOURCE_INDEX_SETTINGS_PAGE', 'ri-resource-indexes-settings-page' );
}

require_once RI_RESOURCES_PLUGIN_DIR_PATH . 'includes/autoloader.php';

/**
 * Run the plugin.
 *
 * @return void
 */
function ri_resource_bootstrap() {
	$ri_resources_cpt_setup = new \Pei_Ri_Resources\Ri_Resources_CPT_Setup();
	$ri_resources_cpt_setup->run();

	$ri_resources_custom_roles = new \Pei_Ri_Resources\Roles();
	$ri_resources_custom_roles->init();

	$ri_resources_frontend = new \Pei_Ri_Resources\Frontend();
	$ri_resources_frontend->init();

	$ri_acf_custom_settings = new \Pei_Ri_Resources\ACFConfig();
	$ri_acf_custom_settings->init();

	$ri_funds = new \Pei_Ri_Resources\Funds();
	$ri_funds->init();

	$ri_algolia = new \Pei_Ri_Resources\Algolia();
	$ri_algolia->init();
}

add_action( 'plugins_loaded', 'ri_resource_bootstrap', 10 );
