<?php
/**
 * Autoload files for classes when needed
 *
 * @param string $class_name Name of the class being requested
 *
 * @since  1.0.0
 */
function ri_class_file_autoloader( string $class_name ) {

	/**
	 * If the class being requested does not include namespace => Pei_Ri_Resources, stop it.
	 */
	if ( false === strpos( $class_name, 'Pei_Ri_Resources' ) ) {
		return;
	}
	// Split the class name into an array to read the namespace and class.
	// for example { 0 => Pei_Ri_Resources, 1=> Admin, 2=> Admin }
	$file_parts = explode( '\\', $class_name );
	$class_path = 'includes/classes';
	// Remove namespace
	$founded_class = count( $file_parts ) - 1;
	$file_name     = '';
	for ( $i = $founded_class; $i > 0; $i -- ) {

		// Read the current component of the file part.
		$current_class = strtolower( $file_parts[ $i ] );
		$current_class = str_ireplace( '_', '-', $current_class );

		// If we're at the first entry, then we're at the filename.
		if ( $founded_class === $i ) {
			$file_name = "class-{$current_class}.php";
		}
	}

	// Path to class
	$file = RI_RESOURCES_PLUGIN_DIR_PATH . "{$class_path}/" . $file_name;

	if ( file_exists( $file ) ) {
		require( $file );
	} else {
		wp_die(
			esc_html( "The file {$file} attempting to be loaded but {$class_name} does not exist." )
		);
	}
}

spl_autoload_register( 'ri_class_file_autoloader' );