<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Enqueue Scripts and Styles
 */
class Enqueues {

	/**
	 * Fund Term name
	 */
	public $fund_term_name = 'Fund';

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'admin_enqueue_scripts', [ $this, 'register_ri_resources_admin_css' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'register_ri_resources_admin_script' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_ri_resources_frontend_css' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_ri_resources_frontend_rtabs' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'register_ri_resources_required_post_title' ] );
		//Scripts for ajax call
		add_action( 'admin_enqueue_scripts', [ $this, 'register_ri_resources_ajax_script' ] );

		// Disabled but not disabled
		add_action( 'admin_enqueue_scripts', [ $this, 'register_disabled_but_not_disabled' ] );
	}

	/**
	 * Register admin style for RI Resources.
	 *
	 * @return void
	 */
	public function register_ri_resources_admin_css() {
		wp_register_style( 'ri_wp_admin_css', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/css/admin-style.css', [], RI_RESOURCES_PLUGIN_CURRENT_VERSION );
		wp_enqueue_style( 'ri_wp_admin_css' );
	}

	/**
	 * Register frontend style for RI Resources.
	 *
	 * @return void
	 */
	public function register_ri_resources_frontend_css() {
		wp_register_style( 'ri_wp_frontend_css', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/css/style.css', [], RI_RESOURCES_PLUGIN_CURRENT_VERSION );
		wp_enqueue_style( 'ri_wp_frontend_css' );
	}

	/**
	 * Register admin scripts for RI Resources.
	 *
	 * @return void
	 */
	public function register_ri_resources_admin_script() {
		wp_register_script( 'ri_wp_admin_script', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/admin-script.js', [ 'jquery' ], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true );
		wp_enqueue_script( 'ri_wp_admin_script' );
	}

	/**
	 * Register frontend resource tabs scripts
	 *
	 * @return void
	 */
	public function register_ri_resources_frontend_rtabs() {
		wp_register_script( 'ri_wp_frontend_rtabs_script', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/resource-tabs.js', [], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true );
		wp_enqueue_script( 'ri_wp_frontend_rtabs_script' );
	}

	/**
	 * Register required post title for content post type
	 */
	public function register_ri_resources_required_post_title() {
		$screen = get_current_screen();

		if ( 'content' === $screen->post_type && 'post' === $screen->base ) {
			wp_register_script( 'ri_wp_required_post_title', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/ri-required-post-title.js', [ 'jquery' ], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true );
			wp_enqueue_script( 'ri_wp_required_post_title' );
		}
	}

	/**
	 * Register ajax call scripts for RI Resources.
	 *
	 * @return void
	 */
	public function register_ri_resources_ajax_script() {
		wp_register_script( 'ri_wp_ajax_script', RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/admin-ajax-script.js', [ 'jquery' ], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true );
		wp_enqueue_script( 'ri_wp_ajax_script' );

		wp_localize_script(
			'ri_wp_ajax_script', 'ri_resource', [
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'nonce'    => wp_create_nonce( 'ri-addresses-nonce' ),
			]
		);
	}

	/**
	 * Register disabled but not disabled
	 */
	public function register_disabled_but_not_disabled() {
		$screen = get_current_screen();

		if ( 'content' === $screen->post_type && 'post' === $screen->base ) {
			wp_register_script( 'ri_wp_disabled_but_not_disabled', RI_RESOURCES_PLUGIN_DIR_URL . 'assets/src/js/admin-mark-select-as-disabled-but-not-disabled.js', [ 'jquery' ], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true );
			wp_enqueue_script( 'ri_wp_disabled_but_not_disabled' );

			wp_register_script( 'admin-content-fund-select-term-on-meta-box', RI_RESOURCES_PLUGIN_DIR_URL . 'assets/src/js/admin-content-fund-select-term-on-meta-box.js', [ 'jquery' ], RI_RESOURCES_PLUGIN_CURRENT_VERSION, true );
			wp_enqueue_script( 'admin-content-fund-select-term-on-meta-box' );
			$get_fund_term_id = $this->ri_get_term_id_by_name( $this->fund_term_name );

			if( is_int( $get_fund_term_id ) ){
				wp_localize_script(
					'admin-content-fund-select-term-on-meta-box', 'taxonomy_data', [
						'fund_id' => $get_fund_term_id,
					]
				);
			}
			
		}
	}

	/**
	 * Get term id by name
	 */
	public function ri_get_term_id_by_name( $term_name ) {
		$term = get_term_by( 'name', $term_name, 'content-type' );

		if ( ! is_object( $term ) ) {
			return null;
		}

		return $term->term_taxonomy_id;
	}
}
