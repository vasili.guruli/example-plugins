<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Class Ajax
 */
class Ajax {
	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'wp_ajax_ri_ajax_addresses_action', array( $this, 'ri_ajax_addresses_action' ) );
	}

	/**
	 * Ajax actions.
	 *
	 * @return false|string|void
	 */
	public function ri_ajax_addresses_action() {
		if ( isset( $_POST['ri_nonce'] ) ) {

			if ( ! wp_verify_nonce( sanitize_text_field( $_POST['ri_nonce'] ), 'ri-addresses-nonce' ) ) {
				return;
			}
			if ( isset( $_POST['institution_id'] ) ) {
				$institution_id = $_POST['institution_id'];

				$primary_address    = get_field( 'field_63a462bad982d', (int) $institution_id );
				$additional_address = get_field( 'additional_address', (int) $institution_id );
				$sorted_addresses   = array();
				$i = 1;
				foreach ($additional_address as $address ) {
					if ( isset( $address['country'] ) ) {
						$key = $address['country']['value'];
						// Allow duplicate countries keys e.g PL|PL
						$sorted_addresses[ $key . '_' . $i ] = $address;
						$i++;
					}
				}
				ksort($sorted_addresses);
				$addresses = array(
					'institution'        => get_the_title( $institution_id ),
					'institution_url'    => esc_url( get_edit_post_link( $institution_id ) ),
					'primary_address'    => $primary_address,
					'additional_address' => $sorted_addresses,
				);

				echo json_encode( $addresses );
			}
		}

		wp_die();
	}
}
