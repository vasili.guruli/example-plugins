<?php
namespace Pei_Ri_Resources;

use Pei_Ri_Resources\Post_Types;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Contacts Metabox On Institution Edit Page
 */
class Addresses_Metabox_For_Contacts  {
	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'save_post', array( $this, 'save_contact_to_institution' ) );
		add_action( 'save_post', array( $this, 'save_address_for_contact' ) );
		add_action( 'add_meta_boxes', array( $this, 'contacts_addresses_metabox' ) );
	}
	
	/**
	 * Register Metabox to display Addresses Related to contacts.
	 *
	 * @return void
	 */
	public function contacts_addresses_metabox() {
		add_meta_box( 'contacts-addresses',
			__( 'Addresses', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			array( $this, 'add_meta_box_for_contacts' ),
			'contact'
		);
	}
	
	/**
	 * Add Metabox to display Contacts related addresses.
	 *
	 * @param \WP_Post $post
	 *
	 * @return void
	 */
	public function add_meta_box_for_contacts( \WP_Post $post ) {
		include RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/admin/contact-metabox.php';
	}
	
	/**
	 * Save Contact to Institution contacts meta
	 *
	 * @param int $post_id
	 *
	 * @return void
	 */
	public function save_contact_to_institution( int $post_id ) {
		
		if ( 'contact' === get_post_type( $post_id ) ) {
			if ( ! current_user_can( 'manage_options' ) && ! is_admin() ) {
				return;
			}
			
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return;
			}
			
			$institution = get_post_meta( $post_id, 'contact_institution', true );
			
			if ( '' !== $institution && isset( $institution ) ) {
				$contacts = get_post_meta( (int) $institution, 'contacts', true );
				
				if ( in_array( $post_id, $contacts ) ){
					return;
				}
			
				if ( ! is_array( $contacts ) ) {
					$contacts = array();
				}
				$contacts[] = $post_id;
				
				update_post_meta( $institution, 'contacts', $contacts );
			}
		}
	}
	
	/**
	 * Save Selected Institution Address for Contact.
	 *
	 * @param int $post_id
	 *
	 * @return void
	 */
	public function save_address_for_contact( int $post_id ) {
		if ( 'contact' === get_post_type( $post_id ) ) {
			if ( ! current_user_can( 'manage_options' ) && ! is_admin() ) {
				return;
			}
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return;
			}
			if( isset( $_POST['ri_contact_address'] ) ) {
				$ri_contact_address = sanitize_text_field( str_replace( "\\", "",  $_POST['ri_contact_address'] ) );
				update_post_meta( $post_id, 'ri_contact_address', $ri_contact_address );
			}
		}
	}
}
