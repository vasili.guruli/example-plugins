<?php

namespace Pei_Ri_Resources;

use Automattic\WooCommerce\Admin\Composer\Package;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Ri_Resource_Templates class
 */
abstract class Ri_Resource_Templates {

	/**
	 * @var string $post_type
	 */
	protected $post_type;

	/**
	 * @var string $template_name
	 */
	protected $template_name;

	/**
	 * @var string $directory
	 */
	protected $directory = 'public';

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	abstract public function init();

	/**
	 * Load Appropriate Template
	 *
	 * @param string $template
	 *
	 * @return string
	 */
	public function load_appropriate_template( string $template ) : string {
		global $post;
		$template_file = RI_RESOURCES_PLUGIN_DIR_PATH . "templates/$this->directory/$this->template_name.php";
		if ( $this->post_type === $post->post_type ) {
			if ( $theme_file = locate_template( [ "$this->template_name.php" ] ) ) {
				$template = $theme_file;
			} else{
				if ( file_exists( $template_file ) ) {
					$template = $template_file;
				}
			}
		}

		return $template;
	}
}
