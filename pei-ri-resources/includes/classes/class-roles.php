<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Custom roles class
 */
class Roles {


	/**
	 *  @var string $managing_role
	 */
	protected $managing_role = 'resource_manager';

	/**
	 *  @var string $viewer_role
	 */
	protected $viewer_role = 'resource_viewer';

	/**
	 *  @var string $managing_role_name
	 */
	protected $managing_role_name = 'Resource Manager';

	/**
	 *  @var string $viewer_role_name
	 */
	protected $viewer_role_name = 'Resource Viewer';

	/**
	 * @var array $custom_capabilities_names
	 */
	protected $custom_capabilities_names = array(
		'contact'     => array(
			'singular' => 'contact',
			'plural'   => 'contacts',
		),
		'content'     => array(
			'singular' => 'content',
			'plural'   => 'contents',
		),
		'institution' => array(
			'singular' => 'institution',
			'plural'   => 'institutions',
		),
	);

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		$this->register_roles();
		$this->assign_cap_for_managers();
		$this->assign_cap_for_viewers();
		
		add_action( 'admin_menu', [ $this, 'remove_admin_pages' ], 99999 );
	}

	/**
	 * Assign capabilities for managers
	 *
	 * @return void
	 */
	public function assign_cap_for_managers() {
		$admins = array( $this->managing_role, 'administrator' );

		foreach ( $admins as $the_role ) {
			$admin = get_role( $the_role );

			foreach ( $this->custom_capabilities_names as $capability ) {
				$admin->add_cap( 'resource_edit_' . $capability['singular'] );
				$admin->add_cap( 'resource_edit_' . $capability['plural'] );
				$admin->add_cap( 'resource_edit_other_' . $capability['plural'] );
				$admin->add_cap( 'resource_publish_' . $capability['plural'] );
				$admin->add_cap( 'resource_read_' . $capability['singular'] );
				$admin->add_cap( 'resource_read_private_' . $capability['plural'] );
				$admin->add_cap( 'resource_delete_' . $capability['singular'] );
				$admin->add_cap( 'resource_create_posts_' . $capability['plural'] );
				$admin->add_cap( 'resource_delete_others_posts_' . $capability['plural'] );
				$admin->add_cap( 'resource_edit_published_posts_' . $capability['plural'] );
				$admin->add_cap( 'resource_delete_published_posts_' . $capability['plural'] );
				$admin->add_cap( 'resource_edit_private_posts_' . $capability['plural'] );
				$admin->add_cap( 'resource_delete_private_posts_' . $capability['plural'] );
			}
			$admin->add_cap( 'ri_resource_manage' );
			$admin->add_cap( 'ri_resource_view' );
			$admin->add_cap( 'ri_resource_manage_terms' );
			$admin->add_cap( 'ri_resource_edit_terms');
			$admin->add_cap( 'ri_resource_delete_terms');
			$admin->add_cap( 'ri_resource_assign_terms');
		}
	}

	/**
	 * Assign capabilities for viewers
	 *
	 * @return void
	 */
	public function assign_cap_for_viewers() {
		$viewer = get_role( $this->viewer_role );

		$viewer->add_cap( 'ri_resource_view' );
		$viewer->add_cap( 'edit_posts' );
		$viewer->add_cap( 'delete_posts' );
		
		foreach ( $this->custom_capabilities_names as $capability ) {
			$viewer->add_cap( 'read_' . $capability['singular'] );
			$viewer->add_cap( 'resource_edit_' . $capability['singular'] );
			$viewer->add_cap( 'resource_edit_' . $capability['plural'] );
		}
	}

	/**
	 * Register custom roles
	 *
	 * @return void
	 */
	public function register_roles() {
		add_role(
			$this->managing_role,
			$this->managing_role_name,
			array(
				'read'  => true,
				'delete_posts' => true,
				'edit_posts' => true
			)
		);

		add_role(
			$this->viewer_role,
			$this->viewer_role_name,
			array(
				'read'  => true,
				'delete_posts' => true,
				'edit_posts' => true
			)
		);		
	}

	/**
	 * Remove admin pages
	 *
	 * @return void
	 */
	public function remove_admin_pages() {
		global $current_user;

		$role_list = array($this->managing_role, $this->viewer_role);
		$user_have_roles = array_intersect( $role_list, $current_user->roles );

		if(0 < count($user_have_roles) && count($user_have_roles) == count($current_user->roles)  ) {
			remove_menu_page('edit.php'); // Posts
			remove_menu_page('upload.php'); // Media
			remove_menu_page('link-manager.php'); // Links
			remove_menu_page('edit-comments.php'); // Comments
			remove_menu_page('edit.php?post_type=page'); // Pages
			remove_menu_page('plugins.php'); // Plugins
			remove_menu_page('themes.php'); // Appearance
			remove_menu_page('users.php'); // Users
			remove_menu_page('tools.php'); // Tools
			remove_menu_page('options-general.php'); // Settings
			remove_menu_page('edit.php?post_type=events');
			remove_menu_page('edit.php?post_type=pwl_custom_feed');
			remove_menu_page('edit.php?post_type=print_editions');
			remove_menu_page('edit.php?post_type=shareable_list');
			remove_menu_page('delighted_api');
			remove_menu_page('vc-welcome');
			remove_menu_page('visualizer');
		}
	}


}
