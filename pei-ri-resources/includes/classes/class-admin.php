<?php
namespace Pei_Ri_Resources;

use Pei_Ri_Resources\Submenu_Items;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Plugin admin page
 */
class Admin {
	/**
	 * User must have this capability to use this page.
	 *
	 * @var string
	 */
	const CAPABILITY = 'ri_resource_view';

	/**
	 * Submenu Items for custom post type.
	 *
	 * @var array $sub_menu_items
	 */
	public $sub_menu_items = array();
	/**
	 * Admin page name
	 *
	 * @var string
	 */
	const PAGE_NAME = 'pei_ri_resources';

	/**
	 * @var array $settings
	 *
	 * Settings for RI Resource Index (Could be modified)
	 */
	private $settings = array(
		'directory' => array(
			'name'   => 'Directory',
			'fields' => array(
				'algolia_directory_index' => 'Directory - index name',
			),
		),
		'content'   => array(
			'name'   => 'Content',
			'fields' => array(
				'algolia_content_index' => 'Content - index name',
			),
		),
		'funds'     => array(
			'name'   => 'Funds',
			'fields' => array(
				'algolia_funds_index' => 'Funds - index name',
			),
		),
		'stapi'     => array(
			'name'   => 'Sailthru API',
			'fields' => array(
				'sailthru_api_url' => 'Sailthru API URL',
			),
		),
	);

	/**
	 * @var array $current_settings
	 */
	private $current_settings = array();


	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'admin_menu', array( $this, 'register_menu_page' ), 9 );
		add_action( 'admin_menu', array( $this, 'add_menu_items_for_custom_post_types' ) );
		
		// Register Settings for RI Resource Index
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		$this->currentSettings = get_option( RI_RESOURCE_DB_SETTINGS_KEY );
	}

	/**
	 * Admin menu actions
	 *
	 * Adds options page to admin menu.
	 *
	 * @return void
	 */
	public function register_menu_page() {
		add_menu_page(
			'',
			__( sprintf( '%s', '<b class="ri-resources-admin-menu" ">The Resource</b>' ) ),
			self::CAPABILITY,
			self::PAGE_NAME,
			'',
			'dashicons-admin-generic',
			4
		);
	}

	/**
	 * Add Menu items for custom post types.
	 *
	 * @return void
	 */
	public function add_menu_items_for_custom_post_types() {
		$this->sub_menu_items = new Submenu_Items();
		$callback             = '';
		if ( ! empty( $this->sub_menu_items ) ) {
			foreach ( $this->sub_menu_items->submenu_items() as $menu_item => $item ) {
				if ( RI_RESOURCE_INDEX_SETTINGS_PAGE === $item['menu_slug'] ) {
					$callback = array( $this, $item['callback'] );
				}
				if ( isset( $menu_item ) ) {
					add_submenu_page(
						$item['parent_slug'],
						$item['page_title'],
						$item['menu_title'],
						$item['capability'],
						$item['menu_slug'],
						$callback,
					);
				}
			}
		}
	}

	/**
	 * Displays settings form.
	 *
	 * @return void
	 */
	public function ri_resource_index_settings() {
		?>
		<h2><?php esc_attr_e( 'Settings', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ); ?></h2>
		<form action="options.php" method="post">
			<?php
			settings_fields( RI_RESOURCE_DB_SETTINGS_KEY );
			foreach ( $this->settings as $section => $setting ) {
				do_settings_sections( 'ri_the_resource_' . $section );
			}
			?>
			<input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ); ?>" />
		</form>
		<?php
	}

	/**
	 * Register settings using WordPress Settings API
	 *
	 * @see https://developer.wordpress.org/plugins/settings/settings-api/
	 *
	 * @return void
	 */
	public function register_settings() {
		register_setting( RI_RESOURCE_DB_SETTINGS_KEY, RI_RESOURCE_DB_SETTINGS_KEY );

		foreach ( $this->settings as $section => $setting ) {
			add_settings_section( $section, $setting->name, false, 'ri_the_resource_' . $section );

			foreach ( $setting['fields'] as $key => $title ) {
				add_settings_field(
					$key,
					$title,
					array( $this, 'field_' . $key ),
					'ri_the_resource_' . $section,
					$section
				);
			}
		}
	}

	/* === Directory === */
	/**
	 * Display Enable Profiles Search field
	 *
	 * @return void
	 */
	public function field_algolia_directory_index() {
		$value = $this->currentSettings['algolia_directory_index'] ?? '';
		?>
		<input type="text" name="<?php echo RI_RESOURCE_DB_SETTINGS_KEY; ?>[algolia_directory_index]" value="<?php echo $value; ?>" style="width:400px;">
		<?php
	}
	/* === Directory END === */

	/* === Content === */
	/**
	 * Display Enable Profiles Search field
	 *
	 * @return void
	 */
	public function field_algolia_content_index() {
		$value = $this->currentSettings['algolia_content_index'] ?? '';
		?>
		<input type="text" name="<?php echo RI_RESOURCE_DB_SETTINGS_KEY; ?>[algolia_content_index]" value="<?php echo $value; ?>" style="width:400px;">
		<?php
	}
	/* === Content END === */

	/* === Funds === */
	/**
	 * Display Enable Profiles Search field
	 *
	 * @return void
	 */
	public function field_algolia_funds_index() {
		$value = $this->currentSettings['algolia_funds_index'] ?? '';
		?>
		<input type="text" name="<?php echo RI_RESOURCE_DB_SETTINGS_KEY; ?>[algolia_funds_index]" value="<?php echo $value; ?>" style="width:400px;">
		<?php
	}
	/* === Funds END === */


	/**
	 * Displays sailthru api url field
	 *
	 * @return void
	 */
	public function field_sailthru_api_url() {
		$value = $this->currentSettings['sailthru_api_url'] ?? '';
		?>
		<input type="text" name="<?php echo RI_RESOURCE_DB_SETTINGS_KEY; ?>[sailthru_api_url]" value="<?php echo $value; ?>" style="width:400px;">
		<?php
	}
	/* === Funds END === */
}
