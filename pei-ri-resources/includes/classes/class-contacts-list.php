<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Contacts List Class to provide additional info on Contacts list table.
 */
class Contacts_List extends Custom_List_Table {
	/**
	 * @var $post_type
	 */
	protected $post_type = 'contact';

	/**
	 * Contacts Columns Data.
	 *
	 * @param array $defaults
	 *
	 * @return array
	 */
	public function ri_contact_columns( array $defaults ) {
		unset( $defaults['comments'] );
		unset( $defaults['author'] );
		unset( $defaults['date'] );
		unset( $defaults['title'] );
		
		$defaults['title']        = 'Full Name';
		$defaults['organization'] = 'Institution';
		$defaults['author']       = 'Author';
		$defaults['date']         = 'Date';
		
		return $defaults;
	}
	
	/**
	 * Display Custom Content on Contacts table list.
	 *
	 * @param string $column
	 * @param int $post_id
	 */
	public function ri_contact_columns_content( string $column, int $post_id ) {
		if ( 'organization' === $column ) {
			$contact_institution = get_post_meta( esc_attr( $post_id ), 'contact_institution', true );
			$institution_name    = get_the_title( (int) $contact_institution );
			
			echo '<a href="' . esc_url( get_edit_post_link( $contact_institution ) ) . '">' . esc_attr( $institution_name ) . '</a>';
		}
	}
}
