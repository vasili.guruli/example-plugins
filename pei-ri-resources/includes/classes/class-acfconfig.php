<?php
/**
 * Class Acf Config
 */
namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Class Acf Config
 */
class ACFConfig {

	/**
	 * Init method
	 */
	public function init() {
		add_filter( 'acf/settings/save_json', array( $this, 'pei_ri_change_acf_json_path' ) );
		add_filter( 'acf/settings/load_json', array( $this, 'pei_acf_json_load_point' ) );
		add_filter( 'acf/prepare_field/key=field_634d6b77b4fa0', array( $this, 'mark_select_field_as_disabled_but_not_disabled' ), 10, 1 ); // Product Content Type
	
	}

	/**
	 * Change path where ACF Groups are saved.
	 *
	 * @param string $path - path to store acf jsons
	 *
	 * @return string $path - modified path to store acf jsons
	 */
	public function pei_ri_change_acf_json_path( $path ) {
		// Update path
		$path = RI_RESOURCES_PLUGIN_DIR_PATH . '/acf-json'; 

		// Return path
		return $path;
	}

	public function pei_acf_json_load_point( $paths ) {
		
		// remove original path (optional)
		unset($paths[0]);
		
		
		// append path
		$paths[] = RI_RESOURCES_PLUGIN_DIR_PATH . '/acf-json'; 
		
		
		// return
		return $paths;
		
	}

	/**
	 * Mark select field as disabled but not disabled
	 *
	 * @param array $field - field from ACF
	 *
	 * @return array $field - modified field from ACF
	 */
	public function mark_select_field_as_disabled_but_not_disabled( $field ) {
		$field['readonly'] = 1;
		$field['disabled'] = 0;

		return $field;
	}
}
