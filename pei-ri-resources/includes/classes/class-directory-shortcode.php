<?php

namespace Pei_Ri_Resources;

/**
 * Class that is responsible for configuration of Algolia implementation
 */
class Directory_Shortcode {

	private $shortcode_displayed = false;
	private $settings = [];

	/**
	 * Init function that contains all hooks
	 */
	public function init() {
		add_shortcode( 'resource-directory', [ $this, 'directory_shortcode' ] );
		add_action('init', [ $this, 'register_assets' ] );
		add_action('wp_footer', [ $this, 'render_assets' ] );

		$this->settings = get_option( RI_RESOURCE_DB_SETTINGS_KEY );
	}

	public function directory_shortcode( $attributes ) {

		ob_start();

		include RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/public/directory.php';

		$this->shortcode_displayed = true;

		$html = ob_get_contents();

		ob_end_clean();

		return $html;
	}

	public function register_assets() {
		wp_enqueue_script(
			'the-resource-directory-instantsearch',
			RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/directory-instantsearch.js',
			[ 'algolia-instantsearch' ],
			RI_RESOURCES_PLUGIN_CURRENT_VERSION,
			true
		);
		wp_localize_script(
			'the-resource-directory-instantsearch',
			'ri_the_resource_directory',
			[
				'index_name' => $this->settings['algolia_directory_index'] ?? ''
			]
		);
	}

	public function render_assets() {
		if ( ! $this->shortcode_displayed ) {
			return;
		}

		wp_print_scripts('directory-instantsearch');
	}

	public function get_shortcode_displayed() {
		return $this->shortcode_displayed;
	}
}
