<?php

namespace Pei_Ri_Resources;

/**
 * Class that is responsible for configuration of Algolia implementation
 */
class Funds_Shortcode {

	private $shortcode_displayed = false;

	private $facets = array(
		array(
			'slug'  => 'region',
			'title' => 'Region',
		),
		array(
			'slug'  => 'strategy',
			'title' => 'Strategy',
		),
		array(
			'slug'  => 'style',
			'title' => 'Style',
		),
		array(
			'slug'  => 'assetclass',
			'title' => 'Asset Class',
		),
		array(
			'slug'  => 'assetsubclass',
			'title' => 'Asset Subclass',
		),
	);

	private $settings = array();

	/**
	 * Init function that contains all hooks
	 */
	public function init() {
		add_shortcode( 'resource-funds', array( $this, 'funds_shortcode' ) );
		add_action( 'init', array( $this, 'register_assets' ) );
		add_action( 'wp_footer', array( $this, 'render_assets' ) );

		$this->settings = get_option( RI_RESOURCE_DB_SETTINGS_KEY );
	}

	public function funds_shortcode( $attributes ) {
		ob_start();

		include RI_RESOURCES_PLUGIN_DIR_PATH . 'templates/public/funds.php';

		$this->shortcode_displayed = true;

		$html = ob_get_contents();

		ob_end_clean();

		return $html;
	}

	public function register_assets() {
		wp_enqueue_script(
			'the-resource-funds-instantsearch',
			RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/funds-instantsearch.js',
			array( 'algolia-instantsearch' ),
			RI_RESOURCES_PLUGIN_CURRENT_VERSION,
			true
		);
		wp_localize_script(
			'the-resource-funds-instantsearch',
			'ri_the_resource_funds',
			array(
				'index_name' => $this->settings['algolia_funds_index'] ?? '',
			)
		);
	}

	public function render_assets() {
		if ( ! $this->shortcode_displayed ) {
			return;
		}

		wp_print_scripts( 'funds-instantsearch' );
	}

	public function get_shortcode_displayed() {
		return $this->shortcode_displayed;
	}
}
