<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

use \Pei_Ri_Resources\Helper;

/**
 * Add fields to algolia
 */
class Algolia {

	/**
	 * @var $description_length
	 */
	private $description_length = 150;

	/**
	 * Init function that contains all hooks
	 */
	public function init() {
		add_filter( 'algolia_post_shared_attributes', array( $this, 'add_funds_attributes' ), 10, 2 );
		add_filter( 'algolia_post_print_editions_shared_attributes', array( $this, 'add_funds_attributes' ), 10, 2 );
		add_filter( 'algolia_searchable_post_shared_attributes', array( $this, 'add_funds_attributes' ), 10, 2 );
	}

	/**
	 * Adding new fields for algolia
	 *
	 * @param $attributes
	 * @param $post
	 *
	 * @return mixed
	 */
	public function add_funds_attributes( $attributes, $post ) {
		$post_type = get_post_type( $post->ID );
		if ( 'content' == $post_type ) {
			// Get Group
			$ri_product = get_field( 'field_634d726c15374', $post->ID );
			$description    = $ri_product['content_description'];
			$is_product = get_post_meta( $post->ID, 'is_this_product', true );
			$content_type       = 'field_634d72cc15375';

			if ( '1' === $is_product ) {
				$content_type = 'field_634d6b77b4fa0';
				$ri_product = get_field( 'field_634d6b4fb4f9f', $post->ID );
				$institution_id = ( $ri_product['product_institution'] ?? '' );
				$description = $ri_product[ 'product_description' ];
				$attributes['institution_thumb'] = get_the_post_thumbnail_url( $institution_id, 'post-thumbnail' );
				$attributes['institution_name'] = get_the_title( $institution_id );
			}

			if ( strlen( $description ) > $this->description_length ) {
				$description = substr( $description, 0, $this->description_length ) . '...';
			}

			$content_type        = get_field( $content_type, $post->ID );
			// Full name
			$content_type_name   = ( $content_type->name ?? '' );
			// Uppercase first char.
			$content_type_slug   = ( ucfirst( $content_type->slug ) ?? '' );

			$attributes['description']      = addslashes( strip_tags( $description ) );
			$attributes['content_type']      = $content_type_name;
			$attributes['content_type_slug'] = $content_type_slug;
			$attributes['strategy'] = ( isset( $ri_product['strategy'] ) ? $ri_product['strategy']->name : '' );
			$attributes['region']   = ( isset( $ri_product['region'] ) ? $ri_product['region']->name : '' );

			if ( isset( $ri_product['style'] ) ) {
				$style = get_term_by( 'id', $ri_product['style'], 'style' );
				$attributes['style'] = $style->name;
			}

			if ( isset( $ri_product['asset_class'] ) ) {
				$asset_class = get_term_by( 'id', $ri_product['asset_class'], 'asset-class' );
				$attributes['asset_class'] = $asset_class->name;
			}

			if ( isset( $ri_product['asset_subclass'] ) ) {
				$asset_subclass = get_term_by( 'id', $ri_product['asset_subclass'], 'asset-sub-class' );
				$attributes['asset_subclass'] = $asset_subclass->name;
			}

		}

		return $attributes;
	}
}
