<?php
namespace Pei_Ri_Resources;

use Pei_Ri_Resources\Post_Types;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Plugin admin page
 */
class Institutions extends Post_Types {
	/**
	 * @var string $post_type
	 *
	 * @access public.
	 */
	public $post_type = 'institution';

	/**
	 * @var string $plural_name
	 *
	 * @access public.
	 */
	public $plural_name = 'Institutions';

	/**
	 * @var string $singular_name
	 *
	 * @access public.
	 */
	public $singular_name = 'Institution';

	/**
	 * @var string $name_admin_bar
	 *
	 * @access public.
	 */
	public $name_admin_bar = 'Institution';

	/**
	 * @var string $menu_name
	 *
	 * @access public.
	 */
	public $menu_name = 'Institutions';

	/**
	 * @var int $decimal_count
	 *
	 * @access protected
	 */
	protected $decimal_count = 3;

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'init', [ $this, 'ri_institutions_post_type' ] );
		add_filter( 'acf/prepare_field/name=ri_post_id', array( $this, 'ri_add_post_id_to_post_id_field' ), 10, 1 );
		add_filter( 'acf/validate_value/name=aum_amount', array( $this, 'ri_validate_decimals' ), 10, 4 );
	}

	/**
	 * Register Institution Post Type.
	 *
	 * @return \WP_Post_Type|\WP_Error The registered post type object on success,
	 *                               WP_Error object on failure.
	 */
	public function ri_institutions_post_type() {
		return $this->ri_post_type();
	}

	/**
	 * Add Post ID as an Institution ID to post_id field and mark this as ready only.
	 */
	public function ri_add_post_id_to_post_id_field( $field ) {
		global $post;

		if ( is_object( $post ) ) {
			$field['value']    = $post->ID;
			$field['readonly'] = true;
		}

		return $field;
	}

	/**
	 * Validate decimal count.
	 *
	 * @param bool $valid
	 * @param string $value
	 * @param array $field
	 * @param string $input_name
	 *
	 * @return string|bool
	 */
	public function ri_validate_decimals( bool $valid, string $value, array $field, string $input_name ) {

		if( true !== $valid ) {
			return $valid;
		}

		$decimal_count = (int) strpos( strrev( $value), '.');

		if ( $decimal_count > $this->decimal_count ) {
			return __( "Max allowed numbers after '.' are {$this->decimal_count}" );
		}

		return true;
	}
}
