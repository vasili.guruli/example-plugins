<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Import RI Resource Class.
 */
class Ri_Resource_Import_Content {

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {}
}
