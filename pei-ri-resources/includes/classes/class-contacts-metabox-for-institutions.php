<?php
namespace Pei_Ri_Resources;

use Pei_Ri_Resources\Post_Types;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Contacts Metabox On Institution Edit Page
 */
class Contacts_Metabox_For_Institutions  {
	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'add_meta_boxes', array( $this, 'institutions_contact_metabox' ) );
	}

	/**
	 * Register Metabox to display Contacts related to Institution.
	 *
	 * @return void
	 */
	public function institutions_contact_metabox() {
		add_meta_box( 'institutions-contacts',
			__( 'Contacts', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			array( $this, 'add_meta_box_for_institutions' ),
			'institution'
		);
	}

	/**
	 * Add Metabox to display Contacts related to Institution.
	 *
	 * @param \WP_Post $post
	 *
	 * @return void
	 */
	public function add_meta_box_for_institutions( \WP_Post $post ) {
		include RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/admin/institution-metabox.php';
	}
}
