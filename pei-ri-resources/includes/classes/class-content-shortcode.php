<?php

namespace Pei_Ri_Resources;

/**
 * Class that is responsible for configuration of Algolia implementation.
 */
class Content_Shortcode {

	/**
	 * @var bool $shortcode_displayed
	 */
	private $shortcode_displayed = false;

	/**
	 * @var array $settings
	 */
	private $settings = [];

	/**
	 * Init function that contains all hooks.
	 */
	public function init() {
		add_shortcode( 'resource-content', [ $this, 'content_shortcode' ] );
		add_action('init', [ $this, 'register_assets' ] );
		add_action('wp_footer', [ $this, 'render_assets' ] );

		$this->settings = get_option( RI_RESOURCE_DB_SETTINGS_KEY );
	}

	/**
	 * Render All Content Template.
	 *
	 * @return string
	 */
	public function content_shortcode( $attributes ) {

		ob_start();

		include RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/public/content.php';

		$this->shortcode_displayed = true;

		$html = ob_get_contents();

		ob_end_clean();

		return $html;
	}

	/**
	 * Register Appropriate Assets for content.
	 *
	 * @return void
	 */
	public function register_assets() {
		wp_enqueue_script(
			'the-resource-content-instantsearch',
			RI_RESOURCES_PLUGIN_DIR_URL . '/assets/src/js/content-instantsearch.js',
			[ 'algolia-instantsearch' ],
			time(),
			true
		);
		wp_localize_script(
			'the-resource-content-instantsearch',
			'ri_content',
			[
				'index_name' => $this->settings['algolia_funds_index'] ?? ''
			]
		);
	}

	/**
	 * Render Assets for content.
	 *
	 * @return void
	 */
	public function render_assets() {
		if ( ! $this->shortcode_displayed ) {
			return;
		}

		wp_print_scripts('content-instantsearch');
	}

	/**
	 * Display Shortcode.
	 *
	 * @return bool
	 */
	public function get_shortcode_displayed() {
		return $this->shortcode_displayed;
	}
}
