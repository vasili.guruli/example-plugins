<?php
namespace Pei_Ri_Resources;

use Pei_Ri_Resources\Post_Types;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Plugin admin page
 */
class Content extends Post_Types {
	/**
	 * @var string $post_type
	 *
	 * @access public.
	 */
	public $post_type = 'content';

	/**
	 * @var string $plural_name
	 *
	 * @access public.
	 */
	public $plural_name = 'Contents';

	/**
	 * @var string $singular_name
	 *
	 * @access public.
	 */
	public $singular_name = 'Content';

	/**
	 * @var string $name_admin_bar
	 *
	 * @access public.
	 */
	public $name_admin_bar = 'Content';

	/**
	 * @var string $menu_name
	 *
	 * @access public.
	 */
	public $menu_name = 'Contents';

	/**
	 * Content Type Fund name
	 */
	public $fund_term_name = 'Fund';

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'init', [ $this, 'ri_content_post_type' ] );
		add_filter( 'acf/prepare_field/name=related_content_id', array( $this, 'ri_related_content_add_post_id_to_acf_field_and_mark_this_field_as_read_only' ), 10, 1 );
		add_filter( 'acf/fields/taxonomy/query/name=content_content_type', array( $this, 'ri_remove_fund_from_content_type_query' ), 10, 3);
	}
	
	/**
	 * Add post ID to related_content_id field, and mark this field as read only.
	 *
	 * @param array $field Field from ACF
	 * @return array
	 */
	public function ri_related_content_add_post_id_to_acf_field_and_mark_this_field_as_read_only( $field ) {
		global $post;
		
		if ( is_object( $post ) ) {
			$field['value']    = $post->ID;
			$field['readonly'] = true;
		}
		
		return $field;
	}

	/**
	 * Register Institution Post Type.
	 *
	 * @return \WP_Post_Type|\WP_Error The registered post type object on success,
	 *                               WP_Error object on failure.
	 */
	public function ri_content_post_type() {
		return $this->ri_post_type();
	}

	/**
	 * Remove Fund from Content Type query.
	 *
	 * @param array $args Args for query
	 * @param array $field Field from ACF
	 * @param int   $post_id Post ID
	 * @return array
	 */
	public function ri_remove_fund_from_content_type_query( $args, $field, $post_id ) {

		$get_fund_term_id = $this->ri_get_term_id_by_name( $this->fund_term_name );

		if ( ! is_int( $get_fund_term_id ) ) {
			return $args;
		}

		$args['exclude'] = $get_fund_term_id;

		return $args;
	}

	/**
	 * Get term id by name
	 */
	public function ri_get_term_id_by_name( $term_name ) {
		$term = get_term_by( 'name', $term_name, 'content-type' );

		if ( ! is_object( $term ) ) {
			return null;
		}

		return $term->term_taxonomy_id;
	}
}
