<?php
namespace Pei_Ri_Resources;

use Pei_Ri_Resources\Post_Types;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Plugin admin page
 */
class Contacts extends Post_Types {
	/**
	 * @var string $post_type
	 *
	 * @access public.
	 */
	public $post_type = 'contact';

	/**
	 * @var string $plural_name
	 *
	 * @access public.
	 */
	public $plural_name = 'Contacts';

	/**
	 * @var string $singular_name
	 *
	 * @access public.
	 */
	public $singular_name = 'Contact';

	/**
	 * @var string $name_admin_bar
	 *
	 * @access public.
	 */
	public $name_admin_bar = 'Contact';

	/**
	 * @var string $menu_name
	 *
	 * @access public.
	 */
	public $menu_name = 'Contacts';

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'init', [ $this, 'ri_contact_post_type' ] );
		add_filter( 'acf/prepare_field/name=contact_id', array( $this, 'ri_add_contact_id_and_make_it_readonly' ), 10, 1 );
	}

	/**
	 * Register Institution Post Type.
	 *
	 * @return \WP_Post_Type|\WP_Error The registered post type object on success,
	 *                               WP_Error object on failure.
	 */
	public function ri_contact_post_type() {
		return $this->ri_post_type();
	}
	
	/**
	 * Add contact_id field and mark this as ready only.
	 *
	 * @param array $field
	 *
	 * @return array
	 */
	public function ri_add_contact_id_and_make_it_readonly( array $field ) {
		global $post;
		
		if ( is_object( $post ) ) {
			$field['value']    = $post->ID;
			$field['readonly'] = true;
		}
		
		return $field;
	}
}
