<?php
namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Class Institutions
 */
class Ri_Page_Template {
	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_filter( 'theme_page_templates', [ $this, 'add_ri_page_template' ] );
		add_filter( 'template_include', [ $this, 'change_page_template' ] );
	}

	/**
	 * Add page templates.
	 *
	 * @param  array  $templates  The list of page templates
	 *
	 * @return array  $templates  The modified list of page templates
	 */
	public function add_ri_page_template( $templates ) {

		$templates[RI_RESOURCES_PLUGIN_DIR_PATH . 'templates/page-ri-resources-template.php'] = __( 'RI Resource Page Template', RI_RESOURCES_PLUGIN_TEXT_DOMAIN );

		return $templates;
	}

	/**
	 * Change the page template to the selected template on the dropdown.
	 *
	 * @param string $template
	 *
	 * @return mixed
	 */
	public function change_page_template( $template ) {

		if ( is_page_template( 'page-ri-resources-template.php' )) {

			$template = RI_RESOURCES_PLUGIN_DIR_PATH . '/templates/page-ri-resources-template.php';
		}

		if ( '' === $template ) {
			throw new \Exception( 'No template found' );
		}

		return $template;
	}
}
