<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Content List Class to provide additional info on Content list table.
 */
class Content_List extends Custom_List_Table {
	/**
	 * @var $post_type
	 */
	protected $post_type = 'content';
	
	/**
	 * Content Columns Data.
	 *
	 * @param array $defaults
	 *
	 * @return array
	 */
	public function ri_content_columns( array $defaults ) {
		unset( $defaults['title'] );
		unset( $defaults['comments'] );
		unset( $defaults['author'] );
		unset( $defaults['date'] );
		unset( $defaults['taxonomy-content-type'] );
		
		$defaults['content_id']            = 'Id';
		$defaults['title']                 = 'Title';
		$defaults['author']                = 'Author';
		$defaults['taxonomy-content-type'] = 'Content Type';
		$defaults['institution_name']      = 'Institution Name';
		$defaults['date']                  = 'Date';
		
		
		return $defaults;
	}
	
	/**
	 * Display Custom Content on Content's table list.
	 *
	 * @param string $column
	 * @param int $post_id
	 */
	public function ri_content_columns_content( string $column, int $post_id ) {
		switch ( $column ) {
			case 'content_id' :
				echo esc_attr( $post_id );
				break;
			case 'institution_name' :
				$is_product  = get_post_meta( esc_attr( $post_id ), 'is_this_product', true );
				$institution = get_post_meta( esc_attr( $post_id ), 'related_content_institution', true );
				//product_institution
				if ( '1' === $is_product ) {
					$institution = get_post_meta( esc_attr( $post_id ), 'ri_product_product_institution', true );
					
				}
				$institution_name = get_the_title( (int) $institution );
				
				echo '<a href="' . esc_url( get_edit_post_link( $institution ) ) . '">' . esc_attr( $institution_name ) . '</a>';
				break;
		}
	}
}
