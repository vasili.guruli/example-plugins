<?php

namespace Pei_Ri_Resources {

	defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );
	/**
	 * Class Institutions
	 */
	class Ri_Resources_CPT_Setup {
		/**
		 * Array of plugin classes with Custom Post Types.
		 */
		public $plugin_classes = [
			[
				'class' => 'Enqueues',
			],
			[
				'class' => 'Admin',
			],
			[
				'class' => 'Institutions',
			],
			[
				'class' => 'Contacts',
			],
			[
				'class' => 'Content',
			],
			[
				'class' => 'Taxonomies',
			],
			[
				'class' => 'Ri_Page_Template',
			],
			[
				'class' => 'Directory_Shortcode',
			],
			[
				'class' => 'Funds_Shortcode',
			],
			[
				'class' => 'Institutions_Template',
			],
			[
				'class' => 'Contents_Template',
			],
			[
				'class' => 'Contacts_Metabox_For_Institutions',
			],
			[
				'class' => 'Addresses_Metabox_For_Contacts',
			],
			[
				'class' => 'Ajax',
			],
			[
				'class' => 'Content_Shortcode',
			],
			[
				'class' => 'Institutions_List',
			],
			[
				'class' => 'Contacts_List',
			],
			[
				'class' => 'Content_List',
			],
		];

		/**
		 * Run plugin with CTP's basic classes.
		 *
		 * @return void
		 */
		public function run() {
			foreach ( $this->plugin_classes as $classes => $class  ) {
				if ( isset( $classes ) ) {
					$namespace_class = __NAMESPACE__ . '\\' . $class['class'];
					if ( class_exists( $namespace_class ) ) {
						$called_class = new $namespace_class();
						if ( method_exists( $called_class, 'init' ) ) {
							$called_class->init();
						}
					}
				}
			}
		}

		/**
		 * Get list of basic plugin classes.
		 *
		 * @return array
		 */
		public function get_plugin_classes() {
			return apply_filters( 'ri_resources_plugin_classes', $this->plugin_classes );
		}
	}
}



