<?php

namespace Pei_Ri_Resources;

use Pei_Ri_Resources\Admin;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Submenu Items for Custom Post Types
 */
class Submenu_Items extends Admin {

	/**
	 * Submenu Items for custom post types.
	 *
	 * @return array
	 */
	public function submenu_items() {
		$submenu_items = [
			// Institutions
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'All Institutions' ),
				'menu_title'  => __( sprintf( '%s', '<b class="' . self::PAGE_NAME . '">All Institutions</b>' ) ),
				'capability'  => 'ri_resource_view',
				'menu_slug'   => 'edit.php?post_type=institution',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Add New' ),
				'menu_title'  => __( 'Add New' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'post-new.php?post_type=institution',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Company Type' ),
				'menu_title'  => __( 'Company Type' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=company-type&post_type=institution',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Sponsorship Type' ),
				'menu_title'  => __( 'Sponsorship Type' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=sponsorship-type&post_type=institution',
			],
			// Content
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'All Contents' ),
				'menu_title'  => __( sprintf( '%s', '<b class="' . self::PAGE_NAME . '">All Contents</b>' ) ),
				'capability'  => 'ri_resource_view',
				'menu_slug'   => 'edit.php?post_type=content',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Add New' ),
				'menu_title'  => __( 'Add New' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'post-new.php?post_type=content',
			],
			/*[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Products' ),
				'menu_title'  => __( 'Products' ),
				'capability'  => self::CAPABILITY,
				'menu_slug'   => 'edit-tags.php?taxonomy=product&post_type=content',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Related Contents' ),
				'menu_title'  => __( 'Related Contents' ),
				'capability'  => self::CAPABILITY,
				'menu_slug'   => 'edit-tags.php?taxonomy=related-content&post_type=content',
			],*/
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Content Type' ),
				'menu_title'  => __( 'Content Type' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=content-type&post_type=content',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Resource Category' ),
				'menu_title'  => __( 'Resource Category' ),
				'capability'  => 'resource-category',
				'menu_slug'   => 'edit-tags.php?taxonomy=resource-category&post_type=content',
			],
			// Contacts
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'All Contacts' ),
				'menu_title'  => __( sprintf( '%s', '<b class="' . self::PAGE_NAME . '">All Contacts</b>' ) ),
				'capability'  => 'ri_resource_view',
				'menu_slug'   => 'edit.php?post_type=contact',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Add New' ),
				'menu_title'  => __( 'Add New' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'post-new.php?post_type=contact',
			],

			// Categories
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Resource Categories' ),
				'menu_title'  => __( sprintf( '%s', '<b class="' . self::PAGE_NAME . '">Resource Categories</b>' ) ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=resource-category&post_type=content',
			],

			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Resource Content Filters' ),
				'menu_title'  => __( sprintf( '%s', '<b onclick="return false;" class="' . self::PAGE_NAME . '">Resource Content Filters</b>' ) ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=resource_content_filters',
			],

			// Resource Content Filters
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Region' ),
				'menu_title'  => __( 'Regions' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=region&post_type=content',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Strategy' ),
				'menu_title'  => __( 'Strategies' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=strategy&post_type=content',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Style' ),
				'menu_title'  => __( 'Styles' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=style&post_type=content',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Asset Class' ),
				'menu_title'  => __( 'Asset Classes' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=asset-class&post_type=content',
			],
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'Asset Sub Class' ),
				'menu_title'  => __( 'Asset Sub Classes' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => 'edit-tags.php?taxonomy=asset-sub-class&post_type=content',
			],
			// RI Resources Index Settings
			[
				'parent_slug' => self::PAGE_NAME,
				'page_title'  => __( 'RI Resource Index Settings' ),
				'menu_title'  => __( 'RI Resource Index Settings' ),
				'capability'  => 'ri_resource_manage',
				'menu_slug'   => RI_RESOURCE_INDEX_SETTINGS_PAGE,
				'callback'    => 'ri_resource_index_settings'
			],
		];

		return apply_filters( 'ri_resources_submenu_items', $submenu_items );
	}
}

