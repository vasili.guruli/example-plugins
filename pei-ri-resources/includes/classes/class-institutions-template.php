<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Institution_Template class
 */
class Institutions_Template extends Ri_Resource_Templates {

	/**
	 * @var string $post_type
	 */
	protected $post_type = 'institution';

	/**
	 * @var string $template_name
	 */
	protected $template_name = 'single-institution';


	public function init() {
		add_filter( 'single_template', [ $this, 'load_appropriate_template' ] );
	}
}
