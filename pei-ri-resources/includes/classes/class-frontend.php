<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Custom frontend parts class
 */
class Frontend {

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_filter( 'the_content', array( $this, 'automatic_optin' ), 10 );
	}

	/**
	 * Assign capabilities for managers
	 *
	 * @return void
	 */
	public function automatic_optin( $content ) {
		if ( ! is_admin() && (
				'the-resource' == get_queried_object()->post_name ||
				is_page_template( 'page-ri-resources-template.php' ) ||
				'contact' == get_post_type() ||
				'content' == get_post_type() ||
				'institution' == get_post_type()
			) ) {

			if ( ! $this->user_have_optin() ) {
				$this->set_optin();
			}
		}

		return $content;
	}
	/**
	 * Sets user optin in sailthru
	 *
	 * @return void
	 */
	public function set_optin() {

		$helper    = new \Pei_Ri_Resources\Helper();
		$api_key   = $helper->get_sailthru_api_key();
		$api_url   = $helper->get_api_url();
		$use_email = $helper->get_logged_in_user_email();

		$json       = array(
			'id'  => $use_email,
			'var' => 'Pref_DATASHARING',
		);
		$postfields = array(
			'api_key' => $api_key,
			'json'    => json_encode( $json ),
		);

		$curl = curl_init();

		curl_setopt_array(
			$curl,
			array(
				CURLOPT_URL            => $api_url . '/user/subscribe',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING       => '',
				CURLOPT_MAXREDIRS      => 10,
				CURLOPT_TIMEOUT        => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST  => 'POST',
				CURLOPT_POSTFIELDS     => http_build_query( $postfields ),
				CURLOPT_HTTPHEADER     => array(
					'Content-Type: application/x-www-form-urlencoded',
				),
			)
		);

		$response = curl_exec( $curl );

		curl_close( $curl );
	}


	/**
	 * Checks if user have optin set
	 *
	 * @return void
	 */
	public function user_have_optin() {
		$helper    = new \Pei_Ri_Resources\Helper();
		$api_key   = $helper->get_sailthru_api_key();
		$api_url   = $helper->get_api_url();
		$use_email = $helper->get_logged_in_user_email();

		$json       = array(
			'id'     => $use_email,
			'fields' => array( 'Pref_DATASHARING' => 1 ),
		);
		$postfields = array(
			'api_key' => $api_key,
			'json'    => json_encode( $json ),
		);

		$curl = curl_init();

		curl_setopt_array(
			$curl,
			array(
				CURLOPT_URL            => $api_url . '/user',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING       => '',
				CURLOPT_MAXREDIRS      => 10,
				CURLOPT_TIMEOUT        => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST  => 'POST',
				CURLOPT_POSTFIELDS     => http_build_query( $postfields ),
				CURLOPT_HTTPHEADER     => array(
					'Content-Type: application/x-www-form-urlencoded',
				),
			)
		);

		$response = curl_exec( $curl );
		$response = json_decode( $response, true );
		curl_close( $curl );

		if ( 401 == $response['statusCode'] ) {
			$data = json_decode( $response['body'], true );

			if ( 1 == $data['Pref_DATASHARING'] ) {
				return true;
			}
		}

		return false;
	}
}
