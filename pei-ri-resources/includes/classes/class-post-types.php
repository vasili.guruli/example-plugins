<?php
namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Class Institutions
 */
abstract class Post_Types {
	/**
	 * @var string $post_type
	 *
	 * @access public.
	 */
	public $post_type;

	/**
	 * @var string $plural_name
	 *
	 * @access public.
	 */
	public $plural_name;

	/**
	 * @var string $singular_name
	 *
	 * @access public.
	 */
	public $singular_name;

	/**
	 * @var string $name_admin_bar
	 *
	 * @access public.
	 */
	public $name_admin_bar;

	/**
	 * @var string $menu_name
	 *
	 * @access public.
	 */
	public $menu_name;

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	abstract public function init();

	/**
	 * Post Type Institutions.
	 *
	 * @return \WP_Post_Type|\WP_Error The registered post type object on success,
	 *  WP_Error object on failure.
	 */
	public function ri_post_type() {
		$labels = array(
			'name'                  => __( $this->plural_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'singular_name'         => __( $this->singular_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'menu_name'             => __( $this->menu_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'name_admin_bar'        => __( $this->name_admin_bar, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'add_new'               => __( 'Add New', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'add_new_item'          => __( 'Add New ' . $this->singular_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'new_item'              => __( 'New ' . $this->singular_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'edit_item'             => __( 'Edit ' . $this->singular_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'view_item'             => __( 'View ' . $this->singular_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'all_items'             => __( $this->plural_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'search_items'          => __( 'Search ' . $this->plural_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'parent_item_colon'     => __( 'Parent ' . $this->plural_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'not_found'             => __( 'No ' . $this->plural_name . ' found.', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'not_found_in_trash'    => __( 'No ' . $this->plural_name . ' found in Trash.', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'featured_image'        => __( $this->singular_name . ' Cover Image', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'set_featured_image'    => __( 'Set cover image', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'remove_featured_image' => __( 'Remove cover image', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'use_featured_image'    => __( $this->singular_name . ' image', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'archives'              => __( $this->singular_name . ' archives', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'insert_into_item'      => __( 'Insert into ' . $this->singular_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'uploaded_to_this_item' => __( 'Uploaded to ' . $this->singular_name, RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'filter_items_list'     => __( 'Filter ' . $this->plural_name . ' list', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'items_list_navigation' => __( $this->plural_name . ' list navigation', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			'items_list'            => __( $this->plural_name . ' list', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => false,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $this->post_type ),
			'capability_type'    => strtolower( $this->singular_name ),
			'capabilities'          => array(
				'edit_post'          => 'resource_edit_' . strtolower( $this->singular_name ),
				'edit_posts'         => 'resource_edit_' . strtolower( $this->plural_name ),
				'edit_others_posts'  => 'resource_edit_other_' . strtolower( $this->plural_name ),
				'edit_private_posts' => 'resource_edit_private_posts_' . strtolower( $this->plural_name ),
				'delete_private_posts' => 'resource_delete_private_posts_' . strtolower( $this->plural_name ),
				'publish_posts'      => 'resource_publish_' . strtolower( $this->plural_name ),
				'read_post'          => 'resource_read_' . strtolower( $this->singular_name ),
				'read_private_posts' => 'resource_read_private_' . strtolower( $this->plural_name ),
				'delete_post'        => 'resource_delete_' . strtolower( $this->singular_name ),
				'delete_posts'		 => 'resource_delete_posts_' . strtolower( $this->singular_name ),
				'create_posts'       => 'resource_create_posts_' . strtolower( $this->plural_name ),
				'delete_others_posts' => 'resource_delete_others_posts_' . strtolower( $this->plural_name ),
				'delete_published_posts' => 'resource_delete_published_posts_' . strtolower( $this->plural_name ),
				'edit_published_posts' => 'resource_edit_published_posts_' . strtolower( $this->plural_name ),

				
			),
			'map_meta_cap'          => true,
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 20,
			'show_in_rest'       => true,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
		);

		return $this->register_ri_institution_post_type( $this->post_type, $args );
	}

	/**
	 * Register Post Type.
	 *
	 * @param array  $args Args of Post Type to Register.
	 * @param string $post_type Custom Post Type.
	 *
	 * @return \WP_Post_Type|\WP_Error The registered post type object on success,
	 *                               WP_Error object on failure.
	 */
	private function register_ri_institution_post_type( string $post_type, array $args ) {
		return register_post_type( $post_type, $args );
	}
}
