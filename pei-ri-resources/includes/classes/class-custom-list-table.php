<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Custom List Table Class to display custom columns in custom posts list.
 */
abstract class Custom_List_Table {
	
	/**
	 * @var string $post_type
	 */
	protected $post_type;
	
	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_filter( "manage_{$this->post_type}_posts_columns", array( $this, "ri_{$this->post_type}_columns" ), 10, 1 );
		add_action( "manage_{$this->post_type}_posts_custom_column", array(
			$this,
			"ri_{$this->post_type}_columns_content"
		), 10, 2 );
	}
}
