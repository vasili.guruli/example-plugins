<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Institutions List Class to provide additional info on Institutions list table.
 */
class Institutions_List extends Custom_List_Table {
	/**
	 * @var $post_type
	 */
	protected $post_type = 'institution';
	
	/**
	 * Institutions Columns Data.
	 *
	 * @param array $defaults
	 *
	 * @return array
	 */
	public function ri_institution_columns( array $defaults ) {
		unset( $defaults['title'] );
		unset( $defaults['comments'] );
		unset( $defaults['author'] );
		unset( $defaults['date'] );
		
		$defaults['organization_id'] = 'Organization Id';
		$defaults['title']           = 'Name';
		$defaults['sponsorship']     = 'Sponsorship';
		$defaults['starts_at']       = 'Starts At';
		$defaults['ends_at']         = 'Ends At';
		$defaults['company_type']    = 'Company Type';
		$defaults['author']          = 'Author';
		$defaults['date']            = 'Date';
		
		
		return $defaults;
	}
	
	/**
	 * Display Custom Content on Institution's table list.
	 *
	 * @param string $column
	 * @param int $post_id
	 */
	public function ri_institution_columns_content( string $column, int $post_id ) {
		$companies = array();
		switch ( $column ) {
			case 'organization_id' :
				echo esc_attr( $post_id );
				break;
			case 'sponsorship' :
				echo get_post_meta( esc_attr( $post_id ), 'sponsorship_level', true );
				break;
			case 'starts_at' :
				echo get_post_meta( esc_attr( $post_id ), 'starts_at', true );
				break;
			case 'ends_at' :
				echo get_post_meta( esc_attr( $post_id ), 'ends_at', true );
				break;
			case 'company_type' :
				$company_types = get_field( 'field_637cdffc854c3', esc_attr( $post_id ) );
				foreach ( $company_types as $company_id ) {
					$company     = get_term( $company_id );
					$companies[] = $company->name;
				}
				if ( count( $companies ) === 1 ) {
					echo esc_attr( $companies[0] );
				} else {
					echo esc_attr( implode( ' | ', $companies ) );
				}
				
				break;
		}
	}
}
