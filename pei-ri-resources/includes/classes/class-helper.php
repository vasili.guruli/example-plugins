<?php

namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Helper methods to retrieve specific data
 */
class Helper {

	/**
	 * Get Sailthru API from options
	 *
	 * @return mixed
	 */
	public function get_sailthru_api_key() {
		$sailthru_data = get_option( 'sailthru_setup_options' );

		return $sailthru_data['sailthru_api_key'];
	}

    /**
	 * Get Brand URL from options
	 *
	 * @return false|mixed|void
	 */
	public function get_brand_url() {
		return get_option( 'odin_blaize_frontend_url' );
	}

	/**
	 * Get API URL
	 *
	 * @return false|mixed|void
	 */
	public function get_api_url() {
        $settings = get_option( RI_RESOURCE_DB_SETTINGS_KEY );

		if ( ! is_array( $settings ) ) {
			return null;
		}
		$api_url = $settings['sailthru_api_url'];

		return $api_url;
	}

	/**
	 * Get logged in user email address from Blaize endpoint
	 *
	 * @return void
	 */
	public function get_logged_in_user_email() {
		$user_session_id         = $_COOKIE['blaize_session'];
		$headers                 = [ 'Cookie' => "blaize_session=$user_session_id", ];
		$blaize_account_endpoint = esc_url( $this->get_brand_url() . '/blaize/account' );
		$response                = wp_remote_get( $blaize_account_endpoint, [ 'headers' => $headers ] );

		if ( is_array( $response ) && ! is_wp_error( $response ) ) {
			$request_body  = wp_remote_retrieve_body( $response );
			$response_body = json_decode( $request_body, true );

			return $response_body['identifiers']['email_address'];
		} else {
			return false;
		}
	}

	/**
	 * Get slug for content type.
	 *
	 * @param string $content_type
	 * @param string $meta
	 *
	 * @return string
	 */
	public static function get_meta_slug_for_content_type( string $content_type = 'content', string $meta = '' ) {
		$slug = 'related_content_' . $meta;

		if ( 'product' === $content_type ) {
			$slug = 'ri_product_' . $meta;
		}

		return $slug;
	}

	/**
	 * Bread Crumbs for Content.
	 *
	 * @param int $institution_id
	 * @param string $institution_title
	 *
	 * @return string
	 */
	public static function ri_content_breadcrumbs( int $institution_id, string $institution_title ) {
		$html = '<div class="entry-crumbs ri-breadcrumbs" itemprop="breadcrumb">';
		$html .= '<a href="' . esc_url( home_url() ) . '">' . esc_html( 'Home' ) . '</a>';
		$html .= '<i class="td-icon-right td-bread-sep"></i> ';
		$html .= '<a href="' . esc_url( home_url( '/the-resource' ) ) . '">' . esc_html( 'ESG Hub' ) . '</a>';
		$html .= '<i class="td-icon-right td-bread-sep"></i> ';
		$html .= '<a href="' . esc_url( get_permalink( $institution_id ) ) . '">' . esc_html( $institution_title ) . '</a>';
		$html .= '<i class="td-icon-right td-bread-sep"></i> ';
		$html .= get_the_title();
		$html .= '</div>';

		return $html;
	}
}
