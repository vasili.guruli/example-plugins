<?php
namespace Pei_Ri_Resources;

use Pei_Ri_Resources\Post_Types;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Plugin admin page
 */
class Funds extends Post_Types {
	/**
	 * @var string $post_type
	 *
	 * @access public.
	 */
	public $post_type = 'institution';

	/**
	 * Term slug
	 */
    public $term_name = 'Fund';

    /**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_filter( 'acf/prepare_field/name=fund_product_id', array( $this, 'ri_fund_add_post_id_to_acf_field_and_mark_this_field_as_read_only' ), 10, 1 );
		add_filter( 'acf/prepare_field/key=field_634d6b77b4fa0', array( $this, 'ri_load_default_content_type' ), 10, 1 );
	}

    /**
     * Add post ID to fund_product_id field, and mark this field as read only
     * 
     * @param array $field Field from ACF
     */
    public function ri_fund_add_post_id_to_acf_field_and_mark_this_field_as_read_only( $field ) {
        global $post;

		if ( is_object( $post ) ) {
			$field['value']    = $post->ID;
			$field['readonly'] = true;
		}

		return $field;
    }

	/*
	 * Load default content type
	 * 
	 * @param array $args
	 * @param array $field
	 * @param int $post_id
	 */
	public function ri_load_default_content_type( $field ) {

		$get_term_id = $this->ri_get_term_id_by_name( $this->term_name );

		if ( ! is_int( $get_term_id ) ) {
			return $field;
		}

		$field['value'] = $get_term_id;

		return $field;
	}

	/**
	 * Get term id by name
	 */
	public function ri_get_term_id_by_name( $term_name ) {
		$term = get_term_by( 'name', $term_name, 'content-type' );

		if ( ! is_object( $term ) ) {
			return null;
		}

		return $term->term_taxonomy_id;
	}
}
