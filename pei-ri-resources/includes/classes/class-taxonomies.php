<?php
namespace Pei_Ri_Resources;

defined( 'ABSPATH' ) || die( 'Direct file access is forbidden' );

/**
 * Class Institutions
 */
class Taxonomies {
	/**
	 * @var array $taxonomies
	 *
	 * @access public.
	 */
	public $taxonomies = [
		// Content Taxonomies
		/*[
			'slug'         => 'product',
			'single_name'  => 'Product',
			'plural_name'  => 'Products',
			'post_type'    => 'content',
		],
		[
			'slug'         => 'related-content',
			'single_name'  => 'Related Content',
			'plural_name'  => 'Related Contents',
			'post_type'    => 'content',
		],*/
		[
			'slug'         => 'content-type',
			'single_name'  => 'Content Type',
			'plural_name'  => 'Content Type',
			'post_type'    => 'content',
		],
		// Resource Categories Tax
		[
			'slug'         => 'resource-category',
			'single_name'  => 'Resource Category',
			'plural_name'  => 'Resource Categories',
			'post_type'    => [ 'content' ],
		],
		// Resource Conte Filters
		[
			'slug'         => 'region',
			'single_name'  => 'Region',
			'plural_name'  => 'Regions',
			'post_type'    => [ 'content' ],
		],
		[
			'slug'         => 'strategy',
			'single_name'  => 'Strategy',
			'plural_name'  => 'Strategies',
			'post_type'    => [ 'content' ],
		],
		[
			'slug'         => 'style',
			'single_name'  => 'Style',
			'plural_name'  => 'Styles',
			'post_type'    => [ 'content' ],
		],
		[
			'slug'         => 'asset-class',
			'single_name'  => 'Asset Class',
			'plural_name'  => 'Asset Classes',
			'post_type'    => [ 'content' ],
		],
		[
			'slug'         => 'asset-sub-class',
			'single_name'  => 'Asset Sub Class',
			'plural_name'  => 'Asset Sub Classes',
			'post_type'    => [ 'content' ],
		],
		[
			'slug'         => 'company-type',
			'single_name'  => 'Company Type',
			'plural_name'  => 'Company Types',
			'post_type'    => [ 'institution' ],
		],
		[
			'slug'         => 'sponsorship-type',
			'single_name'  => 'Sponsorship Type',
			'plural_name'  => 'Sponsorship Types',
			'post_type'    => [ 'institution' ],
		],
	];

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'init', [ $this, 'ri_taxonomy' ] );
	}

	/**
	 * Post Type Institutions.
	 *
	 * @return void
	 */
	public function ri_taxonomy() {

		foreach ( $this->taxonomies as $taxonomy ) {
			$labels = [
				'name'              => _x( $taxonomy['plural_name'], 'taxonomy general name', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'singular_name'     => _x( $taxonomy['single_name'], 'taxonomy singular name', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'search_items'      => __( 'Search ' . $taxonomy['plural_name'], RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'all_items'         => __( 'All ' . $taxonomy['plural_name'], RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'parent_item'       => __( 'Parent ' . $taxonomy['single_name'], RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'parent_item_colon' => __( 'Parent ' . $taxonomy['single_name'] . ':', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'edit_item'         => __( 'Edit ' . $taxonomy['single_name'], RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'update_item'       => __( 'Update ' . $taxonomy['single_name'], RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'add_new_item'      => __( 'Add New ' . $taxonomy['single_name'], RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'new_item_name'     => __( 'New ' . $taxonomy['single_name'] . ' Name', RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
				'menu_name'         => __(  $taxonomy['plural_name'], RI_RESOURCES_PLUGIN_TEXT_DOMAIN ),
			];

			$rewrite      = $taxonomy['rewrite'] ?? [ 'slug' => $taxonomy['slug'] ];
			$hierarchical = $taxonomy['hierarchical'] ?? true;

			$args = [
				'rewrite'           => $rewrite,
				'hierarchical'      => $hierarchical,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_in_menu'      => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'capabilities'      => array(
					'manage_terms'  => 'ri_resource_manage_terms',
					'edit_terms'    => 'ri_resource_edit_terms',
					'delete_terms'  => 'ri_resource_delete_terms',
					'assign_terms'  => 'ri_resource_assign_terms'
				)
			];

			$this->register_ri_taxonomy( $taxonomy['slug'], [ $taxonomy['post_type'] ], $args );
		}
	}

	/**
	 * Register Post Type.
	 *
	 * @param array  $args
	 * @param array  $post_type
	 * @param string $taxonomy
	 *
	 * @return \WP_Taxonomy|WP_Error The registered taxonomy object on success,
	 * WP_Error object on failure.
	 */
	public function register_ri_taxonomy( string $taxonomy, array $post_type, array $args ) {
		return register_taxonomy( $taxonomy, $post_type, $args );
	}
}
