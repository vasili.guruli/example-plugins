# PEI RI Resource
Provides RI Resources for Responsible Investor.

## Tests
Install dependencies with `composer require --dev brain/monkey:2.* phpunit/phpunit ^9`

Run tests with `vendor/bin/phpunit`

## PLEASE DO NOT DISTRIBUTE THE PLUGIN TO THIRD PERSONS